<?php
$WSDL='
<definitions xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:tns="BroadbandProvisioningServices" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns="http://schemas.xmlsoap.org/wsdl/" targetNamespace="BroadbandProvisioningServices">
	<types>
		<xsd:schema targetNamespace="BroadbandProvisioningServices">
			<xsd:import namespace="http://schemas.xmlsoap.org/wsdl/"/>
			<xsd:element name="Can_CreateRqElement" type="tns:CommonCCRqType"/>
			<xsd:element name="Can_CreateRsElement" type="tns:CommonCCRsType"/>
			<xsd:element name="ActivateRqElement" type="tns:CommonRqType"/>
			<xsd:element name="ActivateRsElement" type="tns:CommonRsType"/>
			<xsd:element name="DeactivateRqElement" type="tns:CommonRqType"/>
			<xsd:element name="DeactivateRsElement" type="tns:CommonRsType"/>
			<xsd:element name="Get_ParamsRqElement" type="tns:CommonGPRqType"/>
			<xsd:element name="Get_ParamsRsElement" type="tns:CommonGPRsType"/>
			<xsd:element name="CommonExceptionElement" type="tns:CommonExceptionType"/>
			<xsd:complexType name="CommonCCRqType">
				<xsd:sequence>
					<xsd:element name="subscriberIdentity" type="xsd:string" minOccurs="0" maxOccurs="1" use="required"/>
				</xsd:sequence>
			</xsd:complexType>
			<xsd:complexType name="CommonCCRsType">
				<xsd:sequence>
					<xsd:element name="status" type="xsd:string" minOccurs="0" maxOccurs="1"/>
				</xsd:sequence>
			</xsd:complexType>
			<xsd:complexType name="CommonGPRqType">
				<xsd:sequence>
					<xsd:element name="subscriberIdentity" type="xsd:string" minOccurs="0" maxOccurs="1" use="required"/>
				</xsd:sequence>
			</xsd:complexType>
			<xsd:complexType name="CommonGPRsType">
				<xsd:sequence>
					<xsd:element name="return" type="xsd:string" minOccurs="0" maxOccurs="1"/>
				</xsd:sequence>
			</xsd:complexType>
			<xsd:complexType name="CommonRqType">
				<xsd:sequence>
					<xsd:element name="subscriberIdentity" type="xsd:string" minOccurs="0" maxOccurs="1"/>
					<xsd:element name="customerID" type="xsd:string" minOccurs="0" maxOccurs="1"/>
					<xsd:element name="deviceSerialNumber" type="xsd:string" minOccurs="0" maxOccurs="1"/>
					<xsd:element name="servicePlan" type="xsd:string" minOccurs="0" maxOccurs="1"/>
					<xsd:element name="invoiceProfile" type="xsd:string" minOccurs="0" maxOccurs="1"/>
					<xsd:element name="oldSerialNumber" type="xsd:string" minOccurs="0" maxOccurs="1"/>
					<xsd:element name="capacity" type="xsd:string" minOccurs="0" maxOccurs="1"/>
					<xsd:element name="technology" type="xsd:string" minOccurs="0" maxOccurs="1"/>
				</xsd:sequence>
			</xsd:complexType>
			<xsd:complexType name="CommonRsType">
				<xsd:sequence>
					<xsd:element name="status" type="xsd:string" minOccurs="0" maxOccurs="1"/>
					<xsd:element name="message" type="xsd:string" minOccurs="0" maxOccurs="1"/>
					<xsd:element name="customerID" type="xsd:string" minOccurs="0" maxOccurs="1"/>
					<xsd:element name="checkDigit" type="xsd:string" minOccurs="0" maxOccurs="1"/>
					<xsd:element name="totalBonusBytes" type="xsd:string" minOccurs="0" maxOccurs="1"/>
				</xsd:sequence>
			</xsd:complexType>
			<xsd:complexType name="CommonExceptionType">
				<xsd:sequence>
					<xsd:element name="code" type="xsd:string" minOccurs="1" maxOccurs="1"/>
					<xsd:element name="message" type="xsd:string" minOccurs="1" maxOccurs="1"/>
				</xsd:sequence>
			</xsd:complexType>
		</xsd:schema>
	</types>
	<message name="Can_CreateRequestMessage">
		<part name="parameter" element="tns:Can_CreateRqElement"/>
	</message>
	<message name="Can_CreateResponseMessage">
		<part name="parameter" element="tns:Can_CreateRsElement"/>
	</message>
	<message name="Can_CreateExceptionMessage">
		<part name="parameter" element="tns:CommonExceptionElement"/>
	</message>
	<message name="ActivateRequestMessage">
		<part name="parameter" element="tns:ActivateRqElement"/>
	</message>
	<message name="ActivateResponseMessage">
		<part name="parameter" element="tns:ActivateRsElement"/>
	</message>
	<message name="ActivateExceptionMessage">
		<part name="parameter" element="tns:CommonExceptionElement"/>
	</message>
	<message name="DeactivateRequestMessage">
		<part name="parameter" element="tns:DeactivateRqElement"/>
	</message>
	<message name="DeactivateResponseMessage">
		<part name="parameter" element="tns:DeactivateRsElement"/>
	</message>
	<message name="DeactivateExceptionMessage">
		<part name="parameter" element="tns:CommonExceptionElement"/>
	</message>
	<message name="Get_ParamsRequestMessage">
		<part name="parameter" element="tns:Get_ParamsRqElement"/>
	</message>
	<message name="Get_ParamsResponseMessage">
		<part name="parameter" element="tns:Get_ParamsRsElement"/>
	</message>
	<message name="GetParamsExceptionMessage">
		<part name="parameter" element="tns:CommonExceptionElement"/>
	</message>

	<portType name="BroadbandProvisioningServicesPortType">
		<operation name="Can_Create">
			<input name="Can_CreateRequest" message="tns:Can_CreateRequestMessage"/>
			<output name="Can_CreateResponse" message="tns:Can_CreateResponseMessage"/>
			<fault name="Can_CreateException" message="tns:Can_CreateExceptionMessage"/>
		</operation>
		<operation name="Activate">
			<input name="ActivateRequest" message="tns:ActivateRequestMessage"/>
			<output name="ActivateResponse" message="tns:ActivateResponseMessage"/>
			<fault name="ActivateException" message="tns:ActivateExceptionMessage"/>
		</operation>
		<operation name="Deactivate">
			<input name="DeactivateRequest" message="tns:DeactivateRequestMessage"/>
			<output name="DeactivateResponse" message="tns:DeactivateResponseMessage"/>
			<fault name="DeactivateException" message="tns:DeactivateExceptionMessage"/>
		</operation>
		<operation name="Get_Params">
			<input name="Get_ParamsRequest" message="tns:Get_ParamsRequestMessage"/>
			<output name="Get_ParamsResponse" message="tns:Get_ParamsResponseMessage"/>
			<fault name="Get_ParamsException" message="tns:Get_ParamsExceptionMessage"/>
		</operation>
	</portType>
	<binding name="BroadbandProvisioningServicesBinding" type="tns:BroadbandProvisioningServicesPortType">
		<soap:binding style="document" transport="http://schemas.xmlsoap.org/soap/http"/>
		<operation name="Can_Create">
			<soap:operation soapAction="Can_CreateAction" style="document"/>
			<input name="Can_CreateRequest">
				<soap:body parts="parameter" use="literal"/>
			</input>
			<output name="Can_CreateResponse">
				<soap:body parts="parameter" use="literal"/>
			</output>
			<fault name="Can_CreateException" xsi:type="tns:CommonException">
				<soap:fault name="Can_CreateException" use="literal"/>
			</fault>
		</operation>
		<operation name="Activate">
			<soap:operation soapAction="ActivateAction" style="document"/>
			<input name="ActivateRequest">
				<soap:body parts="parameter" use="literal"/>
			</input>
			<output name="ActivateResponse">
				<soap:body parts="parameter" use="literal"/>
			</output>
			<fault name="ActivateException" xsi:type="tns:CommonException">
				<soap:fault name="ActivateException" use="literal"/>
			</fault>
		</operation>
		<operation name="Deactivate">
			<soap:operation soapAction="DeactivateAction" style="document"/>
			<input name="DeactivateRequest">
				<soap:body parts="parameter" use="literal"/>
			</input>
			<output name="DeactivateResponse">
				<soap:body parts="parameter" use="literal"/>
			</output>
			<fault name="DeactivateException" xsi:type="tns:CommonException">
				<soap:fault name="DeactivateException" use="literal"/>
			</fault>
		</operation>
		<operation name="Get_Params">
			<soap:operation soapAction="Get_ParamsAction" style="document"/>
			<input name="Get_ParamsRequest">
				<soap:body parts="parameter" use="literal"/>
			</input>
			<output name="Get_ParamsResponse">
				<soap:body parts="parameter" use="literal"/>
			</output>
			<fault name="Get_ParamsException" xsi:type="tns:CommonException">
				<soap:fault name="Get_ParamsException" use="literal"/>
			</fault>
		</operation>		
	</binding>
	<service name="BroadbandProvisioningServices">
		<port name="BroadbandProvisioningServicesPort" binding="tns:BroadbandProvisioningServicesBinding">
			<soap:address location="'.$soap_location.'"/>
		</port>
	</service>
</definitions>';
/*
$WSDLold='
<definitions xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:tns="BroadbandProvisioningServices" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns="http://schemas.xmlsoap.org/wsdl/" targetNamespace="BroadbandProvisioningServices">
	<types>
		<xsd:schema targetNamespace="BroadbandProvisioningServices">
			<xsd:import namespace="http://schemas.xmlsoap.org/wsdl/"/>
			<xsd:element name="Can_CreateRqElement" type="tns:CommonCCRqType"/>
			<xsd:element name="Can_CreateRsElement" type="tns:CommonCCRsType"/>
			<xsd:element name="ActivateRqElement" type="tns:CommonRqType"/>
			<xsd:element name="ActivateRsElement" type="tns:CommonRsType"/>
			<xsd:element name="DeactivateRqElement" type="tns:CommonRqType"/>
			<xsd:element name="DeactivateRsElement" type="tns:CommonRsType"/>
			<xsd:element name="UpdateRqElement" type="tns:CommonRqType"/>
			<xsd:element name="UpdateRsElement" type="tns:CommonRsType"/>
			<xsd:element name="SuspendRqElement" type="tns:CommonRqType"/>
			<xsd:element name="SuspendRsElement" type="tns:CommonRsType"/>
			<xsd:element name="UnsuspendRqElement" type="tns:CommonRqType"/>
			<xsd:element name="UnsuspendRsElement" type="tns:CommonRsType"/>
			<xsd:element name="MoveRqElement" type="tns:CommonRqType"/>
			<xsd:element name="MoveRsElement" type="tns:CommonRsType"/>
			<xsd:element name="SwapRqElement" type="tns:CommonRqType"/>
			<xsd:element name="SwapRsElement" type="tns:CommonRsType"/>
			<xsd:element name="AddTokensRqElement" type="tns:CommonRqType"/>
			<xsd:element name="AddTokensRsElement" type="tns:CommonRsType"/>
			<xsd:element name="AddTokensCCRqElement" type="tns:CommonCCRqType"/>
			<xsd:element name="AddTokensCCRsElement" type="tns:CommonCCRsType"/>
			<xsd:element name="CommonExceptionElement" type="tns:CommonExceptionType"/>

			<xsd:complexType name="CommonCCRqType">
				<xsd:sequence>
					<xsd:element name="subscriberIdentity" type="xsd:string" minOccurs="0" maxOccurs="1" use="required"/>
				</xsd:sequence>
			</xsd:complexType>
			<xsd:complexType name="CommonCCRsType">
				<xsd:sequence>
					<xsd:element name="status" type="xsd:string" minOccurs="0" maxOccurs="1"/>
				</xsd:sequence>
			</xsd:complexType>


			<xsd:complexType name="CommonRqType">
				<xsd:sequence>
					<xsd:element name="subscriberIdentity" type="xsd:string" minOccurs="0" maxOccurs="1"/>
					<xsd:element name="customerID" type="xsd:string" minOccurs="0" maxOccurs="1"/>
					<xsd:element name="deviceSerialNumber" type="xsd:string" minOccurs="0" maxOccurs="1"/>
					<xsd:element name="servicePlan" type="xsd:string" minOccurs="0" maxOccurs="1"/>
					<xsd:element name="invoiceProfile" type="xsd:string" minOccurs="0" maxOccurs="1"/>
					<xsd:element name="oldSerialNumber" type="xsd:string" minOccurs="0" maxOccurs="1"/>
					<xsd:element name="capacity" type="xsd:string" minOccurs="0" maxOccurs="1"/>
					<xsd:element name="technology" type="xsd:string" minOccurs="0" maxOccurs="1"/>
				</xsd:sequence>
			</xsd:complexType>
			<xsd:complexType name="CommonRsType">
				<xsd:sequence>
					<xsd:element name="status" type="xsd:string" minOccurs="0" maxOccurs="1"/>
					<xsd:element name="message" type="xsd:string" minOccurs="0" maxOccurs="1"/>
					<xsd:element name="customerID" type="xsd:string" minOccurs="0" maxOccurs="1"/>
					<xsd:element name="checkDigit" type="xsd:string" minOccurs="0" maxOccurs="1"/>
					<xsd:element name="totalBonusBytes" type="xsd:string" minOccurs="0" maxOccurs="1"/>
				</xsd:sequence>
			</xsd:complexType>
			<xsd:complexType name="CommonExceptionType">
				<xsd:sequence>
					<xsd:element name="code" type="xsd:string" minOccurs="1" maxOccurs="1"/>
					<xsd:element name="message" type="xsd:string" minOccurs="1" maxOccurs="1"/>
				</xsd:sequence>
			</xsd:complexType>
		</xsd:schema>
	</types>
	<message name="Can_CreateRequestMessage">
		<part name="parameter" element="tns:Can_CreateRqElement"/>
	</message>
	<message name="Can_CreateResponseMessage">
		<part name="parameter" element="tns:Can_CreateRsElement"/>
	</message>
	<message name="Can_CreateExceptionMessage">
		<part name="parameter" element="tns:CommonExceptionElement"/>
	</message>
	<message name="ActivateRequestMessage">
		<part name="parameter" element="tns:ActivateRqElement"/>
	</message>
	<message name="ActivateResponseMessage">
		<part name="parameter" element="tns:ActivateRsElement"/>
	</message>
	<message name="ActivateExceptionMessage">
		<part name="parameter" element="tns:CommonExceptionElement"/>
	</message>
	<message name="DeactivateRequestMessage">
		<part name="parameter" element="tns:DeactivateRqElement"/>
	</message>
	<message name="DeactivateResponseMessage">
		<part name="parameter" element="tns:DeactivateRsElement"/>
	</message>
	<message name="DeactivateExceptionMessage">
		<part name="parameter" element="tns:CommonExceptionElement"/>
	</message>
	<message name="UpdateRequestMessage">
		<part name="parameter" element="tns:UpdateRqElement"/>
	</message>
	<message name="UpdateResponseMessage">
		<part name="parameter" element="tns:UpdateRsElement"/>
	</message>
	<message name="UpdateExceptionMessage">
		<part name="parameter" element="tns:CommonExceptionElement"/>
	</message>
	<message name="SuspendRequestMessage">
		<part name="parameter" element="tns:SuspendRqElement"/>
	</message>
	<message name="SuspendResponseMessage">
		<part name="parameter" element="tns:SuspendRsElement"/>
	</message>
	<message name="SuspendExceptionMessage">
		<part name="parameter" element="tns:CommonExceptionElement"/>
	</message>
	<message name="UnsuspendRequestMessage">
		<part name="parameter" element="tns:UnsuspendRqElement"/>
	</message>
	<message name="UnsuspendResponseMessage">
		<part name="parameter" element="tns:UnsuspendRsElement"/>
	</message>
	<message name="UnsuspendExceptionMessage">
		<part name="parameter" element="tns:CommonExceptionElement"/>
	</message>
	<message name="MoveRequestMessage">
		<part name="parameter" element="tns:MoveRqElement"/>
	</message>
	<message name="MoveResponseMessage">
		<part name="parameter" element="tns:MoveRsElement"/>
	</message>
	<message name="MoveExceptionMessage">
		<part name="parameter" element="tns:CommonExceptionElement"/>
	</message>
	<message name="SwapRequestMessage">
		<part name="parameter" element="tns:SwapRqElement"/>
	</message>
	<message name="SwapResponseMessage">
		<part name="parameter" element="tns:SwapRsElement"/>
	</message>
	<message name="SwapExceptionMessage">
		<part name="parameter" element="tns:CommonExceptionElement"/>
	</message>
	<message name="AddTokensRequestMessage">
		<part name="parameter" element="tns:AddTokensRqElement"/>
	</message>
	<message name="AddTokensResponseMessage">
		<part name="parameter" element="tns:AddTokensRsElement"/>
	</message>
	<message name="AddTokensExceptionMessage">
		<part name="parameter" element="tns:CommonExceptionElement"/>
	</message>
	<portType name="BroadbandProvisioningServicesPortType">
		<operation name="Can_Create">
			<input name="Can_CreateRequest" message="tns:Can_CreateRequestMessage"/>
			<output name="Can_CreateResponse" message="tns:Can_CreateResponseMessage"/>
			<fault name="Can_CreateException" message="tns:Can_CreateExceptionMessage"/>
		</operation>
		<operation name="Activate">
			<input name="ActivateRequest" message="tns:ActivateRequestMessage"/>
			<output name="ActivateResponse" message="tns:ActivateResponseMessage"/>
			<fault name="ActivateException" message="tns:ActivateExceptionMessage"/>
		</operation>
		<operation name="Deactivate">
			<input name="DeactivateRequest" message="tns:DeactivateRequestMessage"/>
			<output name="DeactivateResponse" message="tns:DeactivateResponseMessage"/>
			<fault name="DeactivateException" message="tns:DeactivateExceptionMessage"/>
		</operation>
		<operation name="Update">
			<input name="UpdateRequest" message="tns:UpdateRequestMessage"/>
			<output name="UpdateResponse" message="tns:UpdateResponseMessage"/>
			<fault name="UpdateException" message="tns:UpdateExceptionMessage"/>
		</operation>
		<operation name="Suspend">
			<input name="SuspendRequest" message="tns:SuspendRequestMessage"/>
			<output name="SuspendResponse" message="tns:SuspendResponseMessage"/>
			<fault name="SuspendException" message="tns:SuspendExceptionMessage"/>
		</operation>
		<operation name="Unsuspend">
			<input name="UnsuspendRequest" message="tns:UnsuspendRequestMessage"/>
			<output name="UnsuspendResponse" message="tns:UnsuspendResponseMessage"/>
			<fault name="UnsuspendException" message="tns:UnsuspendExceptionMessage"/>
		</operation>
		<operation name="Move">
			<input name="MoveRequest" message="tns:MoveRequestMessage"/>
			<output name="MoveResponse" message="tns:MoveResponseMessage"/>
			<fault name="MoveException" message="tns:MoveExceptionMessage"/>
		</operation>
		<operation name="Swap">
			<input name="SwapRequest" message="tns:SwapRequestMessage"/>
			<output name="SwapResponse" message="tns:SwapResponseMessage"/>
			<fault name="SwapException" message="tns:SwapExceptionMessage"/>
		</operation>
		<operation name="AddTokens">
			<input name="AddTokensRequest" message="tns:AddTokensRequestMessage"/>
			<output name="AddTokensResponse" message="tns:AddTokensResponseMessage"/>
			<fault name="AddTokensException" message="tns:AddTokensExceptionMessage"/>
		</operation>
	</portType>
	<binding name="BroadbandProvisioningServicesBinding" type="tns:BroadbandProvisioningServicesPortType">
		<soap:binding style="document" transport="http://schemas.xmlsoap.org/soap/http"/>

		<operation name="Can_Create">
			<soap:operation soapAction="Can_CreateAction" style="document"/>
			<input name="Can_CreateRequest">
				<soap:body parts="parameter" use="literal"/>
			</input>
			<output name="Can_CreateResponse">
				<soap:body parts="parameter" use="literal"/>
			</output>
			<fault name="Can_CreateException" xsi:type="tns:CommonException">
				<soap:fault name="Can_CreateException" use="literal"/>
			</fault>
		</operation>

		<operation name="Activate">
			<soap:operation soapAction="ActivateAction" style="document"/>
			<input name="ActivateRequest">
				<soap:body parts="parameter" use="literal"/>
			</input>
			<output name="ActivateResponse">
				<soap:body parts="parameter" use="literal"/>
			</output>
			<fault name="ActivateException" xsi:type="tns:CommonException">
				<soap:fault name="ActivateException" use="literal"/>
			</fault>
		</operation>
		<operation name="Deactivate">
			<soap:operation soapAction="DeactivateAction" style="document"/>
			<input name="DeactivateRequest">
				<soap:body parts="parameter" use="literal"/>
			</input>
			<output name="DeactivateResponse">
				<soap:body parts="parameter" use="literal"/>
			</output>
			<fault name="DeactivateException" xsi:type="tns:CommonException">
				<soap:fault name="DeactivateException" use="literal"/>
			</fault>
		</operation>
		<operation name="Suspend">
			<soap:operation soapAction="SuspendAction" style="document"/>
			<input name="SuspendRequest">
				<soap:body parts="parameter" use="literal"/>
			</input>
			<output name="SuspendResponse">
				<soap:body parts="parameter" use="literal"/>
			</output>
			<fault name="SuspendException" xsi:type="tns:CommonException">
				<soap:fault name="SuspendException" use="literal"/>
			</fault>
		</operation>
		<operation name="Unsuspend">
			<soap:operation soapAction="UnsuspendAction" style="document"/>
			<input name="UnsuspendRequest">
				<soap:body parts="parameter" use="literal"/>
			</input>
			<output name="UnsuspendResponse">
				<soap:body parts="parameter" use="literal"/>
			</output>
			<fault name="UnsuspendException" xsi:type="tns:CommonException">
				<soap:fault name="UnsuspendException" use="literal"/>
			</fault>
		</operation>
		<operation name="Update">
			<soap:operation soapAction="UpdateAction" style="document"/>
			<input name="UpdateRequest">
				<soap:body parts="parameter" use="literal"/>
			</input>
			<output name="UpdateResponse">
				<soap:body parts="parameter" use="literal"/>
			</output>
			<fault name="UpdateException" xsi:type="tns:CommonException">
				<soap:fault name="UpdateException" use="literal"/>
			</fault>
		</operation>
		<operation name="Move">
			<soap:operation soapAction="MoveAction" style="document"/>
			<input name="MoveRequest">
				<soap:body parts="parameter" use="literal"/>
			</input>
			<output name="MoveResponse">
				<soap:body parts="parameter" use="literal"/>
			</output>
			<fault name="MoveException" xsi:type="tns:CommonException">
				<soap:fault name="MoveException" use="literal"/>
			</fault>
		</operation>
		<operation name="Swap">
			<soap:operation soapAction="SwapAction" style="document"/>
			<input name="SwapRequest">
				<soap:body parts="parameter" use="literal"/>
			</input>
			<output name="SwapResponse">
				<soap:body parts="parameter" use="literal"/>
			</output>
			<fault name="SwapException" xsi:type="tns:CommonException">
				<soap:fault name="SwapException" use="literal"/>
			</fault>
		</operation>
		<operation name="AddTokens">
			<soap:operation soapAction="AddTokensAction" style="document"/>
			<input name="AddTokensRequest">
				<soap:body parts="parameter" use="literal"/>
			</input>
			<output name="AddTokensResponse">
				<soap:body parts="parameter" use="literal"/>
			</output>
			<fault name="AddTokensException" xsi:type="tns:CommonException">
				<soap:fault name="AddTokensException" use="literal"/>
			</fault>
		</operation>
	</binding>
	<service name="BroadbandProvisioningServices">
		<port name="BroadbandProvisioningServicesPort" binding="tns:BroadbandProvisioningServicesBinding">
			<soap:address location="'.$soap_location.'"/>
		</port>
	</service>
</definitions>';*/
?>