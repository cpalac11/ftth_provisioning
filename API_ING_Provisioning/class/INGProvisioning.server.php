<?php
/*! \class INGProvisioning
 *  \brief Clase que implementa todos los metodos del Webservice "ABM" - INGProvisioning
 *  \author juebel@gmail.com
 *
 * Ejecuta las acciones para cada Subscriber que se solicitaron en la llamada al Webservice INGProvisioning
 * Se trata de los m�todos "ABM", Alta Baja y Modificaci�n de Subscribers, tanto en la BBDD local como en el NMS correspondiente
 * Deriva las llamadas al NMS "Hughes" o "Provisioning" seg�n la tecnolog�a de la que se trate
 * 
 * Updates
 * \Authors 
 * 		Fernando Munevar fmunevar@directvla.com.co
 * 		Christian Palacios cpalac11@directvla.com.co
 * 
 * Se optimiza el metodo Activate y Deactivate, unicamente con tecnologia FTTH.
 * 
 */
//ini_set('error_reporting', E_ALL|E_STRICT);
//ini_set('display_errors', 1);

class INGProvisioning extends DtvSoapServer {

	function __construct($WSDL, $xml) {
        parent::__construct($WSDL, $xml);
	}

	function Can_Create(){
		$subscriberIdentity = $this->params['subscriberIdentity'];
		if(strlen($subscriberIdentity) != 12){
			$this->soap_can_create_ftth_validation_successfull('Longitud de la FSAN erronea');
		}
		elseif(substr($subscriberIdentity, 0, 4) == 'ZNTS'){
			$this->soap_can_create_ftth_validation_successfull('true');
		}
		elseif(substr($subscriberIdentity, 0, 4) == 'HWTC'){
			$this->Activate();
		}
		else{
			$this->soap_can_create_ftth_validation_successfull('FSAN no valida');
		}
	}

	function Get_Params(){
		$ProvisioningService = new Provisioning();
		$salida = $ProvisioningService->get_params($this->params['subscriberIdentity']);
		//print_r($salida);die();
		$this->soap_return_values($salida);
	}	

    function Activate(){
		$Subscriber = new Subscriber();
		$subscriberIdentity = $this->params['subscriberIdentity'];
		if(strlen($subscriberIdentity) != 12){
			$this->soap_can_create_ftth_validation_successfull('Longitud de la FSAN erronea');
		}
		elseif(substr($subscriberIdentity, 0, 4) == 'HWTC'){
			$save = $Subscriber->FTTH_HWTC($subscriberIdentity, 'Activate');
			$this->soap_can_create_ftth_validation_successfull($save);
		}
        $Subscriber->getOrCreate($this->params['customerID'], $this->params['subscriberIdentity']);
		

		$logM = strftime("%F %X",strtotime('NOW')) . " - Log originado en ".__METHOD__."<hr>";
        if( $Subscriber->status_id == 1 ){
            $this->common_exception(7000, "Subscriber [".$this->params['customerID']."".$this->params['subscriberIdentity']."] ya activado");
		}
        else{
          	// siempre OK en DEV ya que no hay comunicaci�n con .55 (TL, 2017-10-17)
            if(_ENVIROMENT=='DEV') {
				$this->soap_validation_successfull();
			    return true;
			}
           	// inicio el Subscriber con todos los campos para revenir Subscriber vacios
			$Subscriber->technology = $this->params['technology'];
			$Subscriber->deviceSerialNumber = $this->params['deviceSerialNumber'];
			$Subscriber->servicePlanId = $this->params['servicePlan_id'];
            $Subscriber->save();

            $ProvisioningService = new Provisioning();
            $logM .= "\n" . strftime("%F %X",strtotime('NOW')) . " - Es WIMAX/FTTH/LTE";

            if(!$ProvisioningService->response_status){
				$this->common_exception($ProvisioningService->response_code, $ProvisioningService->response_message);
			}
            
			$logM .= "\n" . strftime("%F %X",strtotime('NOW')) . " - Legacy->Create con parametros";
            $ProvisioningService->create($this->params['subscriberIdentity'],$this->params['servicePlan_code_provisioning'],$this->params['technology']);
            $logM .= "\n" . strftime("%F %X",strtotime('NOW')) . var_export([$this->params['subscriberIdentity'],$this->params['servicePlan_code_provisioning'],$this->params['technology']],1);
            $logM .= "\n" . strftime("%F %X",strtotime('NOW')) . " - Response:";
            $logM .= "\n" . strftime("%F %X",strtotime('NOW')) . var_export($ProvisioningService->response_json,1);

			// al tener unificado el response_status, no hay mas ifs ni switchs
            // response_code == 5003 es el "ya aprovisionado" de Provisioning
            if($ProvisioningService->response_status  // ok de todas
				|| $ProvisioningService->response_code == 5005 // ""ya aprovisionado" de ftth?
				|| $ProvisioningService->response_code == 6100 // "ya aprovisionado" de LTE
				|| $ProvisioningService->response_code == 5430 // "ya aprovisionado de LTE/WIMAX"
				){  
                // grabo el subscriber
                $Subscriber->status_id = 1;
                $Subscriber->save();
                $logM .= "\n" . strftime("%F %X",strtotime('NOW')) . " - Subscriber Final:";
                $logM .= "\n" . strftime("%F %X",strtotime('NOW')) . var_export($Subscriber,1);
                $this->soap_validation_successfull();
            } 
			else {
				// reescritura del error
				if(($ProvisioningService->response_code == 400 && preg_match('/Mapped/',$ProvisioningService->response_message))
					||($ProvisioningService->response_code == 500 && preg_match('/JDBC/',$ProvisioningService->response_message))
					||($ProvisioningService->response_code == 5000 && $this->params['technology'] != 'SATELITAL')
					){
					// reintentos en caso de ip duplicada
					$this->common_exception("1-".$ProvisioningService->response_code, $ProvisioningService->response_message);
				} 
				else{
	                $this->common_exception($ProvisioningService->response_code, $ProvisioningService->response_message);
				}
            }
        } // end else de status_id
	} // end Activate
	
	function Deactivate(){
		$Subscriber = new Subscriber();
		if(strlen($this->params['subscriberIdentity']) != 12){
			$this->soap_can_create_ftth_validation_successfull('Longitud de la FSAN erronea');
		}
		elseif(substr($this->params['subscriberIdentity'], 0, 4) == 'HWTC'){
			$save = $Subscriber->FTTH_HWTC($this->params['subscriberIdentity'], 'Deactivate');
			$this->soap_can_create_ftth_validation_successfull($save);
		}
	    // 1. activo, lo desactivo
        // 2. insctivo, lo desactivo? u ok
        // 3 suspendido, lo desactivo
         //// busco Subscriber. Si no existe, lo crea
     	$Subscriber->getOrFail($this->params['customerID'], $this->params['subscriberIdentity']);
		if($Subscriber->count == 0) { 
			$this->common_exception('7000', "No existe el Subscriber"); 
		}
		if($Subscriber->status_id == 2){
			$this->common_exception('7000', "Subscriber [".$this->params['customerID']."".$this->params['subscriberIdentity']."] ya desactivados"); 
        }
		else{
            $ProvisioningService = new Provisioning();
            if(!$ProvisioningService->response_status) { 
               	$this->common_exception($ProvisioningService->response_code, $ProvisioningService->response_message);
			}
            $ProvisioningService->delete($this->params['subscriberIdentity'],$this->params['technology']);
            // al tener unificado el response_status, no hay mas ifs ni switchs
            // response_code == 5003 es el "ya aprovisionado" de Provisioning
			// $ProvisioningService->response_code == 6230 es el "ya desactivado" de provisioning
            // capturo mensajes '11'
            if( ($ProvisioningService->response_code == 11 && preg_match('/CPE already has no/',$ProvisioningService->response_message)) || ( $ProvisioningService->response_code == 11 && preg_match('/SND_CANCELC/',$ProvisioningService->response_message)) ) {
                // cambia de estado
                $Subscriber->status_id = 2;
                $Subscriber->save();
               	$this->common_exception($ProvisioningService->response_code, $ProvisioningService->response_message);
               	exit;
            }
            if($ProvisioningService->response_status || $ProvisioningService->response_code == 6230){
                $Subscriber->status_id = 2;
                $Subscriber->save();
                $this->soap_validation_successfull();
            } 
			else{
				if($ProvisioningService->response_code == 5000 && $this->params['technology'] != 'SATELITAL') {
					// reintentos en caso de 5000-El CPE no puede ser alcanzado
					$this->common_exception("1-".$ProvisioningService->response_code, $ProvisioningService->response_message);
				}
            	$this->common_exception($ProvisioningService->response_code, $ProvisioningService->response_message);
            }
        } // end else de status_id	
	} // end unsuspend

	function Suspend(){
        $Subscriber = new Subscriber(); //// busco Subscriber. Si no existe, lo crea
        $Subscriber->getOrFail($this->params['customerID'], $this->params['subscriberIdentity']);
		// excepciones
		if($Subscriber->count == 0){
			$this->common_exception('7000', "No existe el Subscriber"); 
			exit;
		}

        if($Subscriber->status_id == 2){
			$this->common_exception(7000, "Subscriber [".$this->params['customerID'].":".$this->params['subscriberIdentity']."] esta desactivado"); 
			exit; 
		}
 
    	// no es satelital
        $ProvisioningService = new Provisioning();        
        // salgo si no se puede crear conexion a Provisioning
        if(!$ProvisioningService->response_status){
        	$this->common_exception($ProvisioningService->response_code, $ProvisioningService->response_message);
        }
        // ejecuto "suspension" de LTE/WIMAX/FTTH
        $ProvisioningService->delete($this->params['subscriberIdentity'],$this->params['technology']);                    
        if($ProvisioningService->response_status) {
        	$this->soap_validation_successfull();
        }
		else {
            $this->common_exception($ProvisioningService->response_code, $ProvisioningService->response_message);
    	}        
	} // end suspend

	function Unsuspend(){		
		$Subscriber = new Subscriber(); //// busco Subscriber
        $Subscriber->getOrFail($this->params['customerID'], $this->params['subscriberIdentity']);		
		// excepciones
		if($Subscriber->count == 0) { 
			$this->common_exception('7000', "No existe el Subscriber"); 
			exit; 
		}
        if($Subscriber->status_id == 2) {
			$this->common_exception(7000, "Subscriber [".$this->params['customerID'].":".$this->params['subscriberIdentity']."] esta desactivado"); 
			exit;
		}
		// no es satelital
        $ProvisioningService = new Provisioning();
        if(!$ProvisioningService->response_status){
			$this->common_exception($ProvisioningService->response_code, $ProvisioningService->response_message); 
			exit; 
		}
        
        $ProvisioningService->unsuspend($this->params['subscriberIdentity'],$this->params['technology']);
        $Subscriber->status_id = 1;                    
       	// response del unsuspend no satelital
        if($ProvisioningService->response_status) {
			$this->soap_validation_successfull();
		} else {
			$this->common_exception($ProvisioningService->response_code, $ProvisioningService->response_message);
        }
	} // end unsuspend
	 /** 
     *
     *  @param   
     *  @brief Actualiza el Subscriber con los valores enviados
     *  @brief Valores posibles: deviceSerialNumber, servicePlan, invoiceProfile, capacity
     *  @return array
     *
     */

//  mensajes de error y ok adaptados para ABM (SIN prefijo broN)

	function common_exception($code, $message) {
		$this->writeLog($this->method . ': ERROR-CODE:'.$code .'|'.$message . '|' . json_encode($this->params), LOG_ERR);
		$response = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:'.$this->namespace_tag.'="BroadbandProvisioningServices">
		<soapenv:Body>
			<soapenv:Fault>
				<faultcode>soapenv:Server</faultcode>
				<faultstring xml:lang="en">Error generico</faultstring>
				<detail>
					<'.$this->namespace_tag.':CommonExceptionElement>
					<code>'.$code.'</code>
					<message>'.$message.'</message>
					</'.$this->namespace_tag.':CommonExceptionElement>
				</detail>
			</soapenv:Fault>
		</soapenv:Body>
		</soapenv:Envelope>';
		http_response_code(500);
		print $response;
		exit;
    }

	function method_exception($code, $message){
		$this->writeLog($this->method . ': ERROR-CODE:'.$code .'|'.$message . '|' . json_encode($this->params), LOG_ERR);
    	$response = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:'.$this->namespace_tag.'="BroadbandProvisioningServices">
   		<soapenv:Header/>
   			<soapenv:Body>
				<'.$this->namespace_tag.':'.$this->method.'RsElement>
					<status>'.$code.'</status>
					<message>'.$message.'</message>
				</'.$this->namespace_tag.':'.$this->method.'RsElement>
   			</soapenv:Body>
		</soapenv:Envelope>';
		http_response_code(500);
		print $response;
		exit;
    }

	function soap_validation_successfull(){
        // para cualquier m�todo
        $response = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:'.$this->namespace_tag.'="BroadbandProvisioningServices">
       	<soapenv:Header/>
       		<soapenv:Body>
				<'.$this->namespace_tag.':'.$this->method.'RsElement>
					<status>200</status>
					<message>OK</message>
				</'.$this->namespace_tag.':'.$this->method.'RsElement>
       		</soapenv:Body>
    	</soapenv:Envelope>';
    	http_response_code(200);
    	print $response;
    	exit;
    }

	function soap_can_create_ftth_validation_successfull($mensaje){
        // para cualquier m�todo
        $response = '
		<SOAP-ENV:Envelope SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
			<SOAP-ENV:Body>	 
				<ns1:Provisioning.can_createResponse xmlns:ns1="http://schemas.xmlsoap.org/soap/envelope/">
					<return xsi:type="xsd:boolean">'.$mensaje.'</return>
				</ns1:Provisioning.can_createResponse>
			</SOAP-ENV:Body>
		</SOAP-ENV:Envelope>';
    	http_response_code(200);
    	print $response;
    	exit;
    }

    function soap_return_values($arrValues) {
		$tags ="";
        // para cualquier m�todo
      	foreach ($arrValues as $key => $value){
        // if is array veo
			if(is_array($value)){
				// nuevo nivel
				$tags .=  "\t<".$this->namespace_tag.':'.$key.">\n";
		
							foreach ($value as $subkey => $subvalue) {
							$tags .=  "\t\t<$subkey>$subvalue</$subkey>\n";
							} 

				$tags .=  "\t</".$this->namespace_tag.':'.$key.">\n";
				continue; // para saltar a nueva iteracion sin else
			}
        $tags .=  "<$key>$value</$key>\n";
      }
        $response = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:'.$this->namespace_tag.'="BroadbandProvisioningServices">
       	<soapenv:Header/>
			<soapenv:Body>
				<'.$this->namespace_tag.':'.$this->method.'RsElement>
				'.$tags.'
				</'.$this->namespace_tag.':'.$this->method.'RsElement>
			</soapenv:Body>
    	</soapenv:Envelope>';
    http_response_code(200);
    print $response;
    exit;
    }
} // end class
?>