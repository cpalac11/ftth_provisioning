<?php
//ERROR HANDLING
//ini_set('error_reporting', E_ALL|E_STRICT);
//ini_set('display_errors', 1);
$soap_location = 'http://'.$_SERVER['SERVER_NAME'].$_SERVER['SCRIPT_NAME'];

// requires globales
require("../enviroment.php");
require("../API_ING_includes/Database.class.php");
require("../API_ING_includes/Subscriber.class.php");
require("../API_ING_includes/Utils.class.php");

require("../API_ING_includes/Ip.class.php");
//require("../API_ING_includes/Hughes.class.php");
require("../API_ING_includes/Provisioning.class.php");

require('include/wsdl.php');
require("../API_ING_includes/DtvSoapServer.class.php");
require('class/INGProvisioning.server.php');

$xml = (isset($HTTP_RAW_POST_DATA)) ? $HTTP_RAW_POST_DATA : implode("\r\n", file('php://input'));

$server = new INGProvisioning($WSDL, $xml);
exit;

?>
