<?php
include('telnet.class.php');

class olt_connection {

private $user;
private $password;
private $olt_ip;
private $olt_slot;
private $olt_port;
private $subs_id;
private $connection;
private $telnet;
public $error;

        public function __construct ($olt_ip,$olt_slot,$olt_port,$subs_id){

                $this->password=$GLOBALS['global_config']['olt']['password'];
                $this->user=$GLOBALS['global_config']['olt']['user'];
		$this->olt_ip = $olt_ip;
		$this->olt_slot = $olt_slot;
		$this->olt_port = $olt_port;
		$this->subs_id = $subs_id;
               	$this->telnet = new PHPTelnet(); 

		/*
		echo $this->olt_ip."\n";
		echo $this->olt_slot."\n";
		echo $this->olt_port."\n";
		echo $this->subs_id."\n";
		echo $this->user."\n";
		echo $this->password."\n";
		*/

                if(!$this->connect()){
			syslog(LOG_ERR,$this->error);
                }
        }

        public function __destruct(){
		echo "bye... \n";
		$this->telnet->Disconnect();
        }

        private function connect (){
		
                $this->connection = $this->telnet->Connect($this->olt_ip,$this->user,$this->password);

		         if($this->connection==0){
			
				return true;
                        }else{
                                $this->error = "Telnet loggin problem";
                                return false;
                        }
        }

        public function test (){
		$command= "ip show";
		$this->telnet->DoCommand($command,$result);
		return $result;
        }

	public function get_free_onuid (){
		$command="onu show $this->olt_slot/$this->olt_port";
		$this->telnet->DoCommand($command,$result);

		$result_array = explode("\n",$result);
		$inner_array = array();
		$free_onus_array = array();

		foreach ($result_array as $line){
			trim($line);

			if (preg_match('/^\s*\d/',$line)){
				$inner_array = preg_split('/[\s]+/',$line);
				array_shift($inner_array);
				array_push($free_onus_array,$inner_array);
			}
		}

		if (is_numeric($free_onus_array[0][0]) && ($free_onus_array[0][0] > 0)){ 
		return $free_onus_array[0][0];
		}else{
		syslog(LOG_WARNING,"No more logic slots available for olt: $this->olt_ip on slot".
			           " $this->olt_slot and port $this->oltport");
		exit;
		}

	}


	public function create ($onu_id,$profile){

		//INICIO DES-SETEO DE ZMS PARA GARANTIZAR PRIORIDAD
		$command="update system zmsexists = false zmsipaddress = 0.0.0.0 0";
		$this->telnet->DoCommand($command,$result);

		$command="resetcliprov all";
		$this->telnet->DoCommand($command,$result);

		$command="setline 0";
		$this->telnet->DoCommand($command,$result);
		//FIN DES-SETEO DE ZMS PARA GARANTIZAR PRIORIDAD

	#########################################################################
	#########################ONU_SET#########################################	

	$command = "onu set $this->olt_slot/$this->olt_port/$onu_id vendorid ZNTS".
	" serno fsan ".str_replace('ZNTS','',$this->subs_id)." meprof zhone-2426";
	$this->telnet->DoCommand($command,$result);
	
	$result_array = explode("\n",$result);
	#var_dump($result_array);

	$success = "successfully enabled with serial number ZNTS ".
		   str_replace('ZNTS','',$this->subs_id)." Onu $this->olt_slot/$this->olt_port/$onu_id";
	$message = trim($result_array[1])." ".trim($result_array[2]);

##
	$line_count=0;
	foreach($result_array as $line_string){
 		syslog(LOG_NOTICE,'result_create_onu for '.$this->subs_id.' '.$line_count.' '.trim($line_string));
	$line_count++;
	}
##
		if(strpos($message,$success)){

			syslog(LOG_NOTICE,"create_onu OK for $this->subs_id");

		}else{

			syslog(LOG_NOTICE,"create_onu NOK for $this->subs_id");
	                // FIN SETEO DE ZMS PARA GARANTIZAR PRIORIDAD
	                $command="update system zmsexists = true zmsipaddress = 172.20.4.60 0";
	                $this->telnet->DoCommand($command,$result);
	
	                #$command="configsync initiate partial";
	                #$this->telnet->DoCommand($command,$result);
	                // FIN SETEO DE ZMS PARA GARANTIZAR PRIORIDAD
	                $this->telnet->DoCommand('logout',$result);
	
			return false;
		}
	
	$vlan = $GLOBALS['global_config']['vlans'][$this->olt_ip];
	$profile_down = $GLOBALS['global_config']['profiles'][$profile]['down'];		
	$profile_up = $GLOBALS['global_config']['profiles'][$profile]['up'];		
	$command = "bridge add 1-$this->olt_slot-$this->olt_port-$onu_id/gpononu gtp $profile_down downlink".
	" vlan $vlan tagged epktrule $profile_up g-vlan 200 rg";	

        $this->telnet->DoCommand($command,$result);
        $result_array = explode("\n",$result);
        #var_dump($result_array);
##

	$line_count=0;
	foreach($result_array as $line_string){
 		syslog(LOG_NOTICE,'result_create_bridge for '.$this->subs_id.' '.$line_count.' '.trim($line_string));
	$line_count++;
	}

##

if(((strpos(trim($result_array[2]),"Created bridge")===0) && ((strpos(trim($result_array[3]),"been created"))))
	||(strpos(trim($result_array[0]),"epktrule"))){

			syslog(LOG_NOTICE,"create_bridge OK for $this->subs_id");
	}else{
		syslog(LOG_NOTICE,"create_bridge NOK for $this->subs_id");

	        // FIN SETEO DE ZMS PARA GARANTIZAR PRIORIDAD
                $command="update system zmsexists = true zmsipaddress = 172.20.4.60 0";
                $this->telnet->DoCommand($command,$result);

                #$command="configsync initiate partial";
                #$this->telnet->DoCommand($command,$result);
                // FIN SETEO DE ZMS PARA GARANTIZAR PRIORIDAD
                $this->telnet->DoCommand('logout',$result);

		return false;
	}

		// FIN SETEO DE ZMS PARA GARANTIZAR PRIORIDAD
		$command="update system zmsexists = true zmsipaddress = 172.20.4.60 0";
		$this->telnet->DoCommand($command,$result);

		#$command="configsync initiate partial";
		#$this->telnet->DoCommand($command,$result);
		// FIN SETEO DE ZMS PARA GARANTIZAR PRIORIDAD
                $this->telnet->DoCommand('logout',$result);
		
	return true;

	}//end_create


public function delete ($olt_onuid){

       $iface=$this->olt_slot.'/'.$this->olt_port.'/'.$olt_onuid;
       $command="onu delete $iface";
       #$command= "ip show";
       $this->telnet->DoCommand($command,$result);
       $this->telnet->DoCommand('yes',$result);
       $this->telnet->DoCommand('no',$result);
       $this->telnet->DoCommand('yes',$result);
	return true;

/*
       foreach ($result_array as $array_element){
		echo $array_element."\n";
                $exist = strpos($array_element,"Ok to delete ONU $iface and all of its configuration?");
                if ($exist!==false){
                        $this->telnet->DoCommand('yes',$result);
       			$result_array = explode("\n",$result);
			foreach($result_array as $array_element){
				echo $array_element."\n";
				$exist = strpos($array_element,"Do you want to exit from this request?");
					if ($exist!==false){
						$this->telnet->DoCommand('no',$result);
       						$result_array = explode("\n",$result);
							foreach($result_array as $array_element){
								$exist = strpos($array_element,"Are you sure?");
									if($exist!==false){
										$this->telnet->DoCommand('yes',$result);
										syslog(LOG_ERR,"delete_onu: ok: ".
										$this->subs_id." at ".$iface.
										" been deleted from ".
										$this->olt_ip);
										return true;
									}
							}
					}
			}		
                }
		
        }


	foreach ($result_array as $array_element){
	syslog(LOG_ERR,"delete_onu: nok:".$array_element);
	return false;		
	}	
*/
}


}//end_class


