<?php
class db_connection {

	private $hostname;
	private $user;
	private $password;
	private $database;
	private $connection;
	public $error;

    public function __construct (){
		$this->hostname='172.20.4.207';
        $this->user='user_netprov';
        $this->password='Providesa.2020';
       	$this->database='provisioningPROD';
		/*
		$this->hostname='172.20.4.54';
        $this->user='ADMIN_PROV';
        $this->password='Desa8875.!';
        $this->database='provisioningPROD';

        $this->hostname=$GLOBALS['global_config']['database']['hostname'];
        $this->user=$GLOBALS['global_config']['database']['user'];
        $this->password=$GLOBALS['global_config']['database']['password'];
        $this->database=$GLOBALS['global_config']['database']['database'];*/

		#echo 'hostname'.$this->hostname.'\n';
		#echo $this->user.'\n';
		#echo $this->password.'\n';
		#echo $this->database.'\n';

        if(!$this->connect()){
            $this->error=mysqli_connect_error();
		syslog(LOG_ERR,'MYSQL'.$this->error);
        }
    }

    public function __destruct(){
		mysqli_close($this->connection);
	}

    private function connect (){
		$this->connection = mysqli_connect($this->hostname, $this->user, $this->password, $this->database);		
		if($this->connection){
			return true;
		}
		else{			
			$this->error = mysqli_connect_error();
			return false;
		}
    }

    public function test (){
		$query = "SELECT * FROM subscriber LIMIT 10";
		$result = mysqli_query($this->connection, $query);
		$row = mysqli_fetch_row($result);
		if (mysqli_num_rows($result) > 0) {
			print_r($row);
			return true;
		}
		else{
			$this->error = mysqli_connect_error();
			return false;
		}
	}

	public function is_provisioned ($subs_id){
		$query="SELECT provisioned FROM subscriber WHERE subscriber_identity='$subs_id'";
		$result = mysqli_query($this->connection, $query);
		$row = mysqli_fetch_row($result);
		if(is_null($row[0])){
			return true;
		}
		else{
			return false;
		}				
	}

	public function check_iface ($olt_ip,$olt_slot,$olt_port,$subs_id){
		$query="SELECT olt_ip,olt_slot,olt_port FROM subscriber_info WHERE subscriber_identity='$subs_id'";
		$result = mysqli_query($this->connection, $query);
		$row = mysqli_fetch_assoc($result);
		if($row){
			$real_iface=$olt_ip.'-'.$olt_slot.'-'.$olt_port;
			$db_iface=$row['olt_ip'].'-'.$row['olt_slot'].'-'.$row['olt_port'];
			if($real_iface==$db_iface){
				return true;
			}
			else{
				syslog(LOG_NOTICE,'trap_rcv: '.$real_iface.'(real) dont match with '.
				$db_iface.'(db) for '.$subs_id);
				return false;
			}
		}
		else{
			syslog(LOG_NOTICE,'forbiden state for'.$subs_id);
			exit;
		}				
	}

	public function check_action_reconfig($subs_id,$olt_ip,$olt_slot,$olt_port){	
		$query = "SELECT * FROM trap_action WHERE olt_ip='$olt_ip' and subscriber_identity='$subs_id' and action_type='15' and state=0 and slot='$olt_slot' and port='$olt_port'";
	    $result = mysqli_query($this->connection, $query);
        if (mysqli_num_rows($result) == 0){
			return false; 
		}
		else{
			return true;
		}
	}

	public function add_action_reconfig($subs_id,$olt_ip,$olt_slot,$olt_port){
		$query="INSERT INTO  trap_action (create_date,olt_ip,fsan,subscriber_identity,slot,port,state,action_type) values (NOW(),'$olt_ip','','$subs_id','$olt_slot','$olt_port','0','15')";
		$result = mysqli_query($this->connection, $query);
		if($result){
			return true;
		}
		else{
			return false;
		}
	}
		
	public function check_action_config($subs_id,$olt_ip,$olt_slot,$olt_port){	
		$query = "SELECT * FROM trap_action WHERE olt_ip='$olt_ip' and subscriber_identity='$subs_id' and action_type='1' and state=0 and slot='$olt_slot' and port='$olt_port'";
        $result = mysqli_query($this->connection, $query);
        if (mysqli_num_rows($result) == 0){
			return false; 
		}
		else{
			return true;
		}
	}

	public function add_action_config($subs_id,$olt_ip,$olt_slot,$olt_port){
		$query="INSERT INTO  trap_action (create_date,olt_ip,fsan, subscriber_identity,slot,port,state,action_type) values (NOW(),'$olt_ip','','$subs_id','$olt_slot','$olt_port','0','1')";
		$result = mysqli_query($this->connection, $query);
		if($result){
			return true;
		}
		else{
			return false;
		}
	}

	public function get_profile($subs_id){
		$query = "SELECT profile FROM subscriber WHERE subscriber_identity='$subs_id' LIMIT 1";
		$result = mysqli_query($this->connection, $query);
		$row = mysqli_fetch_assoc($result);
		if(!array_key_exists($row['profile'],$GLOBALS['global_config']['profiles'])){
			return false;	
		}
		else{
			return $row['profile'];
		}
	}

	public function get_trap_to_process(){
        $query = "SELECT *, DATEDIFF(NOW(),create_date) FROM trap_action WHERE action_type=1 order by id";
		$result = mysqli_query($this->connection, $query);
		if(mysqli_num_rows($result) == 0){
			echo "nothing to do....\n";
			exit;
		}
		$result_array = array();
		while($row = mysqli_fetch_assoc($result)) {
			array_push($result_array, array('subs_id' => $row['subscriber_identity'], 'olt_ip' => $row['olt_ip'], 'olt_slot' => $row['slot'], 
			'olt_port' => $row['port'], 'trap_id' => $row['id']));
		}
		return $result_array;
	}

	public function get_delete_traps(){
		$query = "select id,subscriber_identity from trap_action where action_type = 2";
		$result = mysqli_query($this->connection, $query);
		if(mysqli_num_rows($result)==0){
			echo "nothing to do....\n";
			exit;
		}
		$result_array=array();
		while($row = mysqli_fetch_assoc($result)) {
			array_push($result_array,array('trap_id' => $row['id'], 'subs_id' => $row['subscriber_identity']));
		}
		return $result_array;
	}

	public function get_connection_info($subs_id){
		$query = "SELECT olt_slot,olt_port,olt_onuid,olt_ip FROM subscriber_info WHERE subscriber_identity='$subs_id' LIMIT 1";
		$result = mysqli_query($this->connection, $query);
		$row=mysqli_fetch_assoc($result);
		if(mysqli_num_rows($result) == 1){
			$result_array = array('olt_port'=>$row['olt_port'], 'olt_slot'=>$row['olt_slot'], 'olt_onuid'=>$row['olt_onuid'], 'olt_ip'=>$row['olt_ip']);                				
			return $result_array;				
		}
		else{
			$result_array = array('olt_slot' => 'null', 'olt_port'=>'null', 'olt_onuid'=>'null');
			return $result_array;
		}
	}

	public function update_subscriber($subs_id){
		$query="UPDATE subscriber set provisioned=NOW() WHERE subscriber_identity='$subs_id'";
		$result = mysqli_query($this->connection, $query);
		if ($result){
			return true;
		}
		else{
			return false;
		}
	}

	public function update_subscriber_info($olt_ip,$olt_slot,$olt_port,$onu_id,$subs_id,$profile_up,$profile_down){
	    $qos_hash = array(
		"3000" => "3MB",
		"6000" => "6MB",
		"12000" => "12MB",
		"10000" => "10MB",
		"30000" => "30MB",
		"100000" => "100MB",
	    );
		$prof=$qos_hash[$profile_down];
		$query="INSERT INTO subs_ftth (date,fsan,clientid,box,status,agent,prof,olt_ip,olt_slot,olt_port,olt_onuid) VALUES (now(),'$subs_id',0,0,1,'cron','$prof','$olt_ip',$olt_slot,$olt_port,$onu_id) ON DUPLICATE KEY UPDATE date=now(), olt_slot=$olt_slot, olt_port=$olt_port, olt_onuid=$onu_id, olt_ip='$olt_ip', prof='$prof', status=1, agent='cron'";
        echo "$query<br>";
        $result = mysqli_query($this->connection, $query);
		$query="UPDATE subscriber_info set olt_ip='$olt_ip', olt_slot='$olt_slot', olt_port='$olt_port', olt_onuid='$onu_id', upload_speed='$profile_up', 
		download_speed='$profile_down' where subscriber_identity='$subs_id'";
		$result = mysqli_query($this->connection, $query);
		if ($result){
			return true;
		}
		else{
			return false;
		}
	}
		
	public function delete_action_trap($trap_id){		
		$query="DELETE FROM trap_action WHERE id='$trap_id'";
		$result = mysqli_query($this->connection, $query);
		if ($result){
			return true;
		}
		else{
			return false;
		}
	}

	public function delete_subscriber($subs_id){		
		$query = "DELETE FROM subscriber WHERE subscriber_identity='$subs_id'";
		$result = mysqli_query($this->connection, $query);
		if($result){
			return true;
		}
		else{
			return false;
		}
	}

	public function delete_subscriber_info($subs_id){		
		$query = "DELETE FROM subscriber_info WHERE subscriber_identity='$subs_id'";
		$result = mysqli_query($this->connection, $query);
		if ($result){
			return true;
		}
		else{
			return false;
		}
	}

    public function update_trap_action($trap_id){
		$query = "UPDATE trap_action set action_type=10 WHERE id='$trap_id'";
		$result = mysqli_query($this->connection, $query);
		if ($result){
			return true;
		}
		else{
			return false;
		}
    }

    public function update_delete_trap($trap_id){
		$query="UPDATE trap_action set action_type=20 WHERE id='$trap_id'";
		$result = mysqli_query($this->connection, $query);
		if ($result){
			return true;
		}
		else{
			return false;
		}
    }
	
	public function delete_old_traps(){
		$query = 'SELECT *,TIMESTAMPDIFF(HOUR,create_date,now()) AS old FROM trap_action where action_type=1';
		$result = mysqli_query($this->connection, $query);
		if(mysqli_num_rows($result) == 0){
			echo "nothing to do....\n";
			exit;
		}
		while($row = mysqli_fetch_assoc($result)){		
			if($row['old'] >= 2){
				$inner_query = "DELETE FROM trap_action where id='".$row['id']."'";
				$inner_result = mysqli_query($this->connection, $inner_query);
				if ($inner_result){
					syslog(LOG_NOTICE,'cleaned : trap id '.$row['id'].
					' for '.$row['subscriber_identity'].' olt '.$row['olt_ip'].
					' slot '.$row['slot']. ' port '.$row['port']);						
				}
			}
        }
	}

	public function delete_type2_traps(){
		$query ='SELECT id,TIMESTAMPDIFF(DAY,create_date,now()) AS old FROM trap_action where action_type=2';
		$result = mysqli_query($this->connection, $query);
		if(mysqli_num_rows($result) == 0){
			echo "nothing to do....\n";
			exit;
		}
		while($row = mysqli_fetch_assoc($result)){
			if($row['old'] >= 1){
				$inner_query = "DELETE FROM trap_action where id='".$row['id']."'";
				$inner_result = mysqli_query($this->connection, $inner_query);
				if ($inner_result){
					syslog(LOG_NOTICE,'cleaner_type=2 : trap id '.$row['id'].
					' from action traps');	
				}
			}
		}
	}
}
?>