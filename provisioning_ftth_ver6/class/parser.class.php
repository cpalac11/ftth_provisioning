<?php

/*! \file parser.class.php
    \brief Este archivo es el principal de la clase Parser.

    Details.
*/

class Parser {

private	$home_dir = null;
private	$counter = null;
protected $olt_ip = null;
protected $subs_id = null;
protected $olt_slot = null;
protected $olt_port = null;
protected $olt_shelf = null;
protected $trap_type = null;
protected $oid = null;

	function __construct(){	
		$counter = 0;
		$this->trap_type = 0;
		$this->trap_type=feof(STDIN);
		while (!feof(STDIN)){
			$input=fgets(STDIN);
            if($counter == 1){
                $value=trim($input);
                $this->olt_ip=substr($value,strpos($value,'[')+1,strpos($value,']')-strpos($value,'[')-1);
            }
			else{
				list($oid,$value) = explode(' ',$input);				
				if(!defined($value)){				
					if (strpos($oid,'5504.3.1.2.4.0')) {
						$this->olt_shelf=trim($value);
						continue;
					} elseif (strpos($oid,'5504.3.1.2.5.0')) {
						$this->olt_slot=trim($value);
						continue;
					} elseif (strpos($oid,'5504.3.1.2.6.0')) {
						$this->olt_port=trim($value);
						continue;
					} elseif (strpos($oid,'5504.5.14.1.5.1.4')) {
						$value=str_replace('"','',trim($value));
					  	$this->subs_id='ZNTS'.strtoupper($this->fsan_convert($value));
					  	$this->trap_type=1;
						continue;
					}
				}
			}				
		$counter++;
		}//while
	}

    private function fsan_convert($value) {
        return "0".dechex($value);
    }

	public function get_olt_ip(){
		return $this->olt_ip;
	}

	public function get_trap_type(){
		return $this->trap_type;
	}

	public function get_olt_slot(){
		return $this->olt_slot;
	}

	public function get_olt_port(){
		return $this->olt_port;
	}
	
	public function get_subs_id(){
		return $this->subs_id;
	}
}
?>