#!/usr/bin/php
<?php
/*	Updates
 * \Authors 
 * 		Fernando Munevar fmunevar@directvla.com.co
 * 		Christian Palacios cpalac11@directvla.com.co
 * 
 * Limpia cada 2 horas los traps de la DB, se identa y se organiza el codigo.
 * 
 */
error_reporting(E_ALL ^ E_WARNING); 
openlog('Provisioning_ftth', LOG_ODELAY, LOG_LOCAL7);
	#########################################################################
	########################INIT_PROCESS#####################################
try {
	include('class/mysql_connection.class.php');
	/// Load/Parse in the global configuration file
	$global_config = @parse_ini_file('global_config.ini', true);
	/// Make it globally accessible
	$GLOBALS['global_config'] = $global_config;
	syslog(LOG_INFO,"cleaner: im alive...");
	$conn = new db_connection();
	$conn->delete_old_traps();		
	unset($conn);
} catch (Exception $e){
    syslog(LOG_ERR, $e->getMessage() .' ('. $e->getCode() .')');
}
closelog();
?>