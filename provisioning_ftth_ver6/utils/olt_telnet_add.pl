#!/usr/bin/perl

use Net::Telnet ();
use strict;
use Scalar::Util qw(looks_like_number);
use Sys::Syslog qw(:DEFAULT :standard :macros);

	#usage:
	#olt_telnet_add.pl [olt_ip] [olt_slot] [olt_port] [fsan] [speed] [vlan]

	my $olt_ip = shift(@ARGV);
	my $olt_slot = shift(@ARGV);
	my $olt_port = shift(@ARGV);
	my $fsan = shift(@ARGV);
	my $speed = shift(@ARGV);
	my $vlan = shift(@ARGV);
	$fsan =~ s/ZNTS//;

	my $t = new Net::Telnet (Timeout => 10,	Prompt => '/[\$%#>] $/');
	$t->open($olt_ip);
	$t->login('admin', 'zhone');

	my @lines;
	my @line_arr;
	my $new_line;
	my $inner_line;
	my $onu_created_flag;
	my $bridge_created_flag;
	my @error_arr;
	my $error_line;
	my @success_arr;
	my $success_line;
	my @free_onu_inner_arr;
	my @free_onu_arr;
	my $free_onuid;
	my $free_onuid_flag;
	my $onu_id;
	my $onu_command;
	my $bridge_command;

	#########################################################

	push (@error_arr,"*********begin: olt:$olt_ip slot:$olt_slot port:$olt_port _**************\n");
	push (@success_arr,"*********begin: olt:$olt_ip slot:$olt_slot port:$olt_port _**************\n");

	@lines = $t->cmd("update system zmsexists = false zmsipaddress = 0.0.0.0 0");
	@lines = $t->cmd("resetcliprov all");
	@lines = $t->cmd("setline 0");

	@lines = $t->cmd("onu show $olt_slot/$olt_port");
	foreach $new_line (@lines){
		@line_arr  = split(/\r/,$new_line);	
		foreach $inner_line (@line_arr){			
			if($inner_line=~/^\s*\d/){				
				@free_onu_inner_arr = split(/[\s]+/,$inner_line);
				foreach $free_onuid(@free_onu_inner_arr){						
					if($free_onuid=~/^\s*\d/){
						push(@free_onu_arr,$free_onuid);
					}
				}
			}
		}
		
	}
	
	if (looks_like_number($free_onu_arr[0])){
		$free_onuid_flag = 1;
		$onu_id = $free_onu_arr[0] ;
		push(@success_arr,"free onu id found for $fsan is $onu_id\n");			
		$onu_command = "onu set $olt_slot/$olt_port/$onu_id vendorid ZNTS serno fsan $fsan meprof zhone-2426";
		push(@success_arr,"$onu_command,\n");	
		@lines = $t->cmd($onu_command);
		@line_arr = split(/\r/,@lines);
		
		
		foreach $new_line (@lines){
			@line_arr  = split(/\r/,$new_line);		
			foreach $inner_line (@line_arr){			
				if($inner_line!~/^\s*$/){				
					if($inner_line=~/successfully enabled/){
						$onu_created_flag=1;	
						push(@success_arr,$inner_line);			
						last;
					}
				push(@error_arr,$inner_line);
				}
			}
			if($onu_created_flag){
				last;
			}		
		}

		if($onu_created_flag){
			$bridge_command="bridge add 1-$olt_slot-$olt_port-$onu_id/gpononu gtp $speed downlink vlan".
			" $vlan tagged epktrule $speed g-vlan 200 rg";
			push(@success_arr,"bridge add 1-$olt_slot-$olt_port-$onu_id/gpononu gtp $speed downlink vlan $vlan tagged\n");
			push(@success_arr,"epktrule $speed g-vlan 200 rg\n");	
			#push(@success_arr,"$bridge_command,\n");	
			@lines = $t->cmd($bridge_command);
			@line_arr  = split(/\r/,@lines);
			foreach $new_line (@lines){
			@line_arr  = split(/\r/,$new_line);			
				foreach $inner_line (@line_arr){
					if($inner_line!~/^\s*$/){					
						if($inner_line=~/Created bridge/){
							$bridge_created_flag=1;	
							push(@success_arr,$inner_line);			
							last;
						}
					push(@error_arr,$inner_line);
					}
				}
				if($bridge_created_flag){
					last;
				}			
			}
		}
	}else{
		push(@error_arr,"not free onu id found for");			
	}

	###FINISH TELNET SESION

	@lines = $t->cmd("update system zmsexists = true zmsipaddress = 172.20.4.60 0");
	@lines = $t->cmd("configsync initiate partial");
	push (@error_arr,"closing after sanity\n");
	push (@success_arr,"closing after sanity\n");
	$t->close;

	if(($onu_created_flag)&&($bridge_created_flag)&&($free_onuid_flag)){		
		openlog("add_OK",'ndelay', 'local7');
		foreach $success_line(@success_arr){
			#print "success $success_line";
			syslog(LOG_INFO,"$fsan: $success_line");
		}
		closelog();
		exit $onu_id;
	}else{
		openlog("add_NOK",'ndelay', 'local7');
		foreach $error_line(@error_arr){
			#print "error $error_line";
			syslog(LOG_INFO,"$fsan: $error_line");
		}
		closelog();
		exit 0;
	}