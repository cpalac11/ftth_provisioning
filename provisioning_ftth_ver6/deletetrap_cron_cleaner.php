#!/usr/bin/php
<?php
/* Updates
 * \Authors 
 * 		Fernando Munevar fmunevar@directvla.com.co
 * 		Christian Palacios cpalac11@directvla.com.co
 * 
 * A las 9:30 am hace un borrado en trap_action where action_type=2.
 * 
 */
error_reporting(E_ALL ^ E_WARNING); 
openlog('Provisioning_ftth', LOG_ODELAY, LOG_LOCAL7);
#########################################################################
########################INIT_PROCESS#####################################
try {
	include('class/mysql_connection.class.php');
	/// Load/Parse in the global configuration file
	$global_config = @parse_ini_file('global_config.ini', true);
	/// Make it globally accessible
	$GLOBALS['global_config'] = $global_config;
	syslog(LOG_INFO,"cleaner_type=2: im alive...");
	$conn = new db_connection();
	$conn->delete_type2_traps();
	unset($conn);
} catch (Exception $e){
    syslog(LOG_ERR, $e->getMessage() .' ('. $e->getCode() .')');
}
closelog();
?>