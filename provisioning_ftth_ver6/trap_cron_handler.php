#!/usr/bin/php
<?php
/* Updates
 * \Authors 
 * 		Fernando Munevar fmunevar@directvla.com.co
 * 		Christian Palacios cpalac11@directvla.com.co
 * 
 * Trap que se ejecuta cada minuto buscando los traps y agregando a la DB para asignar perfil
 * 
 */
error_reporting(E_ALL ^ E_WARNING); 
openlog('ProvisionDEV_ftth', LOG_ODELAY, LOG_LOCAL7);
//syslog(LOG_INFO,"ahi tamos");
#########################################################################
########################INIT_PROCESS#####################################

try {	
	include('class/mysql_connection.class.php');
	include('class/olt_connection.class.php');
	/// Load/Parse in the global configuration file
	$global_config = @parse_ini_file('global_config.ini', true);
	/// Make it globally accessible
	$GLOBALS['global_config'] = $global_config;
	$conn = new db_connection();
	$traps_array = $conn->get_trap_to_process();		
	foreach($traps_array as $trap){
        syslog(LOG_INFO,"main: founded ".$trap['trap_id']."=>".$trap['subs_id']." from : ".$trap['olt_ip']." at: ".$trap['olt_slot']."/".$trap['olt_port']." for fsan: ".$fsan);
		$profile = $conn->get_profile($trap['subs_id']);
		$olt_ip=$trap['olt_ip'];
		$olt_slot=$trap['olt_slot'];
		$olt_port=$trap['olt_port'];
		$fsan=$trap['subs_id'];
		##############
		print_r($trap);
		echo "profile $profile\n";
		###############
		if(!$profile){
			syslog(LOG_INFO,"main: invalid/NO profile for ".$trap['subs_id']." nothing to do yet.. for fsan: ".$fsan);
			continue;		
		}	
		syslog(LOG_INFO, "main: provisioning ".$trap['subs_id']." profile: $profile"." for fsan: ".$fsan);
		$vlan = $GLOBALS['global_config']['vlans'][$olt_ip];
	    $profile = $GLOBALS['global_config']['profiles'][$profile]['down'];			
		echo "params $olt_ip $olt_slot $olt_port $fsan $profile $vlan\n";
		syslog(LOG_INFO,"main: IP: ".$olt_ip." - SLOT:".$olt_slot."-PORT:".$olt_port."-FSAN:".$fsan."-PROFILE:".$profile."-VLAN:".$vlan." for fsan: ".$fsan);
		system("/var/www/html/provisioning_ftth_ver6/utils/olt_telnet_add.pl $olt_ip $olt_slot $olt_port $fsan $profile $vlan", $onu_id);
	 	syslog(LOG_INFO,"ONU_ID:".$onu_id." for fsan: ".$fsan);
		if($onu_id){
			syslog(LOG_INFO,"main: ONU_ID for ".$trap['subs_id']." was ".$onu_id." for fsan: ".$fsan);
			syslog(LOG_INFO,"main: provisioning for ".$trap['subs_id']." was OK for fsan: ".$fsan);			
			if($conn->update_subscriber($trap['subs_id'])){
				syslog(LOG_INFO,"main: subscriber table was updated for ".$trap['subs_id']." for fsan: ".$fsan);
			}
			else{
				syslog(LOG_ERR,"main: subscriber table was NOT updated for ".$trap['subs_id']." for fsan: ".$fsan);
			}
			if($conn->update_subscriber_info($trap['olt_ip'],
				$trap['olt_slot'],
				$trap['olt_port'],
				$onu_id,
				$trap['subs_id'],
				$profile,
				$profile)){	
				syslog(LOG_INFO,"main: subscriber_info table was updated for ".$trap['subs_id']." for fsan: ".$fsan);
			}
			else{
				syslog(LOG_ERR,"main: subscriber_inf table wasNOT updated for ".$trap['subs_id']." for fsan: ".$fsan);
			}
				
			if($conn->delete_action_trap($trap['trap_id'])){
				syslog(LOG_INFO,"main: trap_id ".$trap['trap_id']." was deleted for fsan: ".$fsan);
			}
			else{
				syslog(LOG_ERR,"main: trap_id ".$trap['trap_id']." was NOT deleted for fsan: ".$fsan);
			}

		}
		else{
			syslog(LOG_ERR,"main: provisioning for ".$trap['subs_id']." could not be done for fsan: ".$fsan);
            if($conn->update_trap_action($trap['trap_id'])){
                syslog(LOG_INFO,'main: nok: trap_action was updated for '.$trap['subs_id']." for fsan: ".$fsan);
            }else{
                syslog(LOG_ERR,'main: nok: trap_action was NOT updated for '.$trap['subs_id']." for fsan: ".$fsan);
            }
		}	
	}//main_foreach
	unset($conn);
} catch (Exception $e) {
    syslog(LOG_ERR, $e->getMessage() .' ('. $e->getCode() .')'." for fsan: ".$fsan);
}
closelog();
?>