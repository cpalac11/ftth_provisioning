#!/usr/bin/php
<?php
/* Updates
 * \Authors 
 * 		Fernando Munevar fmunevar@directvla.com.co
 * 		Christian Palacios cpalac11@directvla.com.co
 * 
 * Configurado en snmptrap.config es proceso que ejecuta al llamado de la OLT
 * 
 */
error_reporting(0);
openlog('Provisioning_ftth', LOG_ODELAY, LOG_LOCAL7);
########################################################################
########################INIT_PROCESS#####################################
try {
	include('class/parser.class.php');
	include('class/mysql_connection.class.php');
	/// Load/Parse in the global configuration file
	//$global_config = @parse_ini_file('global_config.ini', true);	
	/// Make it globally accessible
	//$GLOBALS['global_config'] = $global_config;	
	$trap = new Parser();
	$trap_type = $trap -> get_trap_type();	
	if($trap_type == 1){
		$olt_ip=$trap->get_olt_ip();
		$olt_slot=$trap->get_olt_slot(); 
		$olt_port=$trap->get_olt_port();
		$subs_id=$trap->get_subs_id();
		unset($trap);
		syslog(LOG_NOTICE,"trap_rcv: from $olt_ip for $subs_id at slot: $olt_slot and port: $olt_port");
		$conn = new db_connection();
		$provisioned = $conn->is_provisioned($subs_id);
		if($provisioned){
			syslog(LOG_NOTICE,"trap_rcv: $subs_id is not provisioned ... adding trap_action");
		}
		else{
			syslog(LOG_NOTICE,"trap_rcv: $subs_id is already provisioned ...lets check iface");
			if($conn->check_iface($olt_ip,$olt_slot,$olt_port,$subs_id)){							
				syslog(LOG_NOTICE,"trap_rcv: $subs_id iface is ok , nothing to do");
				exit;
			}
			else{
				if($conn->check_action_reconfig($subs_id,$olt_ip,$olt_slot,$olt_port)){
					syslog(LOG_NOTICE,"trap_rcv: reconf for $subs_id already exist ...nothing to do");
				}
				else{
					if($conn->add_action_reconfig($subs_id,$olt_ip,$olt_slot,$olt_port)){
						syslog(LOG_NOTICE,"trap_rcv: reconfig was addded for subs : $subs_id");
					}
					else{
						syslog(LOG_ERR,"trap_rcv: add reconfig trap action for : $subs_id fails");
					}
				}
				exit;
			}			
		}
        if($conn->check_action_config($subs_id,$olt_ip,$olt_slot,$olt_port)){
        	syslog(LOG_NOTICE,"trap_rcv: $subs_id already exist ...nothing to do");
        }
		else{
            if($conn->add_action_config($subs_id,$olt_ip,$olt_slot,$olt_port)){
                syslog(LOG_NOTICE,"trap_rcv: action was addded for subs : $subs_id");
            }
			else{
                syslog(LOG_ERR,"trap_rcv: adding trap action for : $subs_id fails");
            }
        }
		unset($conn);
	}
	else{
		unset($trap);
		exit;
	}
}
catch(Exception $e){
    syslog(LOG_ERR, $e->getMessage() .' ('. $e->getCode() .')');
}
closelog();
?>