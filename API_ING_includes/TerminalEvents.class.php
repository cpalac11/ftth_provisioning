<?php
/*
 Event Files Class
*/
 set_time_limit(0);
require_once("/var/www/html/API_ING_includes/enviroment.php");
require_once("/var/www/html/API_ING_includes/Database.class.php");

class terminalEvents {

	public $evtFilesPath = "";

	private $db = "";
	
	// opciones de Curl para transferencia de archivos
	private $proxyIp = '172.27.5.130';
	private $proxyPort = 8081;
	private $timeout = 30; 

	private $arrEventsIds = array(21101,22302,22307,21102,101); // array de eventId que se van a descargar del terminal


	public function __construct() {
		$this->db = new Database();
		$this->db->changeD1(); // uso INGProvisioningD1 / _test

	}

	public function getEventFileMetadataFromDb($esn) {

		$sql = "SELECT ";



	}

	public function getSubscribersBetween($dateFrom, $dateTo) {
		$sql = "SELECT s.id as customerId, s.deviceSerialNumber, SUBSTR(s.deviceSerialNumber,5,8) as esn, a.activationDate from Subscriber s, (select id as customerId, min(updated_at) as activationDate from "._MYSQL_DB.".Subscriber_log group by id having (activationDate between '$dateFrom 00:00:00' and '$dateTo 23:59:59')) as a where s.id = a.customerId;";
		$SubscribersBetween = array();
		$SubscribersBetween = $this->db->getRows($sql);
		//print_r($sql);

		if($this->db->count > 0) { 
			return $SubscribersBetween;
		} else {
			return array();
			}
	}


	function getEvtFileIndex($esn) {
		echo "Buscando archivos Log para $esn\n";
		$url = "http://esn".$esn.".terminal.jww.jupiter.net/cgi-bin/command.cgi?CommandStr=_COM_EVENT_LOG_DISPLAY";
		$links = $this->getUrl($url);
		$links = str_replace("\n","",$links);
		preg_match_all("/(evt[0-9]?\.csv)/", $links, $matches);
		return array_unique($matches[0]);
	}


	function getTerminalEvents($esn, $file, $arrEventsIds = null) {

		if(is_null($arrEventsIds)) {
			$arrEventsIds = $this->arrEventsIds;
			}

		$url = "http://esn".$esn.".terminal.jww.jupiter.net/cgi-bin/command.cgi?Command=419";

		$ch = curl_init();
		
		$eventsIds = ", " . implode(" ,|, ", $arrEventsIds) . " ,";
		$filename = "/fl0/evtlogs/$file";
		$postFields = "mytextarea=$eventsIds&opt_fname=$filename";
		$proxy = $this->proxyIp . ':' . $this->proxyPort;

		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$postFields);
		curl_setopt($ch, CURLOPT_PROXY, $proxy);
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt( $ch, CURLOPT_HEADER, false );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,$this->timeout); 
		curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);
		//curl_setopt($ch, CURLOPT_VERBOSE, true);


		$data = curl_exec($ch);

		if( curl_getinfo($ch, CURLINFO_HTTP_CODE) == 200 ) {
			
		     curl_close($ch);
		     return array_slice(explode("\n",str_replace($filename,"\n",strip_tags($data))),2);

			} else {
				curl_close($ch);
  				return array();
			}
		}// end getTerminalEvents

	 

	public function parseAndSave($customerId, $deviceSerialNumber, $esn, $file, $arrEvents = array()) {

		$parseDate = strftime("%Y-%m-%d %H:%M:%S",strtotime("NOW"));
		$camposToString = "";
		$values = array();
				
		foreach($arrEvents as $event) {
			// match
			$campos = array();
			$campos = explode(",",$event);
			if(!preg_match("/[0-9]{2}\/[0-9]{2}\/[0-9]{4} [0-9]{2}:[0-9]{2}:[0-9]{2}/",trim($campos[0]))) { continue; } // no es linea de log
			$camposToString .= implode(",",$campos);

				$gmtTime = strftime("%Y-%m-%d %H:%M:%S",strtotime(trim($campos[0])));
				$eventId = trim($campos[1]);
				$severity = trim($campos[2]);
				$reason = trim($campos[3]);
				$eventMessage = trim($campos[4]);
				// junto los ultimos campos por si tienen coma
				if(sizeof($campos) > 5 ) {
					$eventMessage .= trim(implode(" | ", array_slice($campos,6)));
					}

			$values[] = "($customerId,'$deviceSerialNumber','$esn', '$file', '$parseDate', ".strlen($camposToString).", '$gmtTime', '$eventId', '$severity', '$reason', '$eventMessage')";
			// print_r($campos);
			}
			$sql = "INSERT INTO TerminalEventsFiltered (customerId, deviceSerialNumber, esn, filename, parseDate, bytes, gmtTime, eventId, severity, reason, eventMessage) VALUES " . implode(",\n",$values) . " ON DUPLICATE KEY UPDATE parseDate = '$parseDate'";

			if(sizeof($values) > 0 ) { // hay datos para guardar
				$this->db->insert($sql);

			}
		return $this->db->count;
		


	}

}


?>