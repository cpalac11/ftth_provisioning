<?php
/*! \class API_ING_Provisioning_ws_client
 *  \brief Interface con NMS Provisioning (DTV)
 *  \author juebel@gmail.com
 *
 * Implementa todas las llamadas a Provisioning utilizadas en DTV via SOAP
 */

class API_ING_Provisioning_SYNC_ws_client extends wsClient {
    
    public function __construct() {
        // $url = __API_ING_Provisioning_ws_url;
        $url = _API_ING_Provisioning_SYNC_URL;
        parent::__construct($url);
        

	    }

    public function getData($arrAction = array()) {
        return parent::getWsData(key($arrAction), $arrAction);
    }

    

   
    
} // end class
?>