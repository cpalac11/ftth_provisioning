<?php
/*! \class Provisioning
 *  \brief Interface con NMS Provisioning (DTV)
 *  \author juebel@gmail.com
 *
 * Implementa todas las llamadas a Provisioning utilizadas en DTV via SOAP
 */

class API_ING_Provisioning extends SoapClient {
    
    var $url = "http://172.20.4.211/API_ING_Provisioning/index.php?wsdl";    /**< URL del contrato WS. */
    var $default_socket_timeout = 30;                               /**< Tiempo de espera para la conección al servidor */
    var $soapClient = null;                                         /**< Objeto SoapClient */
    var $response = "";                                             /**< XML de respuesta a la última llamada al WS */
    var $arrResponse = array();                                     /**< Array asociativo con tags y valores de la respuesta a la última llamada al WS */

    var $response_status      = false;                              /**< Respuesta true o false a la última llamada al WS */
    var $response_code      = 200;                                  /**< Código numérico de la respuesta a la última llamada al WS */
    var $response_message   = "";                                   /**< Texto de respuesta a la última llamada al WS */


    /** 
     *
     *  @param
     *  @brief Constructor. Crea la onexión al WS y establece sus parámetros
     *  @return boolean
     *
     */
    public function __construct() {
        
        set_time_limit($this->default_socket_timeout);
        ini_set("default_socket_timeout", $this->default_socket_timeout);
        
        $this->response_status = true;
        
        $soap_options = array(
                'trace' => 1,
                'connection_timeout' => $this->default_socket_timeout,
                'compression' => true,
                //'local_cert' => '',
                //'passphrase' => '',
                'encoding' => 'utf-8',
                'exceptions' => true,
                // 'ssl_method' => SOAP_SSL_METHOD_SSLv23,
                'features' => SOAP_SINGLE_ELEMENT_ARRAYS

            );

        try {
        $this->soapClient = new SoapClient($this->url, $soap_options);
        }
        catch (SoapFault $e) {
            
            $this->response_status  = false;
            $this->response_code    = $e->faultcode;
            $this->response_message = $e->faultstring;

        }
        return $this->response_status;
    }

    /** 
     *
     *  @param $soapXml
     *  @brief Convierte el XML Soap de respuesta a un array asociativo tag=>valor
     *  @brief Elimina los cmpos vacíos
     *  @return array
     *
     */
    public function return2array($soapXml) {

        $response = preg_replace("/(<\/?)(\w+)\:([^>]*>)/", "$1$2$3", $soapXml);
        $xml = new SimpleXMLElement($response);
        $body = $xml->xpath('//return')[0];
        $array = json_decode(json_encode((array)$body), TRUE);
        $return = array();

        foreach ($array as $keyParametro => $valor) {
            //$parametro = str_replace($this->namespace_tag, '', $keyParametro);
            //echo "$parametro=$valor - ";
            if(empty($valor)) { $return[$keyParametro] = ""; }
            else { $return[$keyParametro] = (string)$valor; }

        }

        return $return;

    }

    /** 
     *
     *  @param $subscriberIdentity string
     *  @param $servicePlan string
     *  @param $technology string
     *  @brief Construye y llama  al método Provisioning.can_create
     *  @return boolean
     *
     */
    public function Activate($subscriberIdentity = null, $customerID = null, $deviceSerialNumber = null, $servicePlan = null, $invoiceProfile = null, $technology = null) {
       
        $action='Activate';

           try {
                $this->soapClient->$action($subscriberIdentity, $customerID, $deviceSerialNumber, $servicePlan, $invoiceProfile, $technology);

                $this->response_status = true;
                $this->response = $this->soapClient->__getLastResponse();
                $this->arrResponse = $this->return2array($this->response);
                
                }
                
                catch(SoapFault $e) {
                    $this->response_status = false;
                    $this->response_code    = $e->faultcode;
                    $this->response_message   = $e->faultstring;

                 }
            //print_r($this);
            return $this->response_status;
       }

    
    // public function AddTokens() { }
    // public function Deactivate() { }
    // public function Move() { }
    // public function Swap() { }
     public function Suspend($customerID, $capacity = null) { 
        $action='Suspend';

           try {
                $this->soapClient->$action(array('customerID' => $customerID, 'capacity' => $capacity));

                $this->response_status = true;
                $this->response = $this->soapClient->__getLastResponse();
                $this->arrResponse = $this->return2array($this->response);
                
                }
                
                catch(SoapFault $e) {
                    $this->response_status = false;
                    $this->response_code    = $e->faultcode;
                    $this->response_message   = $e->faultstring;

                 }
            //print_r($this);
            return $this->response_status;

     }
    // public function Unsuspend() { }
    // public function Update() { }
    
} // end class

?>