<?php

/*! \class wsClient
 *  \brief Cliente SOAP adaptado para llamadas a API_ING
 *  \author Dario Cabral
 *
 * @todo: sacar toda referencia a logs fuera de la clase wsClient y utilizar un mÃ©todo de log propio de API_ING
 */

class wsClient
{
	public $error = false;				/**< indicador de error */
	public $errorMessage = '';			/**< Mensaje de error */

	private $DS = DIRECTORY_SEPARATOR;	/**< Separador de directorio */
	private $doLog = false;				/**< Establece Log segun true o false */
	private $logToFile = true;			/**< Establece Log hacia archivo segun true o false */
	private $logToPhpErrorFile = false;	/**< Establece Log a archiv de errores php segun true o false */
	private $logToScreen = false;		/**< Establece Log en pantalla segun true o false */
	private $defaultError = 'Occurred an error during the operation, try again. If error persists, contact us.';					/**< Error por defecto */

	private $wsUrl = null;				/**< Url del WS a consumir */

	protected $logConfig = array(
		'request_parameters'		=> true,
		'request_headers'			=> true,
		'request_data'				=> false,
		'request_body'				=> true,
		'response'					=> true,
		'response_headers'			=> true,
		'response_body'				=> true,
		'forms_account_creation'	=> true
	);									/**< Parametros para logging */

	protected $headerLastResponse;		/**< Headers del ultimo response */
	protected $bodyLastResponse;		/**< Body del ultimo response */
	public $infoLastResponse;			/**< Info del ultimo response */
	public $lastHttpCode;				/**< Codigo HTTP del ultimo response */


	/**
     *
     *  @param  url string
     *  @param  paremanters array
     *  @brief Constructor. Establece la url a consumir y los parametros de logging
     *  @return void
     *
     */
	public function __construct($wsUrl, $parameters = array()) {
		$this->wsUrl = $wsUrl;

		if (array_key_exists('log', $parameters)) {
			$this->doLog = $parameters['log'];
		}

		if (array_key_exists('log_custom_file', $parameters)) {
			$this->logToFile = $parameters['log_custom_file'];
		}

		if (array_key_exists('log_to_php_file', $parameters)) {
			$this->logToPhpErrorFile = $parameters['log_to_php_file'];
		}

		if (array_key_exists('logConfig', $parameters) && !empty($parameters['logConfig'])) {
			$this->logConfig = array_merge($this->logConfig, $parameters['logConfig']);
		}
	}

	/**
     *
     *  @param  method string
     *  @param  data array
     *  @brief Ejecuta peticion al WS y devuelve el resultado en formato JSON
     *  @return string
     *
     */
	function getWsData($method, $data = null) {
		$message = "";

		$wsURL = $this->wsUrl;
		
		$logSettings = $this->logConfig;
		$log = $this->doLog;

		if ($log && $logSettings['request_data'] && !empty($data)) {
			$this->log("WebService 'makeRequest' -> Data to send:\n" . print_r($data, true));
		}

		$xdebugEnabled = function_exists('xdebug_is_enabled') ? xdebug_is_enabled() : false;

		if ($xdebugEnabled && function_exists('xdebug_disable')) {
			xdebug_disable();
		}

		$context = stream_context_create(array(
			'http' => array(
				'header' => "User-Agent: PHP-SOAP\r\n"
			)
		));

		$response = null;
		$header = array();
		$options = array();


		try {
			$soapClient = @new direcTVSoapClient($wsURL, array(
				'connection_timeout'	=> 60,
				'trace'					=> true,
				'exceptions'			=> true,
				'soap_version'			=> SOAP_1_1,
				'keep_alive'			=> true,
				'encoding'				=> 'UTF-8',
				'compression'			=> SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP | SOAP_COMPRESSION_DEFLATE,
				'cache_wsdl'			=> WSDL_CACHE_NONE,
				'stream_context'		=> $context
				//'ssl_method'			=>  SOAP_SSL_METHOD_TLS
			));



			$response = $soapClient->__soapCall($method, $data, $options, $header);
			//$response = $soapClient->{$method}($data);

		} catch (SoapFault $e) {
			$errorMessage = "Error while trying to connect to the Web Service. SoapFault code: " . $e->faultcode . " - Message: " . $e->faultstring;
			$this->log($errorMessage);

		} catch (Exception $e) {
			$errorMessage = "Error while trying to connect to the Web Service. Exception message: " . $e->getCode() . " - Message: " . $e->getMessage();
			$this->log($errorMessage);
		}

		if ($xdebugEnabled && function_exists('xdebug_enable')) {
			xdebug_enable();
		}

		if (isset($soapClient) && is_object($soapClient)) {
			if ($log && $logSettings['request_headers']) {
				$this->log("WebService -> Request headers:\n" . print_r($soapClient->__getLastRequestHeaders(), true));
			}

			if ($log && $logSettings['request_body']) {
				$this->log("WebService -> Request body:\n" . print_r($soapClient->__getLastRequest(), true));
			}

			if ($log && $logSettings['response_headers']) {
				$this->log("WebService -> Response headers:\n" . print_r($soapClient->__getLastResponseHeaders(), true) . '');
			}

			if ($log && $logSettings['response_body']) {
				$this->log("WebService -> Response body:\n" . print_r($soapClient->__getLastResponse(), true) . "");
			}
		}

		if (is_object($response)) {
			$response = json_decode(json_encode($response), true);
		}

		return $response;
	}

	/**
     *
     *  @param  message string
     *  @param  type string
     *  @brief Logging de message
     *  @return void
     *
     */

	protected function log($message, $type = null) {
		$message = !is_string($message) ? print_r($message, true) : $message;

		if (((empty($type) && $this->doLog) || $type == 'error') && $this->logToPhpErrorFile) {
			error_log($message);
		}

		if ((empty($type) && $this->logToFile) || $type == 'log') {
			$logFile = dirname(__FILE__) . $this->DS . 'logs' . $this->DS . 'ws.log';	// 'ws_' . date('Ymd') . '.log';
			$message = '[' . date('Y-m-d H:i:s') . "] - {$message}\r\n";

			$this->saveFileContent($logFile, $message, true);
		}

		if ($this->logToScreen) {
			echo '<pre>';
			print_r($message);
			echo '</pre>';
		}
	}

	/**
     *
     *  @param  filePath string
     *  @param  content string
     *  @param  addToEndOfFile boolean
     *  @param  lockFile boolean
     *  @param  forceLock boolean
     *  @brief Logging. Guarda mensaje en un archivo
     *  @return boolean
     *
     */
	protected function saveFileContent($filePath, $content, $addToEndOfFile = false, $lockFile = false, $forceLock = false) {
		$saved = false;
		$openType = $addToEndOfFile ? 'a' : 'w+';
		$fileHandler = @fopen($filePath, $openType);

		if ($fileHandler !== false) {
			if ($lockFile && $forceLock) {
				do {
					$canWrite = @flock($fileHandler, LOCK_EX);

					// Si no se obtiene el bloqueo, hace un sleep de entre 0 y 100 milisegundos, esto es para evitar colisiones y sobrecarga en la CPU
					if (!$canWrite) {
						usleep(round(rand(0, 100) * 1000));
					}

					// Intento hasta que pueda bloquear el archivo
				} while (!$canWrite);

			} else if ($lockFile && !$forceLock) {
				$canWrite = @flock($fileHandler, LOCK_EX | LOCK_NB);

				if (!$canWrite) {
					return false;
				}
			}

			if (!$addToEndOfFile) {
				@rewind($fileHandler);
			}

			@fwrite($fileHandler, $content);
			@fflush($fileHandler);

			if (!$addToEndOfFile) {
				@ftruncate($fileHandler, @ftell($fileHandler));
			}

			if ($lockFile) {
				@flock($handle, LOCK_UN);
			}

			@fclose($fileHandler);
			$saved = true;
		}

		return $saved;
	}
}

/*! \class direcTVSoapClient
 *  \brief Cliente SOAP adaptado para llamadas a API_ING, extension de SoapClient PHP
 *  \author Dario Cabral
 *
 */
class direcTVSoapClient extends SoapClient
{


	/**
     *
     *  @param  url string
     *  @param  options array
     *  @brief Constructor. Establece la url a consumir y los parametros de conecciÃ³n
     *  @return void
     *
     */
	public function __construct($wsdl, $options = null) {
		parent::__construct($wsdl, $options);
	}

	/**
     *
     *  @param  request string
     *  @param  location string
     *  @param  action string
     *  @param  version string
     *  @param  one_way boolean
     *  @param  options array
     *  @brief Redefine el mÃ©todo __doRequest de SoapClient, adaptando los namespaces a los utilizados en API_ING
     *  @return void
     *
     */
	public function __doRequest($request, $location, $action, $version, $one_way = 0) {
		$replacements = array(
			'SOAP-ENV'	=> 'soapenv',
			'ns1'		=> 'bro'
		);

		foreach ($replacements as $original => $changed) {
			$request = preg_replace("/{$original}(:|=)/", "{$changed}$1", $request);
		}

		//error_log($request);

		return parent::__doRequest($request, $location, $action, $version, $one_way);
	}
}
