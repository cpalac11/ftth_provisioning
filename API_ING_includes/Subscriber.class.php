<?php
/*! \class Subscriber
 *  \brief Clase ORM de la tabla Subscriber
 *  \brief Gestiona los registros de Subscribers, tokens y acciones pendientes
 *  \author juebel@gmail.com
 *
 */
class Subscriber extends Database {

    // propiedades correspondientes a los campos de la tabla
    var $subscriber_id = null;
    var $SubscriberIdentity = "NULL";
    var $id = "NULL";
    var $servicePlanId = "NULL";
    var $InvoiceProfile_id = "NULL";
    var $deviceSerialNumber = "";
    var $technology = "";
    var $status_id = 2;
    var $updated_at = null;
	var $checkDigit = "NULL";
    var $blockBySaldo = 0;
	var $blockByOffPeak = 0;

    /** 
     *
     *  @param  new boolean
     *  @brief Constructor. Crea la conexión a la BBDD.
     *  @return void
     *
     */
    function __construct() {
        parent::__construct();

    }

    /** 
     *
     *  @param  oR Object
     *  @brief Mapea el resultado de la consulta a BBDD a las propiedades del objeto
     *  @return boolean
     *
     */
    private function mapResultsToObjet($oR) {
        if(!is_object($oR)) { return false; }
        $this->subscriber_id = ($oR->subscriber_id == NULL) ? 'NULL' : $oR->subscriber_id;
        $this->SubscriberIdentity = ($oR->SubscriberIdentity == NULL) ? 'NULL' : $oR->SubscriberIdentity;
        $this->id = ($oR->id == NULL) ? 'NULL' : $oR->id;
        $this->servicePlanId = ($oR->servicePlanId == NULL) ? 'NULL' : $oR->servicePlanId;
        $this->InvoiceProfile_id = ($oR->InvoiceProfile_id == NULL) ? 'NULL' : $oR->InvoiceProfile_id;
        $this->deviceSerialNumber = ($oR->deviceSerialNumber == NULL) ? 'NULL' : $oR->deviceSerialNumber;
        $this->technology = $oR->technology;
        $this->status_id = $oR->status_id;
        $this->updated_at = $oR->updated_at;
		$this->checkDigit = ($oR->checkDigit == NULL) ? 'NULL' : $oR->checkDigit;
        $this->blockBySaldo = ($oR->blockBySaldo == NULL) ? 'NULL' : $oR->blockBySaldo;
		$this->blockByOffPeak = ($oR->blockByOffPeak == NULL) ? 'NULL' : $oR->blockByOffPeak;
        return true;
    }

    public function FTTH_HWTC ($SubscriberIdentity, $tipo){
        $date = date('Y-m-d');
        $ip = $_SERVER['REMOTE_ADDR'];
        $sql = "INSERT INTO provisioningPROD.log (subscriber_identity, request, response, fecha, ip) VALUES ('$SubscriberIdentity', '$tipo', 'Ok', '$date', '$ip')";
        $result = $this->makesql($sql, $tipo);
        return $result;
    }

    /** 
     *
     *  @param  subscriber_id integer
     *  @brief Obtiene el Subscriber buscando por el campo subscriber_id (identificador de registro)
     *  @return boolean
     *
     */
    public function getBySubscriberId($subscriber_id) {
        $sql = "SELECT * from Subscriber WHERE subscriber_id = '$subscriber_id' ORDER BY subscriber_id DESC LIMIT 1";
		$oR = $this->getRow($sql);
        $this->mapResultsToObjet($oR);
    }

    /** 
     *
     *  @param  id integer
     *  @brief Obtiene el Subscriber buscando por el campo id (identificador de cliente, equivalente a "CustomerID" para SATELITAL)
     *  @return boolean
     *
     */
    public function getById($id) {
        $sql = "SELECT * from Subscriber WHERE id = '$id' ORDER BY subscriber_id DESC LIMIT 1";
        $oR = $this->getRow($sql);
        $this->mapResultsToObjet($oR);
		
    }

    /** 
     *
     *  @param  SubscriberIdentity string
     *  @brief Obtiene el Subscriber buscando por el campo SubscriberIdentity (identificador de cliente, equivalente a "SubscriberIdentity" para FTTH, LTE, WIMAX)
     *  @return boolean
     *
     */
    public function getBySubscriberIdentity($SubscriberIdentity) {
        $sql = "SELECT * from Subscriber WHERE SubscriberIdentity = '$SubscriberIdentity' ORDER BY subscriber_id DESC LIMIT 1";
        $oR = $this->getRow($sql);
        $this->mapResultsToObjet($oR);        
    }

	/** 
     *
     *  @param  statusId integer
     *  @param  technology string
     *  @brief Obtiene todos los Subscribers con el status_id indicado
     *  @return array
     *
     */
    public function getByStatusId($statusId, $technology = 'SATELITAL') {
        $sql = "SELECT * from Subscriber WHERE status_id = '$statusId' and technology = '$technology'";
		
        $aoR = $this->getRows($sql);
		if($this->count <= 0) { return array(); }

		foreach ($aoR as $oR) {
			$this->mapResultsToObjet($oR);
			$arrSubscribers[] = $this;
			}
		return $arrSubscribers;
    }

	/** 
     *
     *  @param  statusId integer
     *  @param  technology string
     *  @brief Obtiene todos los Subscribers con el flag blockBySaldo conicidente con el parametro
     *  @return array
     *
     */
    public function getByBlockBySaldo($blockBySaldo = 0, $technology = 'SATELITAL') {
        $sql = "SELECT * from Subscriber WHERE blockBySaldo = $blockBySaldo and technology = '$technology'";
        
        $aoR = $this->getRows($sql);
        if($this->count <= 0) { return array(); }

        foreach ($aoR as $oR) {
            $this->mapResultsToObjet($oR);
            $arrSubscribers[] = $this;
            }
        return $arrSubscribers;
    }

    /** 
     *
     *  @param  statusId integer
     *  @param  technology string
     *  @brief Obtiene todos los Subscribers con el flag blockByOffPeak conicidente con el parametro
     *  @return array
     *
     */
    public function getByBlockByOffPeak($blockByOffPeak = 0, $technology = 'SATELITAL') {
        $sql = "SELECT * from Subscriber WHERE blockByOffPeak = $blockByOffPeak and technology = '$technology'";
        
        $aoR = $this->getRows($sql);
        if($this->count <= 0) { return array(); }

        foreach ($aoR as $oR) {
            $this->mapResultsToObjet($oR);
            $arrSubscribers[] = $this;
            }
        return $arrSubscribers;
    }

    /** 
     *
     *  @param  customerID integer
     *  @param  SubscriberIdentity string
     *  @brief Obtiene un Subscriber con el identificador indicado, sin importar tecnología.
     *  @brief Si no existe, lo crea y devuelve el registro creado como si lo hubiera encontrado
     *  @brief Sirve para buscar Subscribers sin saber si es satelital o de otra tecnología
     *  @return boolean
     *
     */
    public function getOrCreate($customerID = 'NULL', $SubscriberIdentity = 'NULL') {
		$query_SubscriberIdentity = ($SubscriberIdentity == "" || $SubscriberIdentity == 'NULL') ? "(SubscriberIdentity is NULL OR SubscriberIdentity = 'NULL' OR SubscriberIdentity = '')" : "SubscriberIdentity = '$SubscriberIdentity'";
        $query_customerID = ($customerID == "" || $customerID == 'NULL') ? 'id is NULL' : "id = $customerID";        
		$sql = "SELECT * from Subscriber WHERE $query_SubscriberIdentity AND $query_customerID";
        $oR = $this->getRow($sql);
        if($this->count == 0) {
            // lo creamos
            $this->SubscriberIdentity = $SubscriberIdentity;
            $this->id = ($customerID == "") ? 'NULL' : $customerID;
            $this->servicePlanId = 1;
            $this->status_id = 2;
            $this->save();
        } 
        else {
            // lo levantamos
            $this->mapResultsToObjet($oR);
        }        
    }

	/** 
     *
     *  @param  customerID integer
     *  @param  SubscriberIdentity string
     *  @brief Obtiene un Subscriber con el identificador indicado, sin importar tecnología.
     *  @brief Si no existe, no lo crea
     *  @return void|boolean
     *
     */
    public function getOrFail($customerID = 'NULL', $SubscriberIdentity = 'NULL') {
		$query_SubscriberIdentity = ($SubscriberIdentity == "" || $SubscriberIdentity == 'NULL') ? "(SubscriberIdentity is NULL OR SubscriberIdentity = 'NULL' OR SubscriberIdentity = '')" : "SubscriberIdentity = '$SubscriberIdentity'";
        $query_customerID = ($customerID == "" || $customerID == 'NULL') ? 'id is NULL' : "id = $customerID";        
		$sql = "SELECT * from Subscriber WHERE $query_SubscriberIdentity AND $query_customerID";
        
        $oR = $this->getRow($sql);
        //print_r($this->count);die();
		//print_r($sql);
        if($this->count == 0) {
            // no hay resultados!
        } else {
            // lo levantamos
            $this->mapResultsToObjet($oR);
        }        
    }


    
    /** 
     *
     *  @param  deviceSerialNumber string
     *  @brief Obtiene un subscriber buscandolo por el deviceSerialNumber dado
     *  @return boolean
     *
     */
    public function getByDeviceSerialNumber($deviceSerialNumber) {
        $sql = "SELECT * from Subscriber WHERE deviceSerialNumber = '$deviceSerialNumber' ORDER BY subscriber_id DESC LIMIT 1";
        $oR = $this->getRow($sql);
        $this->mapResultsToObjet($oR);        
    }

    /** 
     *
     *  @param  Token_id string
     *  @brief Agrega un Token a un Subscriber. Crea el registro de la relación en la tabla Subscribers_tokens
     *  @return boolean
     *
     */
    public function addToken($Token_id) {
        $sql = "INSERT INTO Subscriber_tokens (subscriber_id, Token_id, date, token_status) values ($this->subscriber_id, $Token_id, NOW(), 1)";
        $this->log($sql);
        $this->insert($sql);
    }

     /** 
     *
     *  @param  
     *  @brief Obtiene la suma de megas de los Tokens pertenecientes al Subscriber de la sesion ($this)
     *  @return integer
     *
     */
     public function getTokensSum() {

        $sql = "select sum(t.capacity) as TokensSum from Subscriber_tokens st join Tokens t ON st.Token_id = t.Token_id where st.subscriber_id = $this->subscriber_id and st.token_status = 1";
        $this->log($sql);
        //print_r($sql);

        return $this->getValue($sql);
    }

    /** 
     *
     *  @param  
     *  @brief Obtiene datos del último Token asignado a un Subscriber. Util para calculo de expiracion por fecha.
     *  @return integer
     *
     */
    public function getLastToken() {
        $sql = "select * from Subscriber_tokens where subscriber_id = $this->subscriber_id ORDER BY date desc limit 1";
        $this->log($sql);
        return $this->getRow($sql);
    }

    public function drainPreviousTokens() {
        $tokenSql = "UPDATE Subscriber_tokens SET token_status = 2 WHERE subscriber_id = ".$this->subscriber_id." AND token_status = 1";
        //print_r($tokenSql);
        $this->log($tokenSql);
        return $this->update($tokenSql);

    }

    /** 
     *
     *  @param  actionId integer
     *  @brief Agrega una acción pendiente para el Subscriber de la sesion ($this). Se utiliza para que el cron las finalice
     *  @brief actionId = 1: SwapDisallow.
     *  @brief actionId = 2: MoveDisallow.
     *  @return boolean
     *  @todo Crear tabla auxiliar de acciones pendientes para identificarlas
     *
     */
    public function addPendingAction($actionId) {
        // actionId 1 = Swap
        // actionId 2 = Move
        $sql = "INSERT INTO Actions_pending (customerId, actionId) VALUES ('".$this->id."',$actionId) ON DUPLICATE KEY UPDATE updated_at = NOW()";
        $this->log($sql);
        // print_r($sql);
        return $this->insert($sql);
        }

    /** 
     *
     *  @param  actionId integer
     *  @brief Elimina una acción pendiente para el Subscriber de la sesion ($this). La utiliza el cron, cuando realiza la acción
     *  @brief actionId = 1: SwapDisallow.
     *  @brief actionId = 2: MoveDisallow.
     *  @return boolean
     *
     */
    public function unlinkPendingAction($actionId) {
        // $sql = "INSERT INTO Actions_pending (customerId, actionId) VALUES ('.$this->id.',$actionId)";
        //$sql = "UPDATE Actions_pending SET actionId = 0 WHERE customerId = '".$this->id."' AND actionId = $actionId";
        $sql = "DELETE FROM Actions_pending WHERE customerId = '".$this->id."' AND actionId = $actionId";
        $this->log($sql);
        //print_r($sql);
        return $this->delete($sql);
        }

    /** 
     *
     *  @param  actionId integer
     *  @param  timeLapse integer
     *  @brief Obtiene todos los Subscribers con la accion pendiente indicada. Si se indica timeLapse, filtra las acciones pendientes que han superado el tiempo indicado en timeLapse EN MINUTOS
     *  @return array
     *
     */
    public function getByActionPendingId($actionId, $timeLapse = null) {
        $sqlTimeLapse = "";
        if(!is_null($timeLapse)) { $sqlTimeLapse = "AND updated_at <= DATE_SUB(NOW(),INTERVAL $timeLapse MINUTE)"; }
        
        $sql = "SELECT * FROM Subscriber WHERE id IN (SELECT customerId FROM Actions_pending WHERE actionId = $actionId $sqlTimeLapse);";
        //print_r($sql);
        $aoR = $this->getRows($sql);
        if($this->count <= 0) { return array(); }
        
        $arrSubscribers = array();
		// print_r($aoR);

        foreach ($aoR as $oR) {
            //echo "\count ++";
			
			$subs = new Subscriber();
			$subs->mapResultsToObjet($oR);
            array_push($arrSubscribers,$subs);
			unset($subs);
            }
		
        return $arrSubscribers;
    }

    public function isBlockedBySaldo() {
        
        if($this->blockBySaldo == 1) { return true; } 
        else { return false; }
    }

    public function isVip() {
        $sql = "SELECT cir FROM ServicePlan_extra WHERE subscriber_id = $this->subscriber_id LIMIT 1";
        
        if($this->getValue($sql)) { 
            return true;
            }
        else {
         return false;
            }
    }

    public function setAsVip() {
        $sql = "INSERT INTO ServicePlan_extra (subscriber_id, cir) VALUES ($this->subscriber_id,1)";
        $this->insert($sql);
    }

    public function revokeVip() {
        $sql = "DELETE FROM ServicePlan_extra WHERE subscriber_id = $this->subscriber_id";
        $this->insert($sql);
    }

    public function isBlockedByOffPeak() {
        
        if($this->blockByOffPeak == 1) { return true; } 
        else { return false; }
    }

    public function isActive() {
        
        if($this->status_id == 1) { return true; } 
                             else { return false; }
    }

    public function isDeactivated() {
        
        if($this->status_id == 2) { return true; } 
                             else { return false; }
    }

    public function isSuspended() {
        
        if($this->status_id == 3) { return true; } 
                             else { return false; }
    }




    /** 
     *
     *  @param  
     *  @brief Guarda un objeto Subscriber en la BBDD. Si ya existía, lo actualiza, si no existía lo crea
     *  @brief Guarda un registro en Subscriber_log si está modificando algún dato
     *  @return boolean
     *
     */

    public function save() {

        // defino si es nuevo o viejo por el subscriber_id
        
        if($this->subscriber_id) { 
            // ya exisitia, hago update
            // guardo log
            $this->save_log($this->subscriber_id);

            $sql = "UPDATE Subscriber SET 
                SubscriberIdentity = '".$this->SubscriberIdentity."', 
                id = ".$this->id.", 
                InvoiceProfile_id = ".$this->InvoiceProfile_id.", 
                servicePlanId = ".$this->servicePlanId.", 
                deviceSerialNumber = '".$this->deviceSerialNumber."', 
                technology = '".$this->technology."', 
                status_id = '".$this->status_id."', 
                checkDigit = ".$this->checkDigit.", 
                blockBySaldo = ".$this->blockBySaldo.",
				blockByOffPeak = ".$this->blockByOffPeak.",
                updated_at = NOW() 
            WHERE 
                subscriber_id = '".$this->subscriber_id."';";


            $this->update($sql);
            $this->log($sql);

            //print_r($sql);
            // retoma el objeto para repopular todas las propiedades
            $this->getBySubscriberId($this->subscriber_id);


        } else {
            // no existia, hago insert y actualizo el subscriber_id 

            $sql = "INSERT INTO  Subscriber (SubscriberIdentity, id, InvoiceProfile_id, servicePlanId, deviceSerialNumber, technology, status_id, updated_at) VALUES ('".$this->SubscriberIdentity."', ".$this->id.",".$this->InvoiceProfile_id.", ".$this->servicePlanId.", '".$this->deviceSerialNumber."', '".$this->technology."', '".$this->status_id."', NOW())";
            $this->insert($sql);

            // retoma el objeto para repopular todas las propiedades
            $this->subscriber_id = $this->last_insert_id;
            $this->getBySubscriberId($this->subscriber_id);
        }
    }

    /** 
     *
     *  @param  subscriber_id integer
     *  @brief Crea un registro en Subscriber_log del subscriber indicado. Se utiliza para crear el histórico de todos los cambios en cualquier campo de un Subscriber
     *  @return array
     *
     */

    public function save_log($subscriber_id) {
        $sql = "INSERT INTO Subscriber_log (subscriber_id, SubscriberIdentity, id, servicePlanId, InvoiceProfile_id, deviceSerialNumber, technology, status_id, checkDigit, updated_at, blockBySaldo, blockByOffPeak) SELECT subscriber_id, SubscriberIdentity, id, servicePlanId, InvoiceProfile_id, deviceSerialNumber, technology, status_id, checkDigit, updated_at, blockBySaldo, blockByOffPeak FROM Subscriber WHERE subscriber_id = '$subscriber_id'";
		//print_r($sql);

        Utils::debug($sql);
        $this->insert($sql);

    }

} // end class

?>