<?php

/*! \class Ip
 *  \brief Gestiona los rangos de IPs a asignar a Subscribers
 *  \brief Reemplaza al DHCP de Huges hasta tanto funcione
 *  \author juebel@gmail.com
 *
 * @todo Reintentar asignar Ips recursivamente para que nunca devuelva una ya utilizada
 */

class Ip extends Database {

	public $id = NULL;                 /**< Propiedad ID del objeto Ip devuelto. identificador de registro en la BBDD */
	public $IPv4 = "";                 /**< Propiedad IPv4 del objeto Ip devuelto. Ip version 4 inicial del rango a asignar */
    public $IPv4Plength = "";          /**< Propiedad IPv4Plength del objeto Ip devuelto. Longitud del rango ips version 4 a asignar */
	public $IPv4Block = "";            /**< Propiedad IPv4Block del objeto Ip devuelto. Bloque completo de rango de ips a asignar */
	public $IPv6 = "";                 /**< Propiedad IPv6 del objeto Ip devuelto. Ip version 6 inicial del rango a asignar */
    public $IPv6Plength = "";          /**< Propiedad IPv6Plength del objeto Ip devuelto. Longitud del rango ips version 6 a asignar */
	public $IPv6Block = "";		       /**< Propiedad IPv6Block del objeto Ip devuelto. Bloque completo de rango de ips a asignar */
	public $customerID = NULL;         /**< Propiedad customerID del objeto Ip devuelto. CustomerID al que se le asigna el rango de IPs */
	
	/** 
     *
     *  @param  new boolean
     *  @brief Constructor. Crea la conexión a la BBDD.
     *  @brief en caso de recibir el parametro $new en true, busca el promer rango de IPs disponible.
     *  @return void|Object
     *
     */

    function __construct($new = true) {
        parent::__construct();
        if($new) { return $this->getFreeIp(); }

    }

    /** 
     *
     *  @param  oR Object
     *  @brief Mapea el resultado de la consulta a BBDD a las propiedades del objeto
     *  @return boolean
     *
     */
    function mapResult($oR) {
    	if(!is_object($oR)) { return false; }
		$this->id = ($oR->id == NULL) ? 'NULL' : $oR->id;
		$this->IPv4 = $oR->IPv4;
        $this->IPv4Plength = $oR->IPv4Plength;
        $this->IPv4Block = $this->IPv4.'/'.$this->IPv4Plength;
        $this->IPv6 = $oR->IPv6;
        $this->IPv6Plength = $oR->IPv6Plength;
		$this->IPv6Block = $this->IPv6.'/'.$this->IPv6Plength;
		$this->customerID = ($oR->customerID == NULL) ? 'NULL' : $oR->customerID;
		return true;

    }

    /** 
     *
     *  @param  customerID integer
     *  @brief Obtiene el rango de IPs asignado a un CustomerID
     *  @return Object
     *
     */
    function getCustomerIDIp($customerID) {
            $sql = "SELECT * from ips WHERE customerID = $customerID LIMIT 1";
            $oR = $this->getRow($sql);
            return $this->mapResult($oR);
    }

    /** 
     *
     *  @param  IPv4 string
     *  @brief Obtiene un objeto Ip con una IP Version 4 dada
     *  @return Object
     *
     */
    function getIPv4($IPv4) {
            $sql = "SELECT * from ips WHERE IPv4 = '$IPv4' LIMIT 1";
            $oR = $this->getRow($sql);
            return $this->mapResult($oR);
    }

    /** 
     *
     *  @param  
     *  @brief Obtiene el primer rango de IPs disponible
     *  @return Object
     *
     */
    function getFreeIp() {
    		$sql = "SELECT * from ips WHERE customerID IS NULL order by id asc limit 1";
    		$oR = $this->getRow($sql);
    		return $this->mapResult($oR);
    }

    /** 
     *
     *  @param  customerID integer
     *  @brief Asocia un rango de IPs a un customer ID. Al hacerlo queda bloqueado para no volver a ser utilizado.
     *  @return boolean
     *
     */
    function linkTo($customerID) {
    	$this->customerID = $customerID;

        // cierro todos los períodos anteriores
        $sql1 = "UPDATE ipsHistorial SET hasta = NOW() WHERE ( CustomerID = ".$this->customerID." AND hasta IS NULL);";
        $this->update($sql1);

        // abro un nuevo período
        $sql2 = "INSERT INTO ipsHistorial (CustomerID,  staticIpv4Subnet ,desde) VALUES (".$this->customerID.", '".$this->IPv4Block."',NOW());";
    	$this->insert($sql2);
        return $this->save();

    }

    /** 
     *
     *  @param  
     *  @brief Guarda un objeto Ip en la base de datos. Actualmente solo modifica el campo "customerID"
     *  @return boolean
     *
     */
    function save() {
    		$sql = "UPDATE ips SET customerID = $this->customerID WHERE id = $this->id";
    		return $this->update($sql);

    }

    /** 
     *
     *  @param  
     *  @brief Libera un rango de Ips para que pueda volver a ser utilizado
     *  @return boolean
     *
     */
    function unlink() {
        // cierro todos los períodos anteriores
        $sql1 = "UPDATE ipsHistorial SET hasta = NOW() WHERE ( CustomerID = ".$this->customerID." AND hasta IS NULL);";
        $this->update($sql1);

    	$sql = "UPDATE ips SET customerID = NULL WHERE id = $this->id";
    	return $this->update($sql);
    }

}
?>