<?php
/*! \class Subscriber
 *  \brief Clase ORM de la tabla Subscriber
 *  \author juebel@gmail.com
 *
 * Crea, busca y modifica los campos del objeto Subscriber
 */
class SubscriberIp extends Database {

    // propiedades correspondientes a los campos de la tabla
    var $subscriber_id = null;
    var $SubscriberIdentity = "NULL";
    var $id = "NULL";
    var $servicePlanId = "NULL";
    var $InvoiceProfile_id = "NULL";
    var $deviceSerialNumber = "";
    var $technology = "";
    var $status_id = 2;
    var $updated_at = null;

    function __construct() {
        parent::__construct();

    }

    private function mapResultsToObjet($oR) {
        
        $this->subscriber_id = ($oR->subscriber_id == NULL) ? 'NULL' : $oR->subscriber_id;
        $this->SubscriberIdentity = ($oR->SubscriberIdentity == NULL) ? 'NULL' : $oR->SubscriberIdentity;
        $this->id = ($oR->id == NULL) ? 'NULL' : $oR->id;
        $this->servicePlanId = ($oR->servicePlanId == NULL) ? 'NULL' : $oR->servicePlanId;
        $this->InvoiceProfile_id = ($oR->InvoiceProfile_id == NULL) ? 'NULL' : $oR->InvoiceProfile_id;
        $this->deviceSerialNumber = ($oR->deviceSerialNumber == NULL) ? 'NULL' : $oR->deviceSerialNumber;
        $this->technology = $oR->technology;
        $this->status_id = $oR->status_id;
        $this->updated_at = $oR->updated_at;

    }

    public function getBySubscriberId($subscriber_id) {
        $sql = "SELECT * from Subscriber WHERE subscriber_id = '$subscriber_id' ORDER BY subscriber_id DESC LIMIT 1";
        $oR = $this->getRow($sql);
        $this->mapResultsToObjet($oR);
    }

    public function getById($id) {
        $sql = "SELECT * from Subscriber WHERE id = '$id' ORDER BY subscriber_id DESC LIMIT 1";
        $oR = $this->getRow($sql);
        $this->mapResultsToObjet($oR);
        
    }

    public function getBySubscriberIdentity($SubscriberIdentity) {
        $sql = "SELECT * from Subscriber WHERE SubscriberIdentity = '$SubscriberIdentity' ORDER BY subscriber_id DESC LIMIT 1";
        $oR = $this->getRow($sql);
        $this->mapResultsToObjet($oR);        
    }

    public function getOrCreate($customerID = 'NULL', $SubscriberIdentity = 'NULL') {
        $query_SubscriberIdentity = ($SubscriberIdentity == "" || $SubscriberIdentity == 'NULL') ? "(SubscriberIdentity is NULL OR SubscriberIdentity = 'NULL' OR SubscriberIdentity = '')" : "SubscriberIdentity = '$SubscriberIdentity'";
        $query_customerID = ($customerID == "" || $customerID == 'NULL') ? 'id is NULL' : "id = $customerID";
        
        $sql = "SELECT * from Subscriber WHERE $query_SubscriberIdentity AND $query_customerID";
        $oR = $this->getRow($sql);

        if($this->count == 0) {
            // lo creamos
            $this->SubscriberIdentity = $SubscriberIdentity;
            $this->id = ($customerID == "") ? 'NULL' : $customerID;
            $this->servicePlanId = 1;
            $this->status_id = 2;
            $this->save();

        } else {
            // lo levantamos
            $this->mapResultsToObjet($oR);

        }
        
    }

    public function getOrFail($customerID = 'NULL', $SubscriberIdentity = 'NULL') {
        $query_SubscriberIdentity = ($SubscriberIdentity == "" || $SubscriberIdentity == 'NULL') ? "(SubscriberIdentity is NULL OR SubscriberIdentity = 'NULL' OR SubscriberIdentity = '')" : "SubscriberIdentity = '$SubscriberIdentity'";
        $query_customerID = ($customerID == "" || $customerID == 'NULL') ? 'id is NULL' : "id = $customerID";
        
        $sql = "SELECT * from Subscriber WHERE $query_SubscriberIdentity AND $query_customerID";
        $oR = $this->getRow($sql);
        //  print_r($sql);
        if($this->count == 0) {
            // no hay resultados!

        } else {
            // lo levantamos
            $this->mapResultsToObjet($oR);

        }
        
    }


    
    public function getByDeviceSerialNumber($deviceSerialNumber) {
        $sql = "SELECT * from Subscriber WHERE deviceSerialNumber = '$deviceSerialNumber' ORDER BY subscriber_id DESC LIMIT 1";
        $oR = $this->getRow($sql);
        $this->mapResultsToObjet($oR);        
    }
    
    public function save() {

        // defino si es nuevo o viejo por el subscriber_id
        
        if($this->subscriber_id) { 
            // ya exisitia, hago update
            // guardo log
            $this->save_log($this->subscriber_id);

            $sql = "UPDATE Subscriber SET SubscriberIdentity = '".$this->SubscriberIdentity."', id = ".$this->id.", InvoiceProfile_id = ".$this->InvoiceProfile_id.", servicePlanId = ".$this->servicePlanId.", deviceSerialNumber = '".$this->deviceSerialNumber."', technology = '".$this->technology."', status_id = '".$this->status_id."', updated_at = NOW() WHERE subscriber_id = '".$this->subscriber_id."';";


            $this->update($sql);
            // retoma el objeto para repopular todas las propiedades
            $this->getBySubscriberId($this->subscriber_id);


        } else {
            // no existia, hago insert y actualizo el subscriber_id 

            $sql = "INSERT INTO  Subscriber (SubscriberIdentity, id, InvoiceProfile_id, servicePlanId, deviceSerialNumber, technology, status_id, updated_at) VALUES ('".$this->SubscriberIdentity."', ".$this->id.",".$this->InvoiceProfile_id.", ".$this->servicePlanId.", '".$this->deviceSerialNumber."', '".$this->technology."', '".$this->status_id."', NOW())";
            $this->insert($sql);

            // retoma el objeto para repopular todas las propiedades
            $this->subscriber_id = $this->last_insert_id;
            $this->getBySubscriberId($this->subscriber_id);
        }
    }
    public function save_log($subscriber_id) {
        $sql = "INSERT INTO Subscriber_log (subscriber_id, SubscriberIdentity, id, servicePlanId, InvoiceProfile_id, deviceSerialNumber, technology, status_id, updated_at) SELECT subscriber_id, SubscriberIdentity, id, servicePlanId, InvoiceProfile_id, deviceSerialNumber, technology, status_id, updated_at FROM Subscriber WHERE subscriber_id = '$subscriber_id'";
        //print_r($sql);
        $this->insert($sql);

    }

}

?>