<?php
/*! \class DtvSoapServer
 *  \brief Clase principal que maneja las llamadas a Webservices ABM y SYNC
 *  \author juebel@gmail.com
 *
 * Unifica el parseo de parámetros y sus validaciones
 * Define los parámetros mandatorios para cada método invocado
 */
class DtvSoapServer {

    var $WSDL   = "";                 /**< Contrato WSDL para el Webservice a servir */
    var $xml    = "";                 /**< XML enviado por POST en la invocación del WS */
    var $namespace_tag = "bro";       /**< Namespace por defecto */
    var $method_suffix = "RqElement";/**< Prefijo para los métodos */ 
    var $method     = "";             /**< Método invocado en la llamada al Webservice */
    var $params     = array();        /**< Array de parámetros recibidos y procesados */

 /** 
     *
     *  @param $WSDL string  
     *  @param $xml string  
     *  @brief Constructor. Recibe y disponibiliza el contrato, los parámetros, identifica el método invocado y aplica validaciones 
	 *  @todo Separar las validaciones fuera del método
	 *  @todo CanCreate: revisar validacion de ServicePlan para SATELITAL
	 *  @todo Graficar/Documentar todas las validaciones
     *  @return void
     *
     */
    public function __construct($WSDL, $xml) {        
        $this->WSDL = $WSDL;
        $this->xml = $xml;
        $this->openLog();
        // si llega index.php?wsdl solicita wsdl
        if( preg_match('/wsdl$/',$_SERVER['REQUEST_URI'])){
            $this->putWsdl();
        } 

        // peticion sin post
        if(empty($this->xml)){
            $this->method_exception(8000,"No se recibio peticion valida [L".__LINE__."]");
        }

        // busco el variable namespace
        try{
          preg_match_all("/xmlns\:(bro[0-9]?)/",$this->xml,$matches);
          $this->namespace_tag = array_pop($matches[1]);
        }
        catch(Exception $e){
          $this->common_exception(8000,"Namespace incorrecto");
        }

        try{
            $arrRequest = $this->soapXml2Array($this->xml);
            preg_match_all("/".$this->namespace_tag."(.*)".$this->method_suffix."/", key($arrRequest), $mMethods);
            $this->method = array_pop($mMethods[1]);
            $keyString = $this->namespace_tag.$this->method.$this->method_suffix;
            if(isset($arrRequest[$keyString]['comment'])) { 
                unset($arrRequest[$keyString]['comment']); 
            }
        }
        catch (Exception $e){
          $this->common_exception(8000,"Error al recuperar metodo [".$this->method."]");          
        }

        foreach ($arrRequest[$keyString] as $keyParametro => $valor){
            $parametro = str_replace($this->namespace_tag, '', $keyParametro);
			if(empty($valor)) { 
                $this->params[$parametro] = "NULL"; 
            }
            else { 
				//$this->params[$parametro] = (string)trim(str_replace('?','',strtoupper($valor)));
                $this->params[$parametro] = (string)trim(str_replace('?','',$valor));
			}
        }
        if(method_exists($this, $this->method ) ){
            try {
            // validamos para cada uno
                switch ($this->method) {            
                // troubleshooting
                    case 'Can_Create':
                        $this->writeLog($this->method . '|' . json_encode($this->params));
                    break;

                    case 'Get_Params':
                        $this->writeLog($this->method . '|' . json_encode($this->params));
                    break;

                // provisioning
                    case 'Activate':
                        $this->writeLog($this->method . '|' . json_encode($this->params));
                        $this->expand_servicePlan();
                        if(_ENVIROMENT != 'PROD' && $this->params['technology'] != 'SATELITAL' && !Utils::isDevelTestingDevice($this->params['subscriberIdentity'])){
                            $this->method_exception(8000,"El dispositivo no puede ser alcanzado en ambiente no productivo");
                        }
                    break;

                    case 'Deactivate':
                        $this->writeLog($this->method . '|' . json_encode($this->params));
                    break;

                    default:
                        $this->common_exception(8000,"No se definieron validaciones para el metodo ".$this->method);
                    break;
                } // END SWITCH	
            }catch (Exception $e) {
                $this->common_exception($e->getCode(), $e->getMessage());
            }        
            $this->{$this->method}();           
        } 
        else{
            $this->common_exception(8000,"Metodo ".$this->method." no implementado");
        }
    }   

    public function openLog() {
        if(_LOG_LOCAL) {
            openlog('API_ING_Provisioning', LOG_ODELAY, LOG_LOCAL7);
        }
    }

    public function writeLog($log_message, $log_type=LOG_INFO) {
        if(_LOG_LOCAL) {
            syslog($log_type,$log_message);
        }
    }

    /** 
     *
     *  @param $soapXml string  
     *  @brief Convierte el xml enviado por POST a un array asociativo
     *  @return array
     *
     */
    public function soapXml2Array($soapXml) {
        $response = preg_replace("/(<\/?)(\w+)\:([^>]*>)/", "$1$2$3", $soapXml);
        $xml = new SimpleXMLElement((string)$response);
        $body = $xml->xpath('//soapenvBody')[0];
        $array = json_decode(json_encode((array)$body), TRUE); 
        return $array;
    }

    /** 
     *
     *  @param 
     *  @brief Devuelve e WSDL y cierra la conexión
     *  @return array
     *
     */
    function putWsdl() {
        header("Content-type: text/xml");
        echo $this->WSDL;
        exit;
    }


    /** @defgroup Validaciones Grupo de validaciones
     *  Se agrupan las validaciones de parámetros
     *  @todo Definir tabla de mensajes de error, grupos, "code" y "message"
     *  @{
     */

    /** 
     *
     *  @param $throw boolean
     *  @brief Valida el formato del subscriberIdentity
     *  @brief Si $throw = true dispara una Excepcion para ser capturada e inturrumpe ejecución
     *  @todo Definir texto y código del error
     *  @return boolean
     *
     */
    function validate_subscriberIdentity($throw = false) {
        $subscriberIdentity = $this->params['subscriberIdentity'];
        $validate = preg_match("/^[0-9a-zA-Z]{10,15}$/", $subscriberIdentity);
        if($throw && !$validate){
            $code = 6000;
            $message = "subscriberIdentity invalido";
            $this->writeLog(__FUNCTION__ . ': ERROR-CODE:'.$code .'|'.$message . '|' . json_encode($this->params), LOG_ERR);
            throw new Exception($message, $code); 
        }
        else{
            return $validate; 
        }
    }

	/** 
     *
     *  @param $throw boolean
     *  @brief Valida el formato del subscriberIdentity para la tecnología LTE
     *  @brief Si $throw = true dispara una Excepcion para ser capturada e inturrumpe ejecución
     *  @todo confirmar expresión regular de la validación
     *  @todo Definir texto y código del error
     *  @return boolean
     *
     */
    function validate_subscriberIdentity_LTE($throw = false) {
        $subscriberIdentity = $this->params['subscriberIdentity'];
        $validate = preg_match("/^[0-9]{15}$/", $subscriberIdentity);
        if($throw && !$validate) { 
            $code = 6000;
            $message = "subscriberIdentity invalido para tecnologia LTE";
            $this->writeLog(__FUNCTION__ . ': ERROR-CODE:'.$code .'|'.$message . '|' . json_encode($this->params), LOG_ERR);
            throw new Exception($message, $code); 
        }
        else { 
            return $validate; 
        }
    }

	/** 
     *
     *  @param $throw boolean
     *  @brief Valida el formato del subscriberIdentity para la tecnología WIMAX
     *  @brief Si $throw = true dispara una Excepcion para ser capturada e inturrumpe ejecución
     *  @todo confirmar expresión regular de la validación
     *  @todo Definir texto y código del error
     *  @return boolean
     *
     */
    function validate_subscriberIdentity_WIMAX($throw = false){
        $subscriberIdentity = $this->params['subscriberIdentity'];
        $validate = preg_match("/^[0-9a-zA-Z]{12}$/", $subscriberIdentity);
        if($throw && !$validate) { 
            $code = 6000;
            $message = "subscriberIdentity invalido para tecnologia WIMAX";
            $this->writeLog(__FUNCTION__ . ': ERROR-CODE:'.$code .'|'.$message . '|' . json_encode($this->params), LOG_ERR);
            throw new Exception($message, $code); 
        }
        else { 
            return $validate; 
        }
    }

	/** 
     *
     *  @param $throw boolean
     *  @brief Valida el formato del subscriberIdentity para la tecnología FTTH
     *  @brief Si $throw = true dispara una Excepcion para ser capturada e inturrumpe ejecución
     *  @todo confirmar expresión regular de la validación
     *  @todo Definir texto y código del error
     *  @return boolean
     *
     */
    function validate_subscriberIdentity_FTTH($throw = false) {
        $subscriberIdentity = $this->params['subscriberIdentity'];
        $validate = preg_match("/^[0-9a-zA-Z]{12}$/", $subscriberIdentity);
        if($throw && !$validate) { 
            $code = 6000;
            $message = "subscriberIdentity invalido para tecnologia FTTH";
            $this->writeLog(__FUNCTION__ . ': ERROR-CODE:'.$code .'|'.$message . '|' . json_encode($this->params), LOG_ERR);            
            throw new Exception($message, $code); 
        }
        else { 
            return $validate; 
        }
    }

    /** 
     *
     *  @param $throw boolean
     *  @brief Valida el formato del customerID (identificador de Subscriber para tecnología SATELITAL)
     *  @brief Si $throw = true dispara una Excepcion para ser capturada e inturrumpe ejecución
     *  @todo Definir texto y código del error
     *  @return boolean
     *
     */
    function validate_customerID($throw = false) { 
        $customerID = $this->params['customerID'];
        if($customerID == 1) { return true; }
        $validate = preg_match("/(^[0-9a-zA-Z]{5,10}$)/", $customerID);
        if($throw && !$validate) { 
            $code = 6000;
            $message = "customerID invalido";
            $this->writeLog(__FUNCTION__ . ': ERROR-CODE:'.$code .'|'.$message . '|' . json_encode($this->params), LOG_ERR);
            throw new Exception($message, $code); 
        }
        else {
            return $validate; 
        }
    }

    /** 
     *
     *  @param $throw boolean
     *  @param $expand boolean
     *  @brief Valida el formato del servicePlan (identifica Plan de Datos contratado) en formato y/o existencia en BBDD
     *  @brief Si $expand == true, busca el servicePlan en BBDD
     *  @brief Si $throw = true dispara una Excepcion para ser capturada e inturrumpe ejecución
     *  @todo Definir texto y código del error
     *  @return mixed boolean|Object
     *
     */
    function validate_servicePlan($throw = false, $expand = false) {
        $servicePlan = $this->params['servicePlan'];
        
        //$servicePlanId = preg_match("/(^[0-9A-Z]{2,6}$)/", strtoupper($servicePlan));
	    $servicePlanId = preg_match("/(^[0-9A-Z]$)/", strtoupper($servicePlan));
        //print_r($servicePlanId);die();
        // los busco en la bd
        if($expand){
            $bd = new Database();
            $sql = "SELECT * from ServicePlan WHERE name LIKE '".$servicePlan."%' Limit 1";
		  //echo "<!-- $sql -->";
            $servicePlanId = $bd->GetRow($sql);
        }

        if($throw && !$servicePlanId) { 
            $code = 6000;
            $message = "servicePlan invalido";
            $this->writeLog(__FUNCTION__ . ': ERROR-CODE:'.$code .'|'.$message . '|' . json_encode($this->params), LOG_ERR);
            throw new Exception($message, $code); }
        else { 
            return $servicePlanId; 
        }
    }

      /** 
     *
     *  @param $throw boolean
     *  @param $expand boolean
     *  @brief Valida que el tag technology contenga una de las tecnologías admitidas
     *  @brief Si $throw = true dispara una Excepcion para ser capturada e inturrumpe ejecución
     *  @todo Definir texto y código del error
     *  @todo Buscar en base de datos listado de tecnologías disponibles (para futuras mejoras)
     *  @return boolean
     *
     */
    function validate_technology($throw = false){
        $technology = $this->params['technology'];
        $validTechnologies = array('SATELITAL', 'FTTH', 'LTE/WIMAX', 'LTE', 'WIMAX','5G');
        $validate = in_array($technology, $validTechnologies);
        if($throw && !$validate) {
            $code = 6000;
            $message = "technology invalido";
            $this->writeLog(__FUNCTION__ . ': ERROR-CODE:'.$code .'|'.$message . '|' . json_encode($this->params), LOG_ERR);            
            throw new Exception($message, $code); }
        else { 
            return $validate; 
        }
    }

      /** 
     *
     *  @param $isTechnology string
     *  @param $throw boolean
     *  @brief Valida que el tag technology contenga la tecnología especificada
     *  @brief Si $throw = true dispara una Excepcion para ser capturada e inturrumpe ejecución
     *  @todo Definir texto y código del error
     *  @return boolean
     *
     */
    function validate_isTechnology($isTechnology, $throw = false) {
        $technology = $this->params['technology'];
        $validate = ($isTechnology == $technology) ? true : false;
        if($throw && !$validate) { 
            $code = 6000;
            $message = "technology $technology invalida para esta operación";
            $this->writeLog(__FUNCTION__ . ': ERROR-CODE:'.$code .'|'.$message . '|' . json_encode($this->params), LOG_ERR);
            throw new Exception($message, $code); }
        else { 
            return $validate; 
        }
    }

    /** 
     *
     *  @param $throw boolean
     *  @brief Valida formato de deviceSerialNuber 
     *  @brief Si $throw = true dispara una Excepcion para ser capturada e inturrumpe ejecución
     *  @todo Definir expresion regular para SerialNumber
     *  @todo Definir texto y código del error
     *  @return boolean
     *
     */
    function validate_deviceSerialNumber($throw = false) {
        $deviceSerialNumber = $this->params['deviceSerialNumber'];
        $validate = true;
        if($throw && !$validate) { 
            $code = 6000;
            $message = "deviceSerialNumber invalido";
            $this->writeLog(__FUNCTION__ . ': ERROR-CODE:'.$code .'|'.$message . '|' . json_encode($this->params), LOG_ERR);
            throw new Exception($message, $code); }
        else { 
            return $validate; 
        }
    }

    /** 
     *
     *  @param $throw boolean
     *  @brief Valida formato de deviceSerialNuber 
     *  @brief Si $throw = true dispara una Excepcion para ser capturada e inturrumpe ejecución
     *  @todo Definir expresion regular para SerialNumber
     *  @todo Definir texto y código del error
     *  @return boolean
     *
     */
    //validate_SATELITALOldDeviceSerialNumber
    function validate_SATELITALdeviceSerialNumber($throw = false) {
        $deviceSerialNumber = $this->params['deviceSerialNumber'];
        $validate = preg_match("/(^[0-9A-Z]{14}$)/", strtoupper($deviceSerialNumber));
        if($throw && !$validate) { 
            $code = 6000;
            $message = "deviceSerialNumber [$deviceSerialNumber] invalido para tecnologia SATELITAL";
            $this->writeLog(__FUNCTION__ . ': ERROR-CODE:'.$code .'|'.$message . '|' . json_encode($this->params), LOG_ERR);
            throw new Exception($message, $code); }
        else { 
            return $validate; 
        }
    }

    /** 
     *
     *  @param $throw boolean
     *  @brief Valida formato de deviceSerialNuber 
     *  @brief Si $throw = true dispara una Excepcion para ser capturada e inturrumpe ejecución
     *  @todo Definir expresion regular para SerialNumber
     *  @todo Definir texto y código del error
     *  @return boolean
     *
     */
    
    function validate_SATELITALOldDeviceSerialNumber($throw = false) {
        $oldSerialNumber = $this->params['oldSerialNumber'];
        $validate = preg_match("/(^[0-9]{8}$)/", strtoupper($oldSerialNumber));
        if($throw && !$validate) { 
            $code = 6000;
            $message = "oldSerialNumber [$oldSerialNumber] invalido para tecnologia SATELITAL";
            $this->writeLog(__FUNCTION__ . ': ERROR-CODE:'.$code .'|'.$message . '|' . json_encode($this->params), LOG_ERR);            
            throw new Exception($message, $code); }
        else { 
            return $validate; 
        }
    }


    /** 
     *
     *  @param $throw boolean
     *  @brief Valida formato de oldSerialNumber 
     *  @brief Si $throw = true dispara una Excepcion para ser capturada e inturrumpe ejecución
     *  @todo Definir expresion regular para SerialNumber
     *  @todo Definir texto y código del error
     *  @return boolean
     *
     */
    function validate_oldSerialNumber($throw = false) {
        $oldSerialNumber = $this->params['oldSerialNumber'];
        $validate = true;
        if($throw && !$validate) { 
            $code = 6000;
            $message = "oldSerialNumber invalido";
            $this->writeLog(__FUNCTION__ . ': ERROR-CODE:'.$code .'|'.$message . '|' . json_encode($this->params), LOG_ERR);
            throw new Exception($message, $code); }
        else { 
            return $validate; 
        }
    }

  /** 
     *
     *  @param $throw boolean
     *  @brief Valida formato y existencia de "capacity"
     *  @brief Si $throw = true dispara una Excepcion para ser capturada e inturrumpe ejecución
     *  @todo Definir la expresión regular para validar capacity
     *  @todo Definir texto y código del error
     *  @return boolean
     *
     */
    function validate_capacity($throw = false, $expand = false) {
        $capacity = $this->params['capacity'];
        $capacity = true;
        // los busco en la bd
        if($expand && $capacity) {
            $bd = new Database();
            $sql = "SELECT * from Tokens WHERE name LIKE '". $this->params['capacity']."%'";
            $capacity = $bd->GetRow($sql);
        }
        if($throw && !$capacity) { 
            $code = 6000;
            $message = "capacity invalido";
            $this->writeLog(__FUNCTION__ . ': ERROR-CODE:'.$code .'|'.$message . '|' . json_encode($this->params), LOG_ERR);
            throw new Exception($message, $code); }
        else { 
            return $capacity; 
        }
    }

   /** 
     *
     *  @param $throw boolean
     *  @param $expand boolean
     *  @brief Valida formato y existencia "invoiceProfile" (perfil de ciclo de facturación)
     *  @brief Si $throw = true dispara una Excepcion para ser capturada e inturrumpe ejecución
     *  @brief Si $expand = true, busca el dato en la BBDD y devuelve Objeto con todos los campos
     *  @todo Definir la expresión regular para validar "invoiceProfile"
     *  @todo Definir texto y código del error
     *  @return boolean
     *
     */
    function validate_invoiceProfile($throw = false, $expand = false) {
        $invoiceProfile = $this->params['invoiceProfile'];
        $invoiceProfileId = true;
        // los busco en la bd
        if($expand && $invoiceProfileId) {
            $bd = new Database();
            $sql = "SELECT * from InvoiceProfiles WHERE InvoiceProfile_id = '$invoiceProfile'";
            $invoiceProfileId = $bd->GetRow($sql);
        }
        if($throw && !$invoiceProfileId) { 
            $code = 6000;
            $message = "invoiceProfile invalido";
            $this->writeLog(__FUNCTION__ . ': ERROR-CODE:'.$code .'|'.$message . '|' . json_encode($this->params), LOG_ERR);
            throw new Exception($message, $code); }
        else { 
            return $invoiceProfileId; 
        }
    }

        /** @} */ // end of Validaciones

    /** @defgroup Expansiones Grupo de expansiones
     *  Se agrupan los métodos que expanden parámetros recibidos tomando datos de la BBDD
     *  @{
     */
    
    /** 
     *
     *  @param 
     *  @brief Valida y expande el parámetro "servicePlan" con todos los campos de la BBDD de la tabla ServicePlan
     *  @return void
     *
     */

    function expand_servicePlan() {
        $oServicePlan = $this->validate_servicePlan(true, true);
        $this->params['servicePlan_id'] = $oServicePlan->id;
        $this->params['servicePlan_name'] = $oServicePlan->name;
        $this->params['servicePlan_code'] = $oServicePlan->code;
        $this->params['servicePlan_code_provisioning'] = $oServicePlan->code_provisioning;
        $this->params['servicePlan_code_hughes_sp'] = $oServicePlan->code_hughes_sp;
    }

    /** 
     *
     *  @param 
     *  @brief Valida y expande el parámetro "servicePlan" con todos los campos de la BBDD de la tabla ServicePlan
     *  @return void
     *
     */

    function expand_servicePlanById($ServicePlanId) {
        $bd = new Database();
        $sql = "SELECT * from ServicePlan WHERE id = $ServicePlanId";
        $oServicePlan = $bd->GetRow($sql);
        if($throw && !$servicePlanId) { 
            throw new Exception("servicePlan invalido", 6000);
            return false;
        }
        else {
            $this->params['servicePlan_id'] = $oServicePlan->id;
            $this->params['servicePlan_name'] = $oServicePlan->name;
            $this->params['servicePlan_code'] = $oServicePlan->code;
            $this->params['servicePlan_code_provisioning'] = $oServicePlan->code_provisioning;
            $this->params['servicePlan_code_hughes_sp'] = $oServicePlan->code_hughes_sp;
            return true;
        }
    }

    /** 
     *
     *  @param 
     *  @brief Valida y expande el parámetro "servicePlan" con todos los campos de la BBDD de la tabla ServicePlan
     *  @return void
     *
     */

    function expand_capacity() {
        $oCapacity = $this->validate_capacity(true, true);
        $this->params['capacity_id'] = $oCapacity->Token_id;
        $this->params['capacity_name'] = $oCapacity->name;
        $this->params['capacity_capacity'] = $oCapacity->capacity;
    }

    /** 
     *
     *  @param 
     *  @brief Valida y expande el parámetro "invoiceProfile" con todos los campos de la BBDD de la tabla InvoiceProfiles
     *  @return void
     *
     */
    function expand_invoiceProfile() {
        $oInvoiceProfile = $this->validate_invoiceProfile(true, true);
        $this->params['InvoiceProfile_id'] = $oInvoiceProfile->InvoiceProfile_id;
        $this->params['InvoiceProfile_id_name'] = $oInvoiceProfile->name;
        $this->params['InvoiceProfile_id_renewal_day'] = $oInvoiceProfile->renewal_day;
    }

    /** @} */ // end of Validaciones
// trasladamos mensajes de error y success a cada implementacion de la clase para adapta
//      function common_exception($code, $message) {}
//		function method_exception($code, $message) {}
//		function soap_validation_successfull() {}
//		function soap_return_values($arrValues) {}
} // end class
?>