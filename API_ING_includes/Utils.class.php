<?php
/*! \class Utils
 *  \brief Clase con utilidades
 *  \author juebel@gmail.com
 *  test change
 * Contiene funciones útiles fuera de toda estructura, o temporales para entorno DEV.
 */
class Utils {
    
public function __construct() {}


    /** 
     *
     *  @param 
     *  @brief \n Obtiene una secuencia del 1 al 4 para respuestas dummy 
     *  @return integer
     *
     */
    function getTmpSequence() {

        $con = mysqli_connect(_MYSQL_HOST,_MYSQL_USER,_MYSQL_PASS, _MYSQL_DB);
        $r = mysqli_query($con,"select seq from tmp_sequence limit 1");
        
        $data = mysqli_fetch_object($r);
        // actualizo para el proximo

        if($data->seq >= 4) { $newvalue = 1; } else { $newvalue = $data->seq + 1; }

        $r = mysqli_query($con,"update tmp_sequence set seq = $newvalue");

        return $data->seq;
    
    }

     /** 
     *
     *  @param $subscriberIdentity string
     *  @brief \n Consulta si un subscriberIdentity esta en la tabla TestingDevices para ser alcanzado en entorno DEVEL
     *  @return boolean|integer
     *
     */
    
    function isDevelTestingDevice($subscriberIdentity) {

	
    if(_ENVIROMENT == 'DEV') {
		error_log("\n".date('U').": ES DEV", 3, "/var/log/API_ING.log");
		
        $con = mysqli_connect(_MYSQL_HOST,_MYSQL_USER,_MYSQL_PASS,_MYSQL_DB);
        $sql = "SELECT count(*) as count FROM TestingDevices WHERE subscriberIdentity = '".$subscriberIdentity."' LIMIT 1";
        $r = mysqli_query($con,$sql);
        $data = mysqli_fetch_object($r);
        // devuelve 1 o 0 (true o false) si esta o no esta en la base de modems de testing
        return $data->count;
     } else {
         // devuelve true su estamos en produccion
 		error_log("\n".date('U').": NOOOO ES DEV", 3, "/var/log/API_ING.log");       

		return true;
		
     }

    }

     /** 
     *
     *  @param $valor integer
     *  @brief Convierte valores de Megas a Gigas
     *  @return float
     *
     */
     static function megasToGigas(&$valor) {
        
        $unidadGb = 1024;
        $valor = number_format((float)($valor/$unidadGb),2,',','');
        return $valor;
    }

    static function debug($msg = "") {
        // $msg .= "\n".date("c")." :: $msg";
        // error_log("/var/log/API_ING.log",$msg,FILE_APPEND);
        
    }

       static function isOffPeakTime() {
        // seteo de horas para este momento
        $offPeak_ini = new DateTime("01:00");
        $offPeak_end = new DateTime("07:00");
        // chequeo de hora
        $now = new DateTime();
        if(($now >= $offPeak_ini) && ($now < $offPeak_end) )  { 
            return true;
        } else { 
            return false;
        }

    }
    /*
    * void logInDb
    * brief: guarda log en base de datos
    * logDB         [resouce] conexion a bbdd (D1)
    * trigger:      [integer] disparador del log. por ahora son solo los crons
    * subscriber:   [integer] subscriberID (satelital)
    * description:  [string]  texto del log
    */
        function logInDb($logDB, $trigger, $subscriber, $description) {

            $sqlCronLog = "INSERT INTO CronLog (subscriber, cron, description) VALUES ($subscriber, $trigger, '$description');";
            $logDB->insert($sqlCronLog);
            //echo "\n".__FUNCTION__;
            //echo "\n $subscriber \t $description";
            }


}
?>
