<?php
/*! \class Provisioning
 *  \brief Interface con NMS Provisioning (DTV)
 *  \author juebel@gmail.com
 *
 * Implementa todas las llamadas a Provisioning utilizadas en DTV via SOAP
 */
//ini_set('error_reporting', E_ALL|E_STRICT);
//ini_set('display_errors', 1);
set_time_limit(_GLOBAL_TIMEOUT+10);
class SoapClientTimeout extends SoapClient{
	private $timeout; 
	public function __setTimeout($timeout){
		if (!is_int($timeout) && !is_null($timeout)){
			throw new Exception("Invalid timeout value", 500);
		} 
		$this->timeout = $timeout;
	}
 
	public function __doRequest($request, $location, $action, $version, $one_way = FALSE){
		if (!$this->timeout){
			// Call via parent because we require no timeout
			$response = parent::__doRequest($request, $location, $action, $version, $one_way);
		}
        else{
			// Call via Curl and use the timeout
            //$location = "httpss:".$location;
			$curl = curl_init($location); 
			curl_setopt($curl, CURLOPT_VERBOSE, FALSE);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($curl, CURLOPT_POST, TRUE);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
			curl_setopt($curl, CURLOPT_HEADER, FALSE);
			curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: text/xml"));
			curl_setopt($curl, CURLOPT_TIMEOUT, $this->timeout);
            $testTimeout = false;
            if($testTimeout){
                sleep(_GLOBAL_TIMEOUT);
                $response = "";
                $curlError = 28;
            }
            else{
                $response = curl_exec($curl);
                $curlError = curl_errno($curl);                
            }
            $arrErrorTimeout = array(1,2,5,7,8,9,19,28,47,55,56,78,89);
			if (in_array($curlError, $arrErrorTimeout)) {
                throw new SoapFault("Fallo la conexion con el NMS Legacy. Timeout [ERROR CURL:".$curlError."]","1-8007");
            }
            elseif (curl_errno($curl)) {
				throw new SoapFault(curl_error($curl), 400);
			} 
			curl_close($curl);
		} 
		if (!$one_way){
			return ($response);
		}
	}
}

class Provisioning {    
    var $url = _ftth_amba;    /**< URL del contrato WS. */
    var $default_socket_timeout = _GLOBAL_TIMEOUT;                               /**< Tiempo de espera para la conección al servidor */
    var $soapClient = null;                                         /**< Objeto SoapClient */
    var $response = "";                                             /**< XML de respuesta a la última llamada al WS */
    var $arrResponse = array();                                     /**< Array asociativo con tags y valores de la respuesta a la última llamada al WS */
    var $response_status    = false;                              /**< Respuesta true o false a la última llamada al WS */
    var $response_code      = 200;                                  /**< Código numérico de la respuesta a la última llamada al WS */
    var $response_message   = "";                                   /**< Texto de respuesta a la última llamada al WS */
    /** 
     *
     *  @param
     *  @brief Constructor. Crea la onexión al WS y establece sus parámetros
     *  @return boolean
     *
     */
    public function __construct(){
        $this->response_status = true;
        error_reporting(E_ALL);
        $soap_options = array(
            'trace' => 1,
            'connection_timeout' => _GLOBAL_TIMEOUT,
            'compression' => true,
            'encoding' => 'utf-8',
            'exceptions' => true,
            'features' => SOAP_SINGLE_ELEMENT_ARRAYS
        );
        try {
            $this->soapClient = new SoapClientTimeout($this->url, $soap_options);
            $this->soapClient->__setTimeout(_GLOBAL_TIMEOUT);	
        }
        catch (Exception $e){
            $this->response_status  = false;
            $this->response_code    = $e->getCode();
            $this->response_message = $e->getMessage();
        }
        catch (SoapFault $se) {
            //prtin_r($se);            
            $this->response_status  = false;
            $this->response_code    = $se->faultcode;
            $this->response_message = $se->faultstring;
        }
        return $this->response_status;
    }

    /** 
     *
     *  @param $soapXml
     *  @brief Convierte el XML Soap de respuesta a un array asociativo tag=>valor
     *  @brief Elimina los cmpos vacíos
     *  @return array
     *
     */
    public function return2array($soapXml){
        $response = preg_replace("/(<\/?)(\w+)\:([^>]*>)/", "$1$2$3", $soapXml);
        $xml = new SimpleXMLElement($response);
        $body = $xml->xpath('//return')[0];
        $array = json_decode(json_encode((array)$body), TRUE);
        $return = array();
        foreach ($array as $keyParametro => $valor) {
            if(empty($valor)){
                $return[$keyParametro] = "";
            }
            else{
                $return[$keyParametro] = (string)$valor;
            }
        }
        return $return;
    }
    /** 
     *
     *  @param $subscriberIdentity string
     *  @param $servicePlan string
     *  @param $technology string
     *  @brief Construye y llama  al método Provisioning.create
     *  @return boolean
     *
     */
    public function create($subscriber_identity, $profile, $technology, $force_provisioning = false){
        $action='Provisioning.create';
        try {
            $this->soapClient->$action($subscriber_identity, $profile, $force_provisioning, $technology);            
            $this->response_status = true;
            $this->response = $this->soapClient->__getLastResponse();
            $this->arrResponse = $this->return2array($this->response);
        }
        catch(SoapFault $e) {
            $this->response_status = false;
            $this->response_code      = $e->faultcode;
            $this->response_message   = $e->faultstring;
        }
        return $this->response_status;
    }

    public function get_params($subscriber_identity){
        $action='Troubleshooting.get_params';        
        try {
            $this->soapClient->$action($subscriber_identity, 'FTTH');
            $this->response_status = $this->response = $this->soapClient->__getLastResponse();
            $this->response_status = $this->arrResponse = $this->return2array($this->response);            
        }
        catch(SoapFault $e) {
            $this->response_status = false;
            $this->response_code      = $e->faultcode;
            $this->response_message   = $e->faultstring;
        }
        return $this->response_status;
    }
    
    /** 
     *
     *  @param $subscriberIdentity string
     *  @param $servicePlan string
     *  @param $technology string
     *  @brief Construye y llama  al método Provisioning.delete
     *  @return boolean
     *
     */
    public function delete($subscriber_identity, $technology){
        $action='Provisioning.delete';
        try{
           // return("llega aca"); die();
            $this->soapClient->$action($subscriber_identity, $technology);
            $this->response_status = true;
            $this->response = $this->soapClient->__getLastResponse();
            $this->arrResponse = $this->return2array($this->response);
        }
        catch(SoapFault $e){
            $this->response_status = false;
            $this->response_code    = $e->faultcode;
            $this->response_message   = $e->faultstring;
        }
        return $this->response_status;        
    }

    /** 
     *
     *  @param $subscriberIdentity string
     *  @param $servicePlan string
     *  @param $technology string
     *  @brief Construye y llama  al método Provisioning.read
     *  @return boolean
     *
     */
    public function read($subscriber_identity, $technology){
        $action='Provisioning.read';
        try {
            $this->soapClient->$action($subscriber_identity, $technology);
            $this->response_status = true;
            $this->response = $this->soapClient->__getLastResponse();
            $this->arrResponse = $this->return2array($this->response);
        }                
        catch(SoapFault $e) {
            $this->response_status = false;
            $this->response_code    = $e->faultcode;
            $this->response_message   = $e->faultstring;
        }               
        return $this->response_status;
    }

    /** 
     *
     *  @param $subscriberIdentity string
     *  @param $servicePlan string
     *  @param $technology string
     *  @brief Construye y llama  al método Provisioning.suspend
     *  @return boolean
     *
     */
    public function suspend($subscriber_identity, $technology) {
        $action='Provisioning.suspend';
        try {
            $this->soapClient->$action($subscriber_identity, $technology);
            $this->response_status = true;
            $this->response = $this->soapClient->__getLastResponse();
            $this->arrResponse = $this->return2array($this->response);
        }               
        catch(SoapFault $e) {
            $this->response_status = false;
            $this->response_code    = $e->faultcode;
            $this->response_message   = $e->faultstring;
        }               
        return $this->response_status;
    }
   
    /** 
     *
     *  @param $subscriberIdentity string
     *  @param $servicePlan string
     *  @param $technology string
     *  @brief Construye y llama  al método Provisioning.unsuspend
     *  @return boolean
     *
     */
    public function unsuspend($subscriber_identity, $technology) {
        $action='Provisioning.unsuspend';
        try {
            $this->soapClient->$action($subscriber_identity, $technology);
            $this->response_status = true;
            $this->response = $this->soapClient->__getLastResponse();
            $this->arrResponse = $this->return2array($this->response);
        }                
        catch(SoapFault $e) {
            $this->response_status = false;
            $this->response_code    = $e->faultcode;
            $this->response_message   = $e->faultstring;
        }               
        return $this->response_status;
    }

    /** 
     *
     *  @param $subscriberIdentity string
     *  @param $servicePlan string
     *  @param $technology string
     *  @brief Construye y llama  al método Provisioning.update
     *  @return boolean
     *
     */
    public function update($subscriber_identity,$profile, $technology) {
        $action='Provisioning.update'; 
        try {
            $this->soapClient->$action($subscriber_identity,$profile, $technology);
            $this->response_status = true;
            $this->response = $this->soapClient->__getLastResponse();
            $this->arrResponse = $this->return2array($this->response);
        }                
        catch(SoapFault $e) {
            $this->response_status = false;
            $this->response_code    = $e->faultcode;
            $this->response_message   = $e->faultstring;
        }               
        return $this->response_status;
    }

    /** 
     *
     *  @param $subscriberIdentity string
     *  @param $servicePlan string
     *  @param $technology string
     *  @brief Construye y llama  al método Provisioning.get_available_firmwares
     *  @return boolean
     *
     */
    public function get_available_firmwares($subscriber_identity, $technology) {
        $action='Troubleshooting.get_available_firmwares';
        try {
            $this->soapClient->$action($subscriber_identity, $technology);
            $this->response_status = true;
            $this->response = $this->soapClient->__getLastResponse();
            $this->arrResponse = $this->return2array($this->response);                
        }                
        catch(SoapFault $e){
            $this->response_status = false;
            $this->response_code    = $e->faultcode;
            $this->response_message   = $e->faultstring;
        }               
        return $this->response_status;
    }
    
    /** 
     *
     *  @param $subscriberIdentity string
     *  @param $servicePlan string
     *  @param $technology string
     *  @brief Construye y llama  al método Provisioning.reset_connection
     *  @return boolean
     *
     */
    public function reset_connection($subscriber_identity, $technology) {
        $action='Troubleshooting.reset_connection';
        try {
            $this->soapClient->$action($subscriber_identity, $technology);
            $this->response_status = true;
            $this->response = $this->soapClient->__getLastResponse();
            $this->arrResponse = $this->return2array($this->response);                
        }                
        catch(SoapFault $e) {
            $this->response_status = false;
            $this->response_code    = $e->faultcode;
            $this->response_message   = $e->faultstring;
        }               
        return $this->response_status;
    }

    /** 
     *
     *  @param $subscriberIdentity string
     *  @param $servicePlan string
     *  @param $technology string
     *  @brief Construye y llama  al método Provisioning.run_speed_test
     *  @return boolean
     *
     */
    public function run_speed_test($subscriber_identity, $technology) {
        $action='Troubleshooting.run_speed_test';
        try {
            $this->soapClient->$action($subscriber_identity, $technology);
            $this->response_status = true;
            $this->response = $this->soapClient->__getLastResponse();
            $this->arrResponse = $this->return2array($this->response);                
        }                
        catch(SoapFault $e) {
            $this->response_status = false;
            $this->response_code    = $e->faultcode;
            $this->response_message   = $e->faultstring;
        }               
        return $this->response_status;
    }
    
    /** 
     *
     *  @param $subscriberIdentity string
     *  @param $servicePlan string
     *  @param $technology string
     *  @brief Construye y llama  al método Provisioning.upload_firmware
     *  @return boolean
     *
     */
    public function upload_firmware($subscriber_identity, $firmware_name, $technology) {
        $action='Troubleshooting.upload_firmware';
        try {
            $this->soapClient->$action($subscriber_identity, $firmware_name, $technology);
            $this->response_status = true;
            $this->response = $this->soapClient->__getLastResponse();
            $this->arrResponse = $this->return2array($this->response);
        }
        catch(SoapFault $e) {
            $this->response_status = false;
            $this->response_code    = $e->faultcode;
            $this->response_message   = $e->faultstring;
        }
        return $this->response_status;
    }
}
?>