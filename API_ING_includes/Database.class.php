<?php
/*! \class Database
 *  \brief Conexión a base de datos MySQL
 *  \author juebel@gmail.com
 *
 * Crea la conexión e implementa los métodos más utilizados para la BBDD definida
 * @todo Manejo de errores de conexión y querys
 */
class Database {
    

    protected $MYSQL_HOST   = _MYSQL_HOST;  /**< Host de la BBDD. Lo toma de enviroment.php */
    protected $MYSQL_USER   = _MYSQL_USER;  /**< Usuario de la BBDD. Lo toma de enviroment.php */
    protected $MYSQL_PASS   = _MYSQL_PASS;  /**< Clave de la BBDD. Lo toma de enviroment.php */
    protected $MYSQL_DB     = _MYSQL_DB_INGPROV;    /**< Nombre de la BBDD por defecto. Lo toma de enviroment.php */
    protected $MYSQL_DB_D1  = _MYSQL_DB_D1; /**< Nombre de la BBDD D-1. Lo toma de enviroment.php */
    public $mysqli = "";                    /**< Objeto de la conexión */
    public $last_insert_id = null;          /**< Ultimo ID insertado, se actualiza en cada INSERT */
	
	public $count = 0;                     /**< Numero de filas afectadas en ultimo INSERT, UPDATE, DELETE */
		
    public $mysqli_error_msg = null;       /**< Mensaje de error de mysql */
    public $mysqli_error_nro = null;       /**< Codigo de error de mysql */
    
    public $log_querys = true;             /**< Indica si deben logearse los querys (true) o no (false) */

    /** 
     *
     *  @param
     *  @brief Constructor. Crea la conexión al BBDD
     *  @return void
     *
     */
    function __construct() {
        $this->mysqli = new mysqli($this->MYSQL_HOST, $this->MYSQL_USER, $this->MYSQL_PASS, $this->MYSQL_DB);
        if ($this->mysqli->connect_errno) {
            echo "Fallo al conectar a MySQL: " . $this->mysqli->connect_error;
        }
        $this->mysqli->query("SET NAMES 'utf8'");
    }

    /** 
     *
     *  @param
     *  @brief Cambia la conexion apuntando a la BBDD por defecto
     *  @return void
     *
     */
    function changeD0() {
        $this->mysqli->select_db($this->MYSQL_DB);
    }

    /** 
     *
     *  @param
     *  @brief Cambia la conexion apuntando a la BBDD D-1
     *  @return void
     *
     */function changeD1() {
        $this->mysqli->select_db($this->MYSQL_DB_D1);
    }

    /** 
     *
     *  @param
     *  @brief Trae el nombre de la BBDD a la que esta conectado
     *  @return void
     *
     */
    function getNameDB() {
        if ($result = $this->mysqli->query("SELECT DATABASE()")) {
            $row = $result->fetch_row();
            printf("Default database is %s\n", $row[0]);
            $result->close();
        }
    }

    /** 
     *
     *  @param
     *  @brief Cierra la conexion
     *  @return boolean
     *
     */
    function close() {
        $this->mysqli->close();
    }

    /** 
     *
     *  @param $sql string
     *  @brief Devuelve un solo valor
     *  @return string|int
     *
     */

    function makesql($sql, $tipo){
        try{
            $this->mysqli->query($sql);
            $result = "true";
        }
        catch(Exception $e){
            $result = $e;
        }
        return $result;
    }
    
    function getValue($sql) {
        $this->log($sql);
        $result = $this->mysqli->query($sql);
		$this->count = $this->mysqli->affected_rows;
        
        while ( $oResult = $result->fetch_object() ) {
        $aResult[] = $oResult;
        }
        return $aResult[0];
    }

    /** 
     *
     *  @param $sql string
     *  @brief Trae una fila
     *  @brief Devuelve unb objeto, donde cada propiedad es una columna de la tabla
     *  @return Object
     *
     */
    function getRow($sql) {
        $this->log($sql);
        $result = $this->mysqli->query($sql);
		$this->count = $this->mysqli->affected_rows;
		if($this->count>0)  { return $result->fetch_object(); }
    }

    /** 
     *
     *  @param $sql string
     *  @brief Trae varias filas
     *  @brief Devuelve un array de objetos 
     *  @return Array
     *
     */
    function getRows($sql) {
        $this->log($sql);
		// echo $sql;
        $result = $this->mysqli->query($sql);
		$this->count = $this->mysqli->affected_rows;
        $aResult = array();
        while ($oResult = $result->fetch_object()) {
            $aResult[] = $oResult;
        }
        
        return $aResult;
    }

    /** 
     *
     *  @param $sql string
     *  @brief Ejecuta un INSERT en la BBDD
     *  @return Boolean
     *
     */
    function insert($sql) {
        $this->log($sql);
        $affected_rows = false;

        if(!$this->mysqli->query($sql)) { 
            $this->mysqli_error_msg = $this->mysqli->error;
			$this->count = $this->mysqli->affected_rows;
            return false;
        }
        $this->last_insert_id = $this->mysqli->insert_id;
		$this->count = $this->mysqli->affected_rows;
        return true;
    }

    /** 
     *
     *  @param $sql string
     *  @brief Ejecuta un Update en la BBDD
     *  @return Boolean
     *
     */
    function update($sql) {
        $this->log($sql);
        $this->mysqli->query($sql);
        $this->count = $this->mysqli->affected_rows;
        return $this->mysqli->affected_rows;
    }

    /** 
     *
     *  @param $sql string
     *  @brief Ejecuta un DELETE en la BBDD
     *  @return Boolean
     *
     */
    function delete($sql) {
        $this->log($sql);
        $this->mysqli->query($sql);
        $this->count = $this->mysqli->affected_rows;
        return $this->mysqli->affected_rows;
    }

    /** 
     *
     *  @param $msg string
     *  @brief Registra los querys si $this->log_querys es true
     *  @return Boolean
     *
     */
    function log($msg) {
		try {
			if($this->log_querys) {
				syslog(LOG_DEBUG, '['.__CLASS__.'] '.$msg);
				error_log("\n".date('U').'['.__CLASS__.'] '.$msg, 3, "/var/log/API_ING.log");
			}
			
		} catch (Exception $e) {
			return false;
		}
    }
} // end class
?>