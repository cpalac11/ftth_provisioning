<?php
/*! \file config.php
 *  \brief General configuration of Provisioning Sysytem
 *  Mysql DB params, Technologies list, Profiles list, SSH params 
 * 
 *  \note To connect to a local database, use 127.0.0.1, no localhost, because MySQLi will try to use IPv6 protocol, an then IPv4 as fallback, with slow results.
 */
/*
$this->hostname='172.20.4.207';
        $this->user='user_netprov';
        $this->password='Providesa.2020';
        $this->database='provisioningPROD';
*/


$FILE_LOG_PATH="/var/log/provisioning/"; 

//! technologies.
/*! technologies array is the list of the technologies supported. */

$technologies[]= "FTTH";
//$technologies[]= "ADSL";
$technologies[]= "LTE/WIMAX";
// $technologies[]= "BTB";

//! profiles
/*! profiles array is the list of the profiles supported. */
$profiles[]= "2MB";
$profiles[]= "3MB";
$profiles[]= "6MB";
$profiles[]= "12MB";
$profiles[]= "30MB";
$profiles[]= "50MB";
$profiles[]= "100MB";


$SSH_USER="admin";
$SSH_PASS="zhone";

$outFile="/var/log/provisioning/snmp.out";
$loggingEnabled=1;

$trapTimeOut=2;

$LTE_WSDL="http://172.31.160.250:6274/Broadband/Argentina/Cordoba/LTE?wsdl"; /*!< LTE_WSDL is a WSDL for the LTE Webservice */
//$LTE_WSDL="TEST"; 

$WIMAX_GET="https://172.24.3.1:8443/aaa/servlet?login=admin&pwd=admin&IsBonding=1&IpPool=0&AcceptType=4101&FramedIpAddress=&QosIdentifier=BE_ERTPS&SaCd=PO01&SaId=30119144"; 
$WIMAX_WSDL="http://172.24.3.5:8888/DTV_API/Bridge?wsdl";

//$WIMAX_GET="TEST"; 
//$WIMAX_WSDL="TEST";

// VALUES FOR WIMAX 
$CINR=100;
$RSSI=100;

// VALUES FOR WIMAX 
$CINR_WX_Param=14;
$RSSI_WX_Param=-78;

// VALUES LTE FOR 3MB
$CINR_LTE_Param_3M=4;
$RSRP_LTE_Param_3M=-105;

// VALUES LTE FOR 6MB
$CINR_LTE_Param_6M=7;
$RSRP_LTE_Param_6M=-100;

/* SNMP v3 Config */
$SNMPv3_USER="DTVNET-SNMP1";
$SNMPv3_PASS="D1reC7V@Dm1n";

$set2default=FALSE;
?>
