<?php
/*! \file provisioningDefine.php
 *  \brief Provisioning SOAP methods
 *
 *	Contains all SOAP method registers for provisioning
 */


$server->register(
                /** register create method: Verifies the subscriber connection conditions and adds the user to the subscriber database */
                'Provisioning.can_create',
                array('subscriber_identity' => 'xsd:string', 'profile' => 'xsd:string',  'technology' => 'xsd:string')
                        ,array('return' => 'xsd:boolean')
                ,'','','',''
                ,'Verifies the subscriber connection conditions and adds the user to the subscriber database.'
);
 
$server->register(
		/** register create method: Verifies the subscriber connection conditions and adds the user to the subscriber database */
		'Provisioning.create', 
		array('subscriber_identity' => 'xsd:string', 'profile' => 'xsd:string',  'force_provisioning' => 'xsd:boolean', 'technology' => 'xsd:string')
			,array('return' => 'xsd:boolean')
		,'','','',''
		,'Verifies the subscriber connection conditions and adds the user to the subscriber database.'
);

$server->register(
        /** register read method: Returns all the subscriber information */
		'Provisioning.read',
        array('subscriber_identity' => 'xsd:string',  'technology' => 'xsd:string')
        ,array('profile' => 'xsd:integer','technology' => 'xsd:string')
        ,'','','',''
		,'Returns all the subscriber information.'
);

$server->register(
        /** register update method: Modify user in the subscriber database */
		'Provisioning.update',
		array('subscriber_identity' => 'xsd:string', 'profile' => 'xsd:string',  'technology' => 'xsd:string')
		,array('return' => 'xsd:boolean')
		,'','','',''
		,'Modify user in the subscriber database.'
		
);

$server->register(
        /** register delete method: Deletes the subscriber from the database */
		'Provisioning.delete',
        array('subscriber_identity' => 'xsd:string', 'technology' => 'xsd:string')
        ,array('return' => 'xsd:boolean')
        ,'','','',''
		,'Deletes the subscriber from the database.'
);

$server->register(
        /** register suspend method: Moves the user from the public pool to the suspended pool. Gives the posibility to display a suspension page when the customer tries to access the Internet */
        'Provisioning.suspend',
        array('subscriber_identity' => 'xsd:string', 'technology' => 'xsd:string')
        ,array('return' => 'xsd:boolean')
        ,'','','',''
		,'Moves the user from the public pool to the suspended pool. Gives the posibility to display a suspension page when the customer tries to access the Internet.'
);

$server->register(
        /** register unsuspend method: Moves the user from the suspended pool to the public pool */
        'Provisioning.unsuspend',
        array('subscriber_identity' => 'xsd:string', 'technology' => 'xsd:string')
        ,array('return' => 'xsd:boolean')
        ,'','','',''
		,'Moves the user from the suspended pool to the public pool.'
);


?>
