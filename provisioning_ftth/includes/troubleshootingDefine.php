<?php
/*! \file troubleshootingDefine.php
 *  \brief Troubleshooting SOAP methods
 *
 *	Contains all SOAP method registers for troubleshooting
 */


 
$server->register(
	/** register getParams method: Returns modem information and current connection parameters */
	'Troubleshooting.get_params',
	array('subscriber_identity' => 'xsd:string', 'technology' => 'xsd:string')
	,array('return' => 'tns:subscriber_info')
	,'','','',''
	,'Returns modem information and current connection parameters'
);

$server->register(
	/** register getAvailableFirmwares method: Displays a list of available firmwares for this particular modem */
	'Troubleshooting.get_available_firmwares',
	array('subscriber_identity' => 'xsd:string', 'technology' => 'xsd:string')
	,array('return' => 'tns:firmwares_array')
	,'','','',''
    ,'Displays a list of available firmwares for this particular modem'
);

$server->register(
	/** register uploadFirmware method: Upgrades/Downgrades the firmware of the specified subscriber */
    'Troubleshooting.upload_firmware',
     array('subscriber_identity' => 'xsd:string', 'firmware_name' => 'xsd:string', 'technology' => 'xsd:string')
     ,array('return' => 'xsd:boolean')
	 ,'','','',''
     ,'Upgrades/Downgrades the firmware of the specified subscriber'
);

$server->register(
	 /** register runSpeedTest method: Connects to the modem, executes a speedtest and returns the DL/UL rate in Kbps */
     'Troubleshooting.run_speed_test',
     array('subscriber_identity' => 'xsd:string', 'technology' => 'xsd:string')
     ,array('return' => 'tns:speedtest_result')
	 ,'','','',''
     ,'Connects to the modem, executes a speedtest and returns the DL/UL rate in Kbps'
);

$server->register(
	 /** register resetConnection method: Restarts the connection of a modem */
     'Troubleshooting.reset_connection',
     array('subscriber_identity' => 'xsd:string', 'technology' => 'xsd:string')
     ,array('return' => 'xsd:boolean')
	 ,'','','',''
     ,'Restarts the connection of a modem'
);

?>
