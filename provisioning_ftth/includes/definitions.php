<?php
$server->wsdl->addComplexType('subscriber_info','complexType','struct','all',
'',
array( 'ip_address' => array('name' => 'ip_address','type' => 'xsd:string', 'minOccurs' => '0', 'maxOccurs' => '1'),
'brand' => array('name' => 'brand','type' => 'xsd:string', 'minOccurs' => '0', 'maxOccurs' => '1'),
'model' => array('name' => 'model','type' => 'xsd:string', 'minOccurs' => '0', 'maxOccurs' => '1'),
'rsrp' => array('name' => 'rsrp','type' => 'xsd:integer', 'minOccurs' => '0', 'maxOccurs' => '1'),
'rssi' => array('name' => 'rssi','type' => 'xsd:string', 'minOccurs' => '0', 'maxOccurs' => '1'),
'cinr' => array('name' => 'cinr','type' => 'xsd:string', 'minOccurs' => '0', 'maxOccurs' => '1'),
'tx_power' => array('name' => 'tx_power','type' => 'xsd:string', 'minOccurs' => '0', 'maxOccurs' => '1'),
'ping_rtt' => array('name' => 'ping_rtt','type' => 'xsd:string', 'minOccurs' => '0', 'maxOccurs' => '1'),
'firmware_version' => array('name' => 'firmware_version','type' => 'xsd:string', 'minOccurs' => '0', 'maxOccurs' => '1'),
'bsid' => array('name' => 'bsid','type' => 'xsd:string', 'minOccurs' => '0', 'maxOccurs' => '1'),
'technology' => array('name' => 'technology','type' => 'xsd:string', 'minOccurs' => '0', 'maxOccurs' => '1'),
'olt_location' => array('name' => 'olt_location','type' => 'xsd:string', 'minOccurs' => '0', 'maxOccurs' => '1'),
'profile' => array('name' => 'profile','type' => 'xsd:string', 'minOccurs' => '0', 'maxOccurs' => '1'),
'oper_status' => array('name' => 'oper_status','type' => 'xsd:string', 'minOccurs' => '0', 'maxOccurs' => '1'),
'config_state' => array('name' => 'config_state','type' => 'xsd:string', 'minOccurs' => '0', 'maxOccurs' => '1'),
'onu_omci_state' => array('name' => 'onu_omci_state','type' => 'xsd:string', 'minOccurs' => '0', 'maxOccurs' => '1'),
'olt_rx_power' => array('name' => 'olt_rx_power','type' => 'xsd:string', 'minOccurs' => '0', 'maxOccurs' => '1'),
'ont_rx_power' => array('name' => 'ont_rx_power','type' => 'xsd:string', 'minOccurs' => '0', 'maxOccurs' => '1'),
'olt_slot' => array('name' => 'olt_slot','type' => 'xsd:string', 'minOccurs' => '0', 'maxOccurs' => '1'),
'olt_port' => array('name' => 'olt_port','type' => 'xsd:string', 'minOccurs' => '0', 'maxOccurs' => '1'),
'onu_ID' => array('name' => 'onu_ID','type' => 'xsd:string', 'minOccurs' => '0', 'maxOccurs' => '1'),
'distance' => array('name' => 'distance','type' => 'xsd:string', 'minOccurs' => '0', 'maxOccurs' => '1')
));

$server->wsdl->addComplexType('firmwares_array','complexType','struct','all',
'',
array( 'firmware_name' => array('name' => 'firmware_name','type' => 'xsd:string')
));

$server->wsdl->addComplexType('speedtest_result','complexType','struct','all',
'',
array( 'download_speed' => array('name' => 'download_speed','type' => 'xsd:integer'),
'upload_speed' => array('name' => 'upload_speed','type' => 'xsd:integer')
));
?>
