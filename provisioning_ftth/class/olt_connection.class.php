<?php
class olt_connection {

	private $user;
	private $password;
	private $olt_ip;
	private $olt_slot;
	private $olt_port;
	private $subs_id;
	private $connection;
	private $telnet;
	public $error;

    public function __construct ($olt_ip,$olt_slot,$olt_port,$subs_id){		
        // $this->user='admins';
		// $this->password='zhone';
		$this->user = _oltUser;
		$this->password = _oltPass;
		$this->olt_ip = $olt_ip;
		$this->olt_slot = $olt_slot;
		$this->olt_port = $olt_port;
		$this->subs_id = $subs_id;
       	$this->telnet = new PHPTelnet(); 
    }

    public function __destruct(){
		$this->telnet->Disconnect();
    }

    public function connect(){
		$this->connection = $this->telnet->Connect($this->olt_ip, $this->user, $this->password);
		if($this->connection==0){			
			return true;
		}
		else{
            $this->error = "Telnet loggin problem";
            return false;
        }
    }

    public function test (){
		$command= "ip show";
		$this->telnet->DoCommand($command,$result);
		return $result;
    }

	public function get_free_onuid(){
		$command="onu show $this->olt_slot/$this->olt_port";
		$this->telnet->DoCommand($command,$result);
		$result_array = explode("\n",$result);
		$inner_array = array();
		$free_onus_array = array();

		foreach ($result_array as $line){
			trim($line);
			if (preg_match('/^\s*\d/',$line)){
				$inner_array = preg_split('/[\s]+/',$line);
				array_shift($inner_array);
				array_push($free_onus_array,$inner_array);
			}
		}
		if (is_numeric($free_onus_array[0][0]) && ($free_onus_array[0][0] > 0)){ 
		return $free_onus_array[0][0];
		}else{
		syslog(LOG_WARNING,"No more logic slots available for olt: $this->olt_ip on slot $this->olt_slot and port $this->oltport");
		exit;
		}
	}


	public function create ($onu_id,$profile){

		//INICIO DES-SETEO DE ZMS PARA GARANTIZAR PRIORIDAD
		$command="update system zmsexists = false zmsipaddress = 0.0.0.0 0";
		$this->telnet->DoCommand($command,$result);

		$command="resetcliprov all";
		$this->telnet->DoCommand($command,$result);

		$command="setline 0";
		$this->telnet->DoCommand($command,$result);
		//FIN DES-SETEO DE ZMS PARA GARANTIZAR PRIORIDAD

	#########################################################################
	#########################ONU_SET#########################################	

	$command = "onu set $this->olt_slot/$this->olt_port/$onu_id vendorid ZNTS".
	" serno fsan ".str_replace('ZNTS','',$this->subs_id)." meprof zhone-2426";
	syslog(LOG_NOTICE,'create_onu_command '.$command);
	$this->telnet->DoCommand($command,$result);
	
	$result_array = explode("\n",$result);
	#var_dump($result_array);

	$success = "successfully enabled with serial number ZNTS ".
		   str_replace('ZNTS','',$this->subs_id)." Onu $this->olt_slot/$this->olt_port/$onu_id";
	$message = trim($result_array[1])." ".trim($result_array[2]);

##
	$line_count=0;
	foreach($result_array as $line_string){
 		syslog(LOG_NOTICE,'result_create_onu for '.$this->subs_id.' '.$line_count.' '.trim($line_string));
	$line_count++;
	}
##
		if(strpos($message,$success)){

			syslog(LOG_NOTICE,"create_onu OK for $this->subs_id");

		}else{

			syslog(LOG_NOTICE,"create_onu NOK for $this->subs_id");
	                // FIN SETEO DE ZMS PARA GARANTIZAR PRIORIDAD
	                $command="update system zmsexists = true zmsipaddress = 172.20.4.60 0";
	                $this->telnet->DoCommand($command,$result);
	
	                #$command="configsync initiate partial";
	                #$this->telnet->DoCommand($command,$result);
	                // FIN SETEO DE ZMS PARA GARANTIZAR PRIORIDAD
	                $this->telnet->DoCommand('logout',$result);
	
			return false;
		}
	
	$vlan = $GLOBALS['global_config']['vlans'][$this->olt_ip];
	$profile_down = $GLOBALS['global_config']['profiles'][$profile]['down'];		
	$profile_up = $GLOBALS['global_config']['profiles'][$profile]['up'];		
	$command = "bridge add 1-$this->olt_slot-$this->olt_port-$onu_id/gpononu gtp $profile_down downlink".
	" vlan $vlan tagged epktrule $profile_up g-vlan 200 rg";	

        $this->telnet->DoCommand($command,$result);
        $result_array = explode("\n",$result);
        #var_dump($result_array);
##

	$line_count=0;
	foreach($result_array as $line_string){
 		syslog(LOG_NOTICE,'result_create_bridge for '.$this->subs_id.' '.$line_count.' '.trim($line_string));
	$line_count++;
	}

##

if(((strpos(trim($result_array[2]),"Created bridge")===0) && ((strpos(trim($result_array[3]),"been created"))))
	||(strpos(trim($result_array[0]),"epktrule"))){

			syslog(LOG_NOTICE,"create_bridge OK for $this->subs_id");
	}else{
		syslog(LOG_NOTICE,"create_bridge NOK for $this->subs_id");

	        // FIN SETEO DE ZMS PARA GARANTIZAR PRIORIDAD
                $command="update system zmsexists = true zmsipaddress = 172.20.4.60 0";
                $this->telnet->DoCommand($command,$result);

                #$command="configsync initiate partial";
                #$this->telnet->DoCommand($command,$result);
                // FIN SETEO DE ZMS PARA GARANTIZAR PRIORIDAD
                $this->telnet->DoCommand('logout',$result);

		return false;
	}

		// FIN SETEO DE ZMS PARA GARANTIZAR PRIORIDAD
		$command="update system zmsexists = true zmsipaddress = 172.20.4.60 0";

		#$command="configsync initiate partial";
		#$this->telnet->DoCommand($command,$result);
		// FIN SETEO DE ZMS PARA GARANTIZAR PRIORIDAD
                $this->telnet->DoCommand('logout',$result);
		
	return true;

	}//end_create



public function update_profile ($interface,$profile){

		//INICIO DES-SETEO DE ZMS PARA GARANTIZAR PRIORIDAD
		$command="update system zmsexists = false zmsipaddress = 0.0.0.0 0";
		$this->telnet->DoCommand($command,$result);

		$command="resetcliprov all";
		$this->telnet->DoCommand($command,$result);

		$command="setline 0";
		$this->telnet->DoCommand($command,$result);
		//FIN DES-SETEO DE ZMS PARA GARANTIZAR PRIORIDAD

	$vlan = $GLOBALS['provisioning_config']['vlans'][$this->olt_ip];
	$prof = $GLOBALS['provisioning_config']['profiles'][$profile]['up'];

	$command = "bridge delete $interface";
 	syslog(LOG_NOTICE,'command_update: '.$command);
	$this->telnet->DoCommand($command,$result1);
##
	$result_array1 = explode("\n",$result1);
	$line_count=0;
	foreach($result_array1 as $line_string){
 		syslog(LOG_NOTICE,'result_update1 for '.$this->subs_id.' '.$line_count.' '.trim($line_string));
	$line_count++;
	}

	sleep(5);
	$this->telnet->DoCommand('showdatetime',$result1);

##
	$command2 = "bridge add $interface/gpononu gtp $prof downlink vlan $vlan tagged epktrule $prof g-vlan 200 rg";
 	syslog(LOG_NOTICE,'command_update2: '.$command2);
        $this->telnet->DoCommand($command2,$result);
	$result_array = array();
        $result_array = explode("\n",$result);
##
	$line_count=0;
	foreach($result_array as $line_string){
 		syslog(LOG_NOTICE,'result_update2 for '.$this->subs_id.' '.$line_count.' '.trim($line_string));
	$line_count++;
	}

##

if(((strpos(trim($result_array[2]),"Created bridge")===0) && ((strpos(trim($result_array[3]),"been created"))))
	||(strpos(trim($result_array[0]),"epktrule"))){

		syslog(LOG_NOTICE,"update_bridge OK for $this->subs_id");
	        // FIN SETEO DE ZMS PARA GARANTIZAR PRIORIDAD
                $command="update system zmsexists = true zmsipaddress = 172.20.4.60 0";
                $this->telnet->DoCommand($command,$result);

                #$command="configsync initiate partial";
                #$this->telnet->DoCommand($command,$result);
                // FIN SETEO DE ZMS PARA GARANTIZAR PRIORIDAD
                $this->telnet->DoCommand('logout',$result);
		return true;
	}else{
		syslog(LOG_NOTICE,"update_bridge NOK for $this->subs_id");
	        // FIN SETEO DE ZMS PARA GARANTIZAR PRIORIDAD
                $command="update system zmsexists = true zmsipaddress = 172.20.4.60 0";
                $this->telnet->DoCommand($command,$result);

                #$command="configsync initiate partial";
                #$this->telnet->DoCommand($command,$result);
                // FIN SETEO DE ZMS PARA GARANTIZAR PRIORIDAD
                $this->telnet->DoCommand('logout',$result);
		return false;
	}
}


public function delete ($interface){
	//INICIO DES-SETEO DE ZMS PARA GARANTIZAR PRIORIDAD
	$command="update system zmsexists = false zmsipaddress = 0.0.0.0 0";
	$this->telnet->DoCommand($command,$result);
	// syslog(LOG_NOTICE,'COMMAND update: result_delete for '.$this->subs_id.': '.str_replace("\n","-",$result));
	$command="resetcliprov all";
	$this->telnet->DoCommand($command,$result);
	// syslog(LOG_NOTICE,'COMMAND reset: result_delete for '.$this->subs_id.': '.str_replace("\n","-",$result));
	$command="setline 0";
	$this->telnet->DoCommand($command,$result);
	// syslog(LOG_NOTICE,'COMMAND setline: result_delete for '.$this->subs_id.': '.str_replace("\n","-",$result));
	//FIN DES-SETEO DE ZMS PARA GARANTIZAR PRIORIDAD

    $command="onu delete $interface";
 
    $this->telnet->DoCommand($command,$result);
	// syslog(LOG_NOTICE,'COMMAND onu delete: result_delete for '.$this->subs_id.': '.str_replace("\n","-",$result));
    $this->telnet->DoCommand('yes',$result);
	// syslog(LOG_NOTICE,'COMMAND yes 1: result_delete for '.$this->subs_id.': '.str_replace("\n","-",$result));
    $this->telnet->DoCommand('no',$result);
	// syslog(LOG_NOTICE,'COMMAND yes 2: result_delete for '.$this->subs_id.': '.str_replace("\n","-",$result));
    $this->telnet->DoCommand('yes',$result);
	// syslog(LOG_NOTICE,'COMMAND yes 3: result_delete for '.$this->subs_id.': '.str_replace("\n","-",$result));

	$result_array = explode("\n",$result);
	$line_count=0;
	$result_delete = false;	

	foreach($result_array as $line_string){
		if (strpos(trim($result_array[2]),"deleting ONU")===0){
			syslog(LOG_NOTICE,'result_delete for '.$this->subs_id.' deleted ok!!');
			$result_delete = true;
			break;
		}
		$line_count++;
	}
	if($result_delete==false){
		syslog(LOG_NOTICE,'result_delete for '.$this->subs_id.' deleted with ERROR: '.str_replace("\n","-",$result));
	}
	// FIN SETEO DE ZMS PARA GARANTIZAR PRIORIDAD
    $command="update system zmsexists = true zmsipaddress = 172.20.4.60 0";
    $this->telnet->DoCommand($command,$result);
    // FIN SETEO DE ZMS PARA GARANTIZAR PRIORIDAD
	return $result_delete;
}


}//end_class


