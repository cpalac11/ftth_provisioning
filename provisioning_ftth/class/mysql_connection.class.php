<?php
class db_connection {
	
	private $hostname;
	private $user;
	private $password;
	private $database;
	private $connection;
	public $error;

    public function __construct (){

		$this->hostname=_MYSQL_HOST;
        $this->user=_MYSQL_USER;
        $this->password=_MYSQL_PASS;
        $this->database=_MYSQL_DB;
		/*
		$this->hostname='172.20.4.54';
        $this->user='ADMIN_PROV';
        $this->password='Desa8875.!';
        $this->database='provisioningPROD';
		*/
		if(!$this->connect()){
			$this->error=mysqli_connect_error();
			syslog(LOG_ERR,'MYSQL'.$this->error);
        }
    }

    public function __destruct(){
		mysqli_close($this->connection);
	}

    private function connect (){
		$this->connection = mysqli_connect($this->hostname, $this->user, $this->password, $this->database);		
		if($this->connection){
			return true;
		}
		else{			
			$this->error = mysqli_connect_error();
			return false;
		}
    }

	public function test (){
		$query = "SELECT * FROM subscriber LIMIT 10";
		$result = mysqli_query($this->connection, $query);
		$row = mysqli_fetch_row($result);
		if (mysqli_num_rows($result) > 0) {
			print_r($row);
			return true;
		}
		else{
			$this->error = mysqli_connect_error();
			return false;
		}
	}

	public function is_provisioned ($subs_id){
		$query="SELECT provisioned FROM subscriber WHERE subscriber_identity='$subs_id'";
		$result = mysqli_query($this->connection, $query);
		$row = mysqli_fetch_row($result);
		if(is_null($row[0])){
			return true;
		}
		else{
			return false;
		}				
	}
		
	public function check_action_config($subs_id,$olt_ip,$olt_slot,$olt_port){	
		$query = "SELECT * FROM trap_action WHERE olt_ip='$olt_ip'".
		" and subscriber_identity='$subs_id' and action_type='1' and state=0 and".
		" slot='$olt_slot' and port='$olt_port'";
        $result = mysqli_query($this->connection, $query);
        if (mysqli_num_rows($result) == 0){
			return false; 
		}
		else{
			return true;
		}
	}

	public function add_action_config($subs_id,$olt_ip,$olt_slot,$olt_port){
		$query="INSERT INTO  trap_action (create_date,olt_ip,fsan,".
			"subscriber_identity,slot,port,state,action_type)".
			" values (NOW(),'$olt_ip','','$subs_id','$olt_slot','$olt_port',".
			"'0','1')";                
		$result = mysqli_query($this->connection, $query);
		if($result){
			return true;
		}
		else{
			return false;
		}
	}

	public function get_profile($subs_id){
		$query = "SELECT profile FROM subscriber WHERE subscriber_identity='$subs_id' LIMIT 1";
		$result = mysqli_query($this->connection, $query);
		$row = mysqli_fetch_assoc($result);
		if(!array_key_exists($row['profile'],$GLOBALS['global_config']['profiles'])){
			return false;	
		}
		else{
			return $row['profile'];
		}
	}	

	public function get_trap_to_process(){
        $query = "SELECT *, DATEDIFF(NOW(),create_date) FROM trap_action WHERE action_type=1 order by id";
		$result = mysqli_query($this->connection, $query);
		if(mysqli_num_rows($result) == 0){
			echo "nothing to do....\n";
			exit;
		}
		$result_array = array();
		while($row = mysqli_fetch_assoc($result)) {
			array_push($result_array, array('subs_id' => $row['subscriber_identity'], 'olt_ip' => $row['olt_ip'], 'olt_slot' => $row['slot'], 
			'olt_port' => $row['port'], 'trap_id' => $row['id']));
		}
		return $result_array;
	}

	public function get_delete_traps(){
		$query = "select id,subscriber_identity from trap_action where action_type = 2";
		$result = mysqli_query($this->connection, $query);
		if(mysqli_num_rows($result)==0){
			echo "nothing to do....\n";
			exit;
		}
		$result_array=array();
		while($row = mysqli_fetch_assoc($result)) {
			array_push($result_array,array('trap_id' => $row['id'], 'subs_id' => $row['subscriber_identity']));
		}
		return $result_array;
	}

	public function get_connection_info($subs_id){
		$query = "SELECT olt_slot,olt_port,olt_onuid,olt_ip FROM subscriber_info WHERE subscriber_identity='$subs_id' LIMIT 1";
		//return($this->database);die();
		$result = mysqli_query($this->connection, $query);
		$row=mysqli_fetch_assoc($result);
		//return(mysqli_num_rows($result));die();
		if(mysqli_num_rows($result) == 1){
			$result_array = array('olt_port'=>$row['olt_port'], 'olt_slot'=>$row['olt_slot'], 'olt_onuid'=>$row['olt_onuid'], 'olt_ip'=>$row['olt_ip']);                				
			return $result_array;				
		}
		else{
			$result_array = array('olt_slot' => 'null', 'olt_port'=>'null', 'olt_onuid'=>'null');
			return $result_array;
		}
	}

	public function update_subscriber($subs_id){
		$query="UPDATE subscriber set provisioned=NOW() WHERE subscriber_identity='$subs_id'";
		$result = mysqli_query($this->connection, $query);
		if ($result){
			return true;
		}
		else{
			return false;
		}
	}

	public function update_subscriber_info($olt_ip,$olt_slot,$olt_port,$onu_id,$subs_id,$profile_up,$profile_down){
		$query="UPDATE subscriber_info set olt_ip='$olt_ip', olt_slot='$olt_slot', olt_port='$olt_port', olt_onuid='$onu_id', upload_speed='$profile_up', 
				download_speed='$profile_down' where subscriber_identity='$subs_id'";
		$result = mysqli_query($this->connection, $query);
		if ($result){
			return true;
		}
		else{
			return false;
		}
	}

	public function update_profile($subs_id,$profile){		
	    $qos_hash = array(
		"2MB" => "2000",
		"3MB" => "3000",
		"6MB" => "6000",
		"12MB" => "12000",
		"10MB" => "10000",
		"30MB" => "30000",
		"100MB" => "100000"
	    );
		$query = "UPDATE subscriber_info  set upload_speed={$qos_hash[$profile]}, download_speed={$qos_hash[$profile]} where subscriber_identity='$subs_id'";
		syslog(LOG_INFO,"update query: $query");
		$result = mysqli_query($this->connection, $query);
		if($result){
			return true;
		}
		else{
			return false;
		}
	}

	public function update_deleted($subs_id,$profile){
		$query1="DELETE FROM subscriber WHERE subscriber_identity='$subs_id'";
        $result1 = mysqli_query($this->connection, $query1);
		$query2="DELETE FROM subscriber_info WHERE subscriber_identity='$subs_id'";
        $result2 = mysqli_query($this->connection, $query2);
		$query3="DELETE FROM subs_ftth WHERE fsan='$subs_id'";
        $result3 = mysqli_query($this->connection, $query3);		
		return ($result1&&$result2);
	}
		
	public function delete_action_trap($trap_id){		
		$query="DELETE FROM trap_action WHERE id='$trap_id'";
		$result = mysqli_query($this->connection, $query);
		if ($result){
			return true;
		}
		else{
			return false;
		}
	}

	public function delete_subscriber($subs_id){		
		$query = "DELETE FROM subscriber WHERE subscriber_identity='$subs_id'";
		$result = mysqli_query($this->connection, $query);
		if($result){
			return true;
		}
		else{
			return false;
		}
	}

	public function delete_subscriber_info($subs_id){		
		$query = "DELETE FROM subscriber_info WHERE subscriber_identity='$subs_id'";
		$result = mysqli_query($this->connection, $query);
		if ($result){
			return true;
		}
		else{
			return false;
		}
	}

    public function update_trap_action($trap_id){
		$query = "UPDATE trap_action set action_type=10 WHERE id='$trap_id'";
		$result = mysqli_query($this->connection, $query);
		if ($result){
			return true;
		}
		else{
			return false;
		}
    }

    public function update_delete_trap($trap_id){
		$query="UPDATE trap_action set action_type=20 WHERE id='$trap_id'";
		$result = mysqli_query($this->connection, $query);
		if ($result){
			return true;
		}
		else{
			return false;
		}
    }
	
	public function delete_old_traps(){
		$query = 'SELECT *,TIMESTAMPDIFF(HOUR,create_date,now()) AS old FROM trap_action where action_type=1'; 
		$result = mysqli_query($this->connection, $query);
		if(mysqli_num_rows($result) == 0){
			echo "nothing to do....\n";
			exit;
		}
		while($row = mysqli_fetch_assoc($result)){		
			if($row['old'] >= 2){
				$inner_query = "DELETE FROM trap_action where id='".$row['id']."'";
				$inner_result = mysqli_query($this->connection, $inner_query);
				if ($inner_result){
					syslog(LOG_NOTICE,'cleaned : trap id '.$row['id'].
					' for '.$row['subscriber_identity'].' olt '.$row['olt_ip'].
					' slot '.$row['slot']. ' port '.$row['port']);						
				}
			}
        }
	}

	public function delete_type2_traps(){
		$query ='SELECT id,TIMESTAMPDIFF(DAY,create_date,now()) AS old FROM trap_action where action_type=2'; 
		$result = mysqli_query($this->connection, $query);
		if(mysqli_num_rows($result) == 0){
			echo "nothing to do....\n";
			exit;
		}
		while($row = mysqli_fetch_assoc($result)){
			if($row['old'] >= 1){
				$inner_query = "DELETE FROM trap_action where id='".$row['id']."'";
				$inner_result = mysqli_query($this->connection, $inner_query);
				if ($inner_result){
					syslog(LOG_NOTICE,'cleaner_type=2 : trap id '.$row['id'].
					' from action traps');	
				}
			}
		}
	}

	public function add_log($subs_id, $request, $response){
		$ip = $_SERVER['REMOTE_ADDR'];
		$query = "INSERT INTO provisioningPROD.log (subscriber_identity,request,response,fecha,ip)
			VALUES ('$subs_id','$request','$response',NOW(),'$ip')";
		$result = mysqli_query($this->connection, $query);
		return $result;
	}
}