<?php
include('Net/SSH2.php');
include('telnet.class.php');
/*! \file connection.class.php
    \brief Este archivo es el principal de la clase Connection.

    Esta archivo contiene la clase y los metodos para la configuracion de la OLT.
*/



//!  Esta es la clase Connection que sirve para configurar los perfiles de las ONT en las OLT.
/*!
  \brief     Clase Connection
  \author    Lucas Lorenzo
  \version   1.0
  \date      March 2015
  \details   Esta clase contiene los metodos para conectarse por SSH a las OLT y configurar los perfiles de las ONT en base a la configuración de los TRAP recibidos por las OLT.
*/
define('NET_SSH2_LOGGING', NET_SSH2_LOG_COMPLEX);
class Connection extends Model {
	var $connection;

	public function getSNMPv3($ip,$param,$slot,$port,$shelf,$onu_id) {
		$flag = "";
		if ( $onu_id == "" ) return -1;
		
		$remove_array = array(
				'Hex-STRING: ','Gauge32: ','INTEGER: ','STRING:'
				);
	
                syslog(LOG_INFO,"onu_id : $onu_id\n");

		switch ($param) {
			case 'distance':
				$oid="1.3.6.1.4.1.5504.5.14.1.7.1.6.$onu_id";
				break;
			case 'rxPowerOlt':
                $oid="1.3.6.1.4.1.5504.5.14.1.7.1.3.$onu_id";
                break;	
			case 'rxPowerOnt':
				$oid="1.3.6.1.4.1.5504.5.14.1.7.1.4.$onu_id";
                 break;
			case 'onuOperationalState':
                $oid="1.3.6.1.2.1.2.2.1.8.$onu_id";
                break;
			case 'onuStatusWord':
				$oid="1.3.6.1.4.1.5504.5.14.1.7.1.1.$onu_id";
				break;
			case 'onuOmciState':	
				$oid="1.3.6.1.4.1.5504.5.14.1.7.1.2.$onu_id";
				break;
			case 'location':	
				$oid="SNMPv2-MIB::sysName.0";
				break;
			default:
				$oid=-1;
				break;
		}	
		if ($oid == -1 ) return -1;

		#$value= snmp2_get($ip,'DTVNET-SNMP',$oid);
		$value= snmpget($ip,'DTVNET-SNMP',$oid);
                syslog(LOG_INFO,"value_afer: $value flag: $flag for for oid ".$oid."\n");
		$value = str_replace($remove_array,'',$value);
                syslog(LOG_INFO,"value_desp: $value flag: $flag for for oid ".$oid."\n");


		if ($param == "onuStatusWord" ) {
				$value_bin=base_convert($value,16,2);
				$valueMIB=substr($value_bin,0,23);
				$index=0;
				$value='';
				
				while ($index <= 23 ) {
                       $flag = $valueMIB[$index];
                       if ($flag == "1" ) {
								
					   	switch ($index) {
						case '0':
							$valueTemp="noAlarm";
						break;
						case '1':
							$valueTemp="inactive";
						break;
						case '2':
							$valueTemp="lossOfSignal";
						break;
						case '3':
							$valueTemp="lossOfFrame";
						break;
						case '4':
							$valueTemp="driftOfWindow";
						break;
						case '5':
							$valueTemp="signalFail";
						break;
						case '6':
							$valueTemp="signalDegrade";
						break;
						case '7':
							$valueTemp="lossOfGemChanDelin";
						break;
						case '8':
							$valueTemp="remoteDefect";
						break;
						case '9':
							$valueTemp="transmitterFailure";
						break;
						case '10':
							$valueTemp="startUpFailure";
						break;
						case '11':
							$valueTemp="lossOfAck";
						break;
						case '12':
							$valueTemp="dyingGasp";
						break;
						case '13':
							$valueTemp="lossOfPloamSync";
						break;
						case '14':
							$valueTemp="messageError";
						break;
						case '15':
							$valueTemp="physEquipError";
						break;
						case '16':
							$valueTemp="bit16notUsed";
						break;
						case '17':
							$valueTemp="bit17notUsed";
						break;
						case '18':
							$valueTemp="excessiveBipErrorsOnuDisabled";
						break;
						case '19':
							$valueTemp="excessiveBipErrorsOnuNotDisabled";
						break;
						case '20':
							$valueTemp="usRxPowerErrorOnuDisabled";
						break;
						case '21':
							$valueTemp="usRxPowerErrorOnuNotDisabled";
						break;
						case '22':
							$valueTemp="rogueOnu";
						break;
						}	
						
						$value=$value.'+'.$valueTemp;
					}  
                       $index=$index+1;
                }
                
                if (substr($value,0,1) == '+' ) $value=substr($value,1);
				
		}
		
		if ($param == "location" ) {
				$value =snmpget($ip,'DTVNET-SNMP',$oid);
				$value = str_replace($remove_array,'',$value);
		}
		
		if ($param == "onuOmciState" ) {
				
				
			switch ($value) {
				case '1':
					$value='waiting - queued for configuration';
					break;
				case '2':
                	$value='pending - transitional state before configuration';
                	break;	
				case '3':
					$value='config - in process of configuration';
                 	break;
				case '4':
                	$value='error - unspecified error';
                	break;
				case '5':
					$value='done - configuration successful';
					break;
				case '6':
                	$value='download - image download in progress';
                	break;	
				case '7':
					$value='aborted - image download aborted';
                 	break;
				case '8':
                	$value='unknown - state cannot be determined';
                	break;
                case '9':
					$value='down - ONU is down';
					break;
				case '10':
                	$value='noConfig - no configuration has been attempted';
                	break;	
				case '11':
					$value='rgComError - ONU unreachable for RG configuration';
                 	break;
				case '12':
                	$value='rgServiceSetupErr - Some RG commands failed, may impact service';
                	break;
                case '13':
					$value='omciFailed - OMCI configuration failed';
					break;
				case '14':
                	$value='omciErrAndRgComErr - OMCI failed and also ONU unreachable for RG';
                	break;	
				case '15':
					$value='omciErrAndRgServErr - OMCI failed and also RG commands failed';
                 	break;	
			}	
				
		}
		
				
		return $value;
	}
	public function setTrapToProcess($id,$state,$subscriber, $port, $slot) {
		$query="UPDATE ".$this->DB.".trap_action set state='$state',modify_date=NOW() WHERE id='$id'";
		$res= mysqli_query($this->connection, $query);

		$query="INSERT INTO ".$this->DB.".trap_action_history ( select * from ".$this->DB.".trap_action WHERE id='$id')";
		$res= mysqli_query($this->connection, $query);

		$query="DELETE FROM ".$this->DB.".trap_action  WHERE id='$id'";
		$res= mysqli_query($this->connection, $query);
		$query="DELETE FROM ".$this->DB.".trap_action  WHERE subscriber_identity='$subscriber' and port='$port' and slot='$slot'";
                $res= mysqli_query($this->connection, $query);
		
	}

	public function addActionConfig($fsan,$profileDown, $profileUp, $oltIp, $oltSlot, $oltPort,$subscriberIdentity,$onuID){


	
		$query="SELECT * FROM ".$this->DB.".trap_action WHERE olt_ip='$oltIp' and fsan='$fsan' and action_type='1' and state=0 and slot='$slot' and port='$oltPort' and onuID='$onuID'";
		$res= mysqli_query($this->connection, $query);
		if ( mysqli_num_rows($res) == 0 ) {
                	$query="INSERT INTO ".$this->DB.".trap_action (id,create_date,olt_ip,fsan,subscriber_identity,port,slot,profile_down,profile_up,state,action_type,onuID) values ('',NOW(),'$oltIp','$fsan','$subscriberIdentity','$oltSlot','$oltPort','$profileDown','$profileUp','0','1','$onuID')";
                	$res= mysqli_query($this->connection, $query);
		}
	}

	public function deleteActionConfig($oltIp, $oltSlot, $oltPort, $oltOnuID, $subscriberIdentity) {
                $query="INSERT INTO ".$this->DB.".trap_action (id,create_date,olt_ip,subscriber_identity,slot,port,olt_onu,state,action_type) values ('',NOW(),'$oltIp','$subscriberIdentity','$oltSlot','$oltPort','$oltOnuID','0','2')";
                $res= mysqli_query($this->connection, $query);
	}

        private function zeroFill($subscriberIdentity,$long) {
                while (strlen($subscriberIdentity) < $long ) {
                        $subscriberIdentity="0".$subscriberIdentity;
                }
                return $subscriberIdentity;
        }


	public function getTrapToProcess() {
		global $trapTimeOut;
		
		

		if (strlen(trim($trapTimeOut)) == "" ) $trapTimeOut=1;

		$susbscriber=new Model();
		$query="SELECT *, DATEDIFF(NOW(),create_date) FROM ".$this->DB.".trap_action WHERE state=0 order by id";
		$res= mysqli_query($this->connection, $query);
		// $result=mysqli_fetch_assoc($res);
		while($row = mysqli_fetch_assoc($res)) {
			if ( $row['old'] >= $trapTimeOut ) {
				$query="INSERT INTO ".$this->DB.".trap_action_history (SELECT * FROM ".$this->DB.".trap_action where id='".$row['id']."')";
				$res= mysqli_query($this->connection, $query);
				
				$query="DELETE FROM ".$this->DB.".trap_action where id='".$row['id']."'";
				$res= mysqli_query($this->connection, $query);
			} else {
        			switch($row['action_type']) {
                			case 1:
						//$row['subscriber_identity']=$this->zeroFill($row['subscriber_identity'],12);
						$row['subscriber_identity']="ZNTS".strtoupper($row['fsan']);
                        			$susbscriberInfo=$susbscriber->subscriberState($row['subscriber_identity'],'FTTH',true);
						echo "\nsusbscriberInfo: $susbscriberInfo ";
                        			
						$profileValue= trim($susbscriberInfo['profile']);

	
									switch ($profileValue) {
										case "3MB":
											$profileUpValue="3000";
											$profileDownValue="3000";
										break;
										
										case "6MB":
											$profileUpValue="6000";
											$profileDownValue="6000";
										break;
										
										case "12MB":
											$profileUpValue="12000";
											$profileDownValue="12000";
										break;
										
										case "30MB":
											$profileUpValue="30000";
											$profileDownValue="30000";
										break;
										
										case "50MB":
											$profileUpValue="50000";
											$profileDownValue="50000";
										break;
										
										case "100MB":
											$profileUpValue="100000";
											$profileDownValue="100000";
										break;
										
									}
							
	


						if ( $susbscriberInfo > 0 and  trim($susbscriberInfo['provisioned']) == "" ) {
								
                                			$resultConfig=$this->oltConfig($row['fsan'], $profileDownValue,$profileUpValue, $row['olt_ip'], $row['slot'], $row['port'],$row['subscriber_identity'],$row['onuID']);
                                			if ( $resultConfig > 0 ) {
                                	        		$this->setTrapToProcess($row['id'],2,$row['subscriber_identity'],$row['port'],$row['slot']);
                                			}
                        			}
                        			break;
                			case 2:
                        			$resultConfig=$this->deleteOnuID($row['olt_ip'],$row['slot'], $row['port'],$row['olt_onu']);
                        			if ( $resultConfig > 0 ) {
                                			$this->setTrapToProcess($row['id'],2,$row['subscriber_identity'],$row['port'],$row['slot']);
                        			}
                        			break;
        			}
			}
		}

        }

	
	public function  
	//! FUNCTION olt_config que sirve para configurar el perfil en la OLT
        /*! Se encarga de conectarse por SSH y configurar el perfil de una ONT en la OLT especificada. */
	oltConfig($fsan,$profileDown, $profileUp, $oltIp, $oltSlot, $oltPort,$subscriberIdentity,$onuId){
			
		global $SSH_USER,$SSH_PASS,$set2default;
		/*
		Cambio firmware
		*/
		$oltSlot_aux=$oltSlot;
		$oltSlot=$oltPort;
		$oltPort=$oltSlot_aux;

		$log=new Log();
		$log->add("Configurando la OLT\n");
		$log->add("IP OLT: $oltIp");
		

		switch ($oltIp) {
				case "172.18.17.25":
					$vlan="1400";
				break;

				case "172.18.16.1":
					$vlan="1400";
				break;
						
				case "172.18.17.1":
					$vlan="1400";
				break;
				
				case "172.18.16.9":
					$vlan="100";
				break;
				
				case "172.18.17.9":
					$vlan="100";
				break;
				
				case "172.18.16.17":
					$vlan="1400";
				break;
				
				case "172.18.17.17":
					$vlan="1400";
				break;

				case "172.20.16.1":
					$vlan="1400";
				break;
				
				case "172.20.17.1":
					$vlan="1400";
				break;
				
				case "172.18.16.61":
					$vlan="200";
				break;

				case "172.24.100.1":
					$vlan="1400";
				break;
	
				case "172.20.17.249":
					$vlan="1400";

				break;
			}
		

		$log->add("VLAN a utilizar: $vlan\n");

		set_include_path('Net/');
		echo "\nINCLUDE SSH2.php OP:1\n";
		// include('Net/SSH2.php');
		// include('telnet.class.php');
		// var $ONU_ID;
	
		//$onuId=$this->getOnuID($oltIp, $oltSlot, $oltPort);	
		$log->add("FSAN: $fsan");	
		$this->trapLogger($oltIp,$fsan,$subscriberIdentity,$oltSlot,$oltPort);
		$log->add("onuId: $onuId\n"); 
		$log->add("IP: $oltIp\n");
		//$ssh = new Net_SSH2($oltIp);
		//if (!$ssh->login($this->SSH_USER, $this->SSH_PASS)) {
			//$log->add("LOG SSH Authentication Fail\n");

    			// LOG Authentication Fail
			$telnet = new PHPTelnet();
			$result = $telnet->Connect($oltIp,$this->SSH_USER,$this->SSH_PASS);
			$log->add("result:".$result."\n");
			if ($result == 0) {
				$log->add("LOG TELNET Authentication Successful!\n");
				
				
				
				// INICIO DES-SETEO DE ZMS PARA GARANTIZAR PRIORIDAD
				$command="update system zmsexists = false zmsipaddress = 0.0.0.0 0";
				$telnet->DoCommand($command,$result);
				$log->add("result: ".$result."\n");
				
				$command="resetcliprov all";
				$telnet->DoCommand($command,$result);
				$log->add("result: ".$result."\n");
				
				$command="setline 0";
				$telnet->DoCommand($command,$result);
				$log->add("result: ".$result."\n");
				// FIN DES-SETEO DE ZMS PARA GARANTIZAR PRIORIDAD

				$command="onu set $oltSlot/$oltPort/$onuId vendorid ZNTS serno fsan $fsan meprof zhone-2426";
				$log->add("LOG COMMAND: $command\n");
				$telnet->DoCommand($command,$result);
				$log->add("result: ".$result."\n");
				
				
				
				if ( trim(strpos($result,"is already enabled with serial number")) !=  ""  ) {
					$log->add("ONU ID YA UTILIZADO. INCREMENTANDO INDEX...\n");
					$log->add("LOG RESULT:".$result."\n");
					
					
					
					$log->add("VALOR ACTUAL DE ONU ID:".$onuId."\n");
					
					if ($onuId==64 && ($oltIp=='172.18.16.9' || $oltIp=='172.18.17.9')){
						$onuId=0;
					}
				
					if ($onuId==126){
						$onuId=0;
					}
				
					$onuId=$onuId+1;
					
					$log->add("NUEVO VALOR:".$onuId."\n");
					
					
					// INICIO SETEO DE ZMS PARA GARANTIZAR PRIORIDAD
					$command="update system zmsexists = true zmsipaddress = 172.20.4.60 0";
					$telnet->DoCommand($command,$result);
					$log->add("result: ".$result."\n");
				
					$command="configsync initiate partial";
					$telnet->DoCommand($command,$result);
					$log->add("result: ".$result."\n");
					// FIN SETEO DE ZMS PARA GARANTIZAR PRIORIDAD
					
					$telnet->Disconnect();
					
					$this->oltConfig($fsan,$profileDown, $profileUp, $oltIp, $oltPort, $oltSlot,$subscriberIdentity,$onuId);
			
					//return 1;
			
				}
				

				
				if ( trim(strpos($result,"successfully enabled")) ==  ""  ) {
					$log->add("ERROR CREANDO LA OLT2\n");
					$log->add("LOG RESULT:".$result);
					$this->deleteOnuIDDB($oltIp, $oltSlot, $oltPort,$onuId);
					
				// INICIO SETEO DE ZMS PARA GARANTIZAR PRIORIDAD
				$command="update system zmsexists = true zmsipaddress = 172.20.4.60 0";
				$telnet->DoCommand($command,$result);
				$log->add("result: ".$result."\n");
				
				$command="configsync initiate partial";
				$telnet->DoCommand($command,$result);
				$log->add("result: ".$result."\n");
				// FIN SETEO DE ZMS PARA GARANTIZAR PRIORIDAD
					
					$telnet->Disconnect();
					
					return 1;  
				}
				$log->add("LOG RESULT:".$result);	

                $command="bridge add 1-$oltSlot-$oltPort-$onuId/gpononu gtp $profileDown downlink vlan $vlan tagged epktrule $profileUp g-vlan 200 rg";
				$log->add("LOG COMMAND: $command\n");
				$telnet->DoCommand($command,$result);

				if ( $set2default ) {
					$command="onu set2default $oltSlot/$oltPort/$onuId";
					$log->add("LOG COMMAND: $command\n");
                                	$telnet->DoCommand($command,$result);

					$command="yes";
                                	$log->add("LOG COMMAND: $command\n");
                                	$telnet->DoCommand($command,$result);

					$command="no";
                                	$log->add("LOG COMMAND: $command\n");
                                	$telnet->DoCommand($command,$result);

					$command="yes";
                                	$log->add("LOG COMMAND: $command\n");
                                	$telnet->DoCommand($command,$result);
				}
				
				
				// INICIO SETEO DE ZMS PARA GARANTIZAR PRIORIDAD
				$command="update system zmsexists = true zmsipaddress = 172.20.4.60 0";
				$telnet->DoCommand($command,$result);
				$log->add("result: ".$result."\n");
				
				$command="configsync initiate partial";
				$telnet->DoCommand($command,$result);
				$log->add("result: ".$result."\n");
				// FIN SETEO DE ZMS PARA GARANTIZAR PRIORIDAD
				
				
				
				$telnet->Disconnect();
				
				/*AGREGAR ACTUALIZACION EN DB del ONU ID.*/
				$query="UPDATE ".$this->DB.".subscriber_info set olt_ip='$oltIp', olt_slot='$oltSlot', olt_port='$oltPort', olt_onuid='$onuId' where subscriber_identity='$subscriberIdentity'";
				$log->add("QUERY: $query\n");
				$res= mysqli_query($this->connection, $query);
				
				
			} else {
				$log->add("LOG TELNET Authentication Fail\n");
				return -1;
			}
			$log->add("setProvisioned\n");
			$this->setProvisioned($subscriberIdentity,$oltIp,$oltSlot,$oltPort,$onuId);

			
		//} else {

			/*$log->add("LOG SSH Authentication Successful!\n");
		
			$ssh->enablePTY();
			
			
			 // INICIO DES-SETEO DE ZMS PARA GARANTIZAR PRIORIDAD
			$ssh->read('#');
			$command="update system zmsexists = false zmsipaddress = 0.0.0.0 0";
			$ssh->write($command);
			
			$ssh->read('#');
			$command="resetcliprov all";
			$ssh->write($command);
			
			$ssh->read('#');
			$command="setline 0";
			$ssh->write($command);
			// FIN DES-SETEO DE ZMS PARA GARANTIZAR PRIORIDAD
			
			
			
			/*$ssh->read('#');
			$command="onu set $oltSlot/$oltPort/$onuId vendorid ZNTS serno fsan $fsan meprof zhone-2426\n";
			$ssh->write($command);
		
			$ssh->read('#');
			$command="cpe eth add $oltSlot/$oltPort/$onuId/1 admin-state up\n";
			$ssh->write($command);
	
			$ssh->read('#');
                	$command="cpe eth add $oltSlot/$oltPort/$onuId/2 admin-state up\n";
                	$ssh->write($command);

			$ssh->read('#');
                	$command="cpe eth add $oltSlot/$oltPort/$onuId/3 admin-state up\n";
                	$ssh->write($command);

			$ssh->read('#');
                	$command="cpe eth add $oltSlot/$oltPort/$onuId/4 admin-state up\n";
                	$ssh->write($command);
	
		//	$ssh->read('#');
			//$GEMPORT=1000+$onuId;
                	$command="bridge add 1-$oltSlot-$oltPort-$onuId/gpononu gtp $profileDown downlink vlan $vlan tagged epktrule $profileUp g-vlan 200 rg\n";
                	$ssh->write($command);
	
			if ( $set2default ) {
				$ssh->read('#');
				$command="onu set2default $oltSlot/$oltPort/$onuId";
				$ssh->write($command);

				$ssh->read(']:');
                        	$command="yes";
                        	$ssh->write($command);

				$ssh->read(']:');
                        	$command="no";
                        	$ssh->write($command);

				$ssh->read(']:');
                        	$command="yes";
                        	$ssh->write($command);

			}
			
			
			// INICIO DES-SETEO DE ZMS PARA GARANTIZAR PRIORIDAD
			$ssh->read('#');
			$command="update system zmsexists = true zmsipaddress = 172.20.4.60 0";
			$ssh->write($command);
			
			$ssh->read('#');
			$command="configsync initiate partial";
			$ssh->write($command);
			
			// FIN DES-SETEO DE ZMS PARA GARANTIZAR PRIORIDAD
			
			
			
			$ssh->read('#');
			$command="logout";
			$ssh->write($command);
			$this->setProvisioned($subscriberIdentity,$oltIp,$oltSlot,$oltPort,$onuId);
		}*/
		return 1;	
	}

	public function deleteOnuID($oltIp,$oltSlot, $oltPort,$oltOnuId) {
		global $SSH_USER,$SSH_PASS;

                set_include_path('Net/');
		
		$log=new Log();

		if (trim($oltIp) == "" ) return 1;
		// include('Net/SSH2.php');
                // include('telnet.class.php');
                
		$onuId=$oltOnuId;
                $ssh = new Net_SSH2($oltIp);
                if (!$ssh->login($this->SSH_USER, $this->SSH_PASS)) {
                        $telnet = new PHPTelnet();
                        $result = $telnet->Connect($oltIp,$this->SSH_USER,$this->SSH_PASS);
                        if ($result == 0) {
                                // LOG TELNET Authentication Successful
                                
                                	// INICIO DES-SETEO DE ZMS PARA GARANTIZAR PRIORIDAD
									$command="update system zmsexists = false zmsipaddress = 0.0.0.0 0";
									$telnet->DoCommand($command,$result);
									$log->add("result: ".$result."\n");
				
									$command="resetcliprov all";
									$telnet->DoCommand($command,$result);
									$log->add("result: ".$result."\n");
				
									$command="setline 0";
									$telnet->DoCommand($command,$result);
									$log->add("result: ".$result."\n");
									// FIN DES-SETEO DE ZMS PARA GARANTIZAR PRIORIDAD

                              		$command="onu delete $oltSlot/$oltPort/$onuId ";
                                	$telnet->DoCommand($command,$result);
									$command="yes";
									$telnet->DoCommand($command,$result);
									$command="no";
                                	$telnet->DoCommand($command,$result);
									$command="yes";
                                	$telnet->DoCommand($command,$result);
                                	
                                	
                                	// INICIO SETEO DE ZMS PARA GARANTIZAR PRIORIDAD
									$command="update system zmsexists = true zmsipaddress = 172.20.4.60 0";
									$telnet->DoCommand($command,$result);
									$log->add("result: ".$result."\n");
				
									$command="configsync initiate partial";
									$telnet->DoCommand($command,$result);
									$log->add("result: ".$result."\n");
									// FIN SETEO DE ZMS PARA GARANTIZAR PRIORIDAD
                                	

				//$GEMPORT=1000+$onuId;
				//$command="bridge delete 1-$oltSlot-$oltPort-$GEMPORT-gponport-200/bridge ";
				//$telnet->DoCommand($command,$result);
	
				$telnet->Disconnect();
			} else {
				// LOG TELNET Authentication Fail
				return -1;
			}
		} else {
                        // LOG  "Authentication Successful!\n";

                        $ssh->enablePTY();
                        
             // INICIO DES-SETEO DE ZMS PARA GARANTIZAR PRIORIDAD
			$ssh->read('#');
			$command="update system zmsexists = false zmsipaddress = 0.0.0.0 0";
			$ssh->write($command);
			
			$ssh->read('#');
			$command="resetcliprov all";
			$ssh->write($command);
			
			$ssh->read('#');
			$command="setline 0";
			$ssh->write($command);
			// FIN DES-SETEO DE ZMS PARA GARANTIZAR PRIORIDAD
                        
                        
                        $ssh->read('#');
                        $command="onu delete $oltSlot/$oltPort/$onuId\n";
                        $ssh->write($command);
			$ssh->read('no]:');
                        $command="yes\n";
			$ssh->read('no]:');
                        $command="no\n";
			$ssh->read('no]:');
                        $command="yes\n";
			
			//$GEMPORT=1000+$onuId;
			 //$command="bridge  delete 1-$oltSlot-$oltPort-$GEMPORT-gponport-200/bridge\n";
			
			// INICIO DES-SETEO DE ZMS PARA GARANTIZAR PRIORIDAD
			$ssh->read('#');
			$command="update system zmsexists = true zmsipaddress = 172.20.4.60 0";
			$ssh->write($command);
			
			$ssh->read('#');
			$command="configsync initiate partial";
			$ssh->write($command);
			
			// FIN DES-SETEO DE ZMS PARA GARANTIZAR PRIORIDAD
			
			$ssh->read('#');
			$command="logout\n";
                        
			$ssh->write($command);
			

		}

		return 1;

	
	}	
	private function getOnuID($oltIp, $oltSlot, $oltPort) {

		$query="SELECT max(onu)+1 as id from ".$this->DB.".olt where ip='$oltIp' and slot='$oltSlot' and port ='$oltPort'";
                $res= mysqli_query($this->connection, $query);
                $ONU= mysqli_fetch_assoc($res);

		if ( trim($ONU['id']) == "" ) $ONU['id']=1;
		
		$query="insert into ".$this->DB.".olt (ip,slot,port,onu) values ('$oltIp','$oltSlot','$oltPort','".$ONU['id']."')";
		$res= mysqli_query($this->connection, $query);

                return $ONU['id'];

	}

	private function deleteOnuIDDB($oltIp, $oltSlot, $oltPort,$onuId) {
		$query="DELETE FROM ".$this->DB.".olt where ip='$oltIp' and slot='$oltSlot' and port='$oltPort' and onu='$onuId'";
		$res= mysqli_query($this->connection, $query);
		return 1;
	}

	private function setProvisioned($subscriberIdentity,$oltIp,$oltSlot,$oltPort,$onuId) {

		$log=new Log();
		$log->add("setProvisioned: $subscriberIdentity,$oltIp,$oltSlot,$oltPort,$onuId\n");
		$query="UPDATE ".$this->DB.".subscriber set provisioned=NOW() where subscriber_identity='$subscriberIdentity'";
		$log->add("QUERY: $query\n");
		$res= mysqli_query($this->connection, $query);

		$query="UPDATE ".$this->DB.".trap_log set provisioned=NOW() where subscriber_identity='$subscriberIdentity'";
		$log->add("QUERY: $query\n");
		$res= mysqli_query($this->connection, $query);

		$query="UPDATE ".$this->DB.".subscriber_info set olt_ip='$oltIp', olt_slot='$oltSlot', olt_port='$oltPort', olt_onuid='$onuId' where subscriber_identity='$subscriberIdentity'";
		$log->add("QUERY: $query\n");
		$res= mysqli_query($this->connection, $query);
		return 1;
	}

	private function trapLogger($oltIp,$fsan,$subscriberIdentity,$slot,$port){

		$query="INSERT INTO ".$this->DB.".trap_log (create_date,olt_ip,fsan,subscriber_identity,port,slot,state) value (NOW(),'$oltIp','$fsan','$subscriberIdentity','$slot','$port','0')";
		$res= mysqli_query($this->connection, $query);
                return 1;
	}


}
?>
