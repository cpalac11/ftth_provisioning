<?php
/* Class Provisioning
 *
 *	Contains all internal provisioning methods
 *
 *	Updates
 * \Authors 
 * 		Fernando Munevar fmunevar@directvla.com.co
 * 		Christian Palacios cpalac11@directvla.com.co
 * 
 * Se modifica clase, solo esta el get_params unicamente con tecnologia FTTH.
 * 
 *
*/
//ini_set('error_reporting', E_ALL|E_STRICT);
//ini_set('display_errors', 1);
require_once("connection.class.php");
require_once("proxy.class.php");
require_once("conciliation.class.php");

class Provisioning extends Model{

	public function can_create($subscriberIdentity, $profile, $technology)	{
		$technology = 'FTTH';
		if ($technology == 'FTTH') {
			return 1;
		}
		$tst = new Troubleshooting();
		$response = $tst->get_params($subscriberIdentity, $technology);
		if ($response->faultstring) {
			$faultcode = "5013";
			$faultmessage = "El CPE esta desconectado o no puede alcanzarse";
			return new soap_fault($faultcode, '', $faultmessage);
		}		
	}

	/** \fn create */
	public function create($subscriberIdentity, $profile, $force_provisioning, $technology){
		$technology = 'FTTH';
		$latitude = "";
		$longitude = "";
		global $ERROR_MYSQL;
		//print_r($ERROR_MYSQL); die();
		//set all the parameters to upper case
		$subscriberIdentity = mb_strtoupper($subscriberIdentity);
		$profile = mb_strtoupper($profile);
		$force_provisioning = mb_strtoupper($force_provisioning);
		if ($ERROR_MYSQL == -3) {
			// return new soap_fault('8', 'Internal error', 'DB ERROR');
			throw new Exception(_('DB ERROR.'), 8);
		}
		//Validate Technology
		if (!$this->validateTechnology($technology)) {
			// return new soap_fault('5004', 'Technology', 'The specified technology is currently unsupported');
			throw new Exception(_('The specified technology is currently unsupported.'), 3003);
		}
		//Validate Subscriber Format
		$validateSubscriber = $this->validateSubscriber($subscriberIdentity, $technology);
		if ($validateSubscriber == -1){
			// No tiene el formato correcto la FSAN');
			throw new Exception(_('Invalid subscriber identifier format.'), 3002);
		}
		//Validate stock
		else if ($validateSubscriber == -3) {
			//El CPE no se encuentra aprovisionado.
			throw new Exception(_('CPE is not provisioned.'), 3001);
		}
		//Validate subscriber Exists
		if ($this->subscriberExist($subscriberIdentity, $technology)) {
			// El CPE ya esta aprovisionado y con acceso a Internet
			throw new Exception(_('CPE is already provisioned with internet access.'), 3000);
		}
		//Validate profile
		if (!$this->validateProfile($profile)) {
			// No encuentra el perfil entregado
			throw new Exception(_('The given profile is not found.'), 3004);
		}
		//Validate compatibility
		if (!$this->validateCompatibility($technology, $profile)) {
			// El perfil ingresado no es compatible con la tecnología indicada.
			throw new Exception(_('Profile is not compatible with given technology.'), 3005);
		}
		// SUBSCRIBER CREATE
		$sql  = "INSERT INTO " . $this->DB . ".subscriber(	";
		$sql .= "subscriber_identity,	";
		$sql .= "profile,				";
		$sql .= "force_provisioning,	";
		$sql .= "latitude,				";
		$sql .= "longitude,				";
		$sql .= "technology,			";
		$sql .= "state)					";

		$sql .= " values ('" . $subscriberIdentity .	"'";
		$sql .= ",'" . $profile .						"'";
		$sql .= ",'" . $force_provisioning .			"'";
		$sql .= ",'" . $latitude .						"'";
		$sql .= ",'" . $longitude .					"'";
		$sql .= ",'" . $technology .					"'";
		$sql .= ",'1')";
		$res = mysqli_query($this->connection, $sql) or $this->soap_fault_return(-3);

		//CREATE SUBSCRIBER INFO
		$sql  = "INSERT INTO " . $this->DB . ".subscriber_info(	";
		$sql .= "subscriber_identity,		";
		$sql .= "ip_address,				";
		$sql .= "brand,						";
		$sql .= "model,						";
		$sql .= "rsrp,						";
		$sql .= "rssi,						";
		$sql .= "cinr,						";
		$sql .= "tx_power,					";
		$sql .= "ping_rtt,					";
		$sql .= "firmware_version,			";
		$sql .= "bsid,						";
		$sql .= "technology,				";
		$sql .= "upload_speed,				";
		$sql .= "download_speed)			";

		$sql .= " values ('" . $subscriberIdentity .	"'";
		$sql .= ",''					  ";
		$sql .= ",''								  ";
		$sql .= ",''							  ";
		$sql .= ",''							  ";
		$sql .= ",'0'								  ";
		$sql .= ",''								  ";
		$sql .= ",''								  ";
		$sql .= ",''								  ";
		$sql .= ",''						  ";
		$sql .= ",''						  ";
		$sql .= ",'" . $technology . "'				 	  ";
		$sql .= ",'2048'							  ";
		$sql .= ",'1024')							  ";
		$res = mysqli_query($this->connection, $sql) or $this->soap_fault_return(-3);
		$sql = "INSERT INTO " . $this->DB . ".log (subscriber_identity,request,response,fecha,ip)
		VALUES ('$subscriberIdentity','Provisioning.create','true',NOW(),'" . $_SERVER['REMOTE_ADDR'] . "')";
		$res = mysqli_query($this->connection, $sql) or $this->soap_fault_return(-3);
		$conciliation = new Conciliation;
		$conciliation->create($subscriberIdentity, $technology, $profile);
		$result['return'] = 1;
		return $result;		
	}

	public function delete($subs_id, $technology){
		openlog('Provisioning_ftth', LOG_ODELAY, LOG_LOCAL7);
		$connection_array = $this->check_interface_integrity($subs_id);
		if ($connection_array){
			$slot = $connection_array['olt_slot'];
			$port = $connection_array['olt_port'];
			$onu_id = $connection_array['olt_onuid'];
			$olt_ip = $connection_array['olt_ip'];			
			syslog(LOG_INFO, "Delete: valid interface founded for $subs_id");			
			include('olt_connection.class.php');
			$olt = new olt_connection($olt_ip, $slot, $port, $subs_id);
			######## Si conecta a la OLT ##########		
			if ($olt->connect()){
				syslog(LOG_INFO, "Connected to OLT! -> OK for $subs_id");
				$interface = $slot . '/' . $port . '/' . $onu_id;
				########### Si borra la FSAN de la OLT ##########
				if ($olt->delete($interface)) {
					$conn = new db_connection();
					########## Si borra la FSAN de subscriber, subscriber_info y subs_ftth ##################
					if ($conn->update_deleted($subs_id, $profile)) {
						syslog(LOG_INFO, "delete: db deleted for $subs_id at $interface");
					} 
					else {
						syslog(LOG_INFO, "delete: db not deleted for $subs_id");
						throw new Exception(_('There was a problem deleting from database. Manual delete should be done.'), 3006);
					}
					$log = $conn->add_log($subs_id, "Provisioning.delete", "true");
					closelog();
					return true;
				}
				else{
					syslog(LOG_INFO, "delete: error deleting ONU from OLT... closing for fsan $subs_id");
					$conn = new db_connection();
					$log = $conn->add_log($subs_id, "Provisioning.delete", "false : olt delete");
					closelog();
					throw new Exception(_('Error deleting ONU from OLT.'), 3007);					
					return false;
				}
			}
			############# No conecta a la OLT ###############
			else {
				syslog(LOG_INFO, "could not connect to olt $olt_ip");
				syslog(LOG_INFO, "$olt->error");
				$conn = new db_connection();
				$log = $conn->add_log($subs_id, "Provisioning.delete", "false: could not connect to olt $olt_ip");
				closelog();
				throw new Exception(_('Error connecting to OLT: Telnet loggin problem'), 3008);
				return false;
			}
		}
		########### Si no hay conexión, agrega el log y retorna falso ###############
		else {
			syslog(LOG_INFO, "delete: not valid interface for $subs_id\n");
			$conn = new db_connection();
			$log = $conn->add_log($subs_id, "Provisioning.delete", "false: not valid interface for $subs_id");
			closelog();			
			return false;
		}
	}

	public function read($subscriberIdentity, $technology){
		$technology = 'FTTH';
		//set all the parameters to upper case
		$subscriberIdentity = mb_strtoupper($subscriberIdentity);
		$technology = mb_strtoupper($technology);
		//Validate Technology
		if (!$this->validateTechnology($technology)) {
			return new soap_fault('5004', 'Technology', 'The specified technology is currently unsupported');
		}
		//Validate Subscriber Format
		$validateSubscriber = $this->validateSubscriber($subscriberIdentity, $technology);
		if ($validateSubscriber == -1){
			return new soap_fault('5000', 'Subscriber identifier', 'Invalid subscriber identifier format');
		}
		//Validate stock
		else if ($validateSubscriber == -3){
			return new soap_fault('5010', 'Subscriber identifier', 'El CPE no se encuentra aprovisionado');
		}
		//Validate subscriber Exists
		$sql = "INSERT INTO " . $this->DB . ".log (subscriber_identity,request,response,fecha,ip) VALUES ('$subscriberIdentity','Provisioning.read','true',NOW(),'" . $_SERVER['REMOTE_ADDR'] . "')";
		$res = mysqli_query($this->connection, $sql) or $this->soap_fault_return(-3);
		//INFO DEL SUBSCRIBER
		$sql  = "SELECT * FROM " . $this->DB . ".subscriber_info WHERE subscriber_identity='" . $subscriberIdentity . "'";
		$res = mysqli_query($this->connection, $sql) or $this->soap_fault_return(-3);
		$result = mysqli_fetch_array($res);
		$result['profile'] = $result['download_speed'];
		return $result;
	}

	private function check_interface_integrity($subs_id){
		include('mysql_connection.class.php');
		$provisioning_config = @parse_ini_file('provisioning_config.ini', true);
		$GLOBALS['provisioning_config'] = $provisioning_config;
		$conn = new db_connection();
		$connection_array = $conn->get_connection_info($subs_id);
		//print_r($connection_array);die();
		if ($connection_array['olt_ip']){
			$slot = $connection_array['olt_slot'];
			$port = $connection_array['olt_port'];
			$onu_id = $connection_array['olt_onuid'];
			$olt_ip = $connection_array['olt_ip'];
			$index_oid = ".1.3.6.1.4.1.5504.3.5.7.1.6.1.$slot.$port.$onu_id.1.25";
			syslog(LOG_INFO, "update: integrity: index is $index_oid for $subs_id");
			$snmp_arr = explode(" ", snmp2_get($olt_ip, 'DTVNET-SNMP', $index_oid));

			if(strlen($snmp_arr[1]) == 10){
				$snmp_index = $snmp_arr[1];
				syslog(LOG_INFO, "update: en uno $snmp_index for $subs_id");
			}
			elseif (strlen($snmp_arr[0]) == 10){
				$snmp_index = $snmp_arr[0];
				syslog(LOG_INFO, "update: en cero $snmp_index for $subs_id");
			}
			else{
				syslog(LOG_INFO, "update: integrity: not index founded for $subs_id");
				throw new Exception(_('Not index found for given FSAN.'), 3009);
				return false;
			}
			syslog(LOG_INFO, "update: integrity: hex oid index  $snmp_index for $subs_id");
			$sub_fsan = snmp2_get($olt_ip, 'DTVNET-SNMP', "1.3.6.1.4.1.5504.5.14.1.2.1.17.$snmp_index");
			$sub_fsan = str_replace(' ', '', $sub_fsan);
			$sub_fsan = str_replace('Hex-STRING:', '', $sub_fsan);
			$sub_fsan = str_replace('"', '', $sub_fsan);
			syslog(LOG_INFO, "update: hex string to converto is  $sub_fsan for $subs_id");
			$str = '';
			for ($i = 0; $i < (strlen($sub_fsan) - 2); $i += 2) $str .= chr(hexdec(substr($sub_fsan, $i, 2)));
			$fsan = 'ZNTS' . $str;
			syslog(LOG_INFO, "update: integrity: fsan to compare is $fsan for $subs_id");
			if ($subs_id == $fsan) {
				return $connection_array;
			} else {
				syslog(LOG_INFO, "ONU not found in given location from database for $subs_id");
				throw new Exception(_('ONU not found in given location from database.'), 3010);
				return false;
			}
		}
		else {
			syslog(LOG_INFO, "update: no interface/ip founded for $subs_id\n");
			throw new Exception(_('OLT IP is not registered in DB.'), 3011);
			return false;
		}
	}

	public function update($subs_id, $profile, $technology){
		$technology = 'FTTH';
		openlog('Provisioning_ftth', LOG_ODELAY, LOG_LOCAL7);
		$connection_array = $this->check_interface_integrity($subs_id);
		if ($connection_array){
			$slot = $connection_array['olt_slot'];
			$port = $connection_array['olt_port'];
			$onu_id = $connection_array['olt_onuid'];
			$olt_ip = $connection_array['olt_ip'];
			syslog(LOG_INFO, "update: valid interface founded for $subs_id");
			include('olt_connection.class.php');
			if ($olt = new olt_connection($olt_ip, $slot, $port, $subs_id)){
				$interface = '1-' . $slot . '-' . $port . '-' . $onu_id;
				if ($olt->update_profile($interface, $profile)){
					$conn = new db_connection();
					if ($conn->update_profile($subs_id, $profile)){
						syslog(LOG_INFO, "update: profile updated for $subs_id");
					}
					else{
						syslog(LOG_INFO, "update: profile updated NOK for $subs_id");
					}
					$log = $conn->add_log($subs_id, "Provisioning.update", "true");
					closelog();
					return true;
				} 
				else {
					$conn = new db_connection();
					$log = $conn->add_log($subs_id, "Provisioning.update", "false: olt update_profile");
					return false;
					closelog();
				}
			} 
			else {
				syslog(LOG_INFO, "could not connect to olt $olt_ip");
				syslog(LOG_INFO, "$olt->error");
				$conn = new db_connection();
				$log = $conn->add_log($subs_id, "Provisioning.update", "false: could not connect to olt $olt_ip");
				return false;
				closelog();
			}
		} 
		else {
			syslog(LOG_INFO, "update: not valid interface for $subs_id\n");
			$conn = new db_connection();
			$log = $conn->add_log($subs_id, "Provisioning.update", "false: not valid interface for $subs_id");
			closelog();
			return false;
		}
	}	

	public function suspend($subscriberIdentity, $technology){
		//set all the parameters to upper case
		$subscriberIdentity = mb_strtoupper($subscriberIdentity);
		$technology = mb_strtoupper($technology);

		//Validate Technology
		if (!$this->validateTechnology($technology)){
			return new soap_fault('5004', 'Technology', 'The specified technology is currently unsupported');
		}
		//Validate Subscriber Format
		$validateSubscriber = $this->validateSubscriber($subscriberIdentity, $technology);
		if ($validateSubscriber == -1){
			return new soap_fault('5000', 'Subscriber identifier', 'Invalid subscriber identifier format');
		}
		//Validate stock
		else if ($validateSubscriber == -3){
			return new soap_fault('5010', 'Subscriber identifier', 'El CPE no se encuentra aprovisionado');
		}
		//Validate subscriber Exists
		if (!$this->subscriberExist($subscriberIdentity, $technology)){
			return new soap_fault('5010', 'Subscriber identifier', 'El CPE no se encuentra aprovisionado');
		}
		//Validate state
		$state = $this->subscriberState($subscriberIdentity, $technology, false);
		if ($state['state'] == '0'){
			return new soap_fault('5007', 'State', 'The susbscriber is already suspended.');
		}

		//ACTUALIZO EL SUBSCRIBER
		$sql  = "UPDATE " . $this->DB . ".subscriber SET	 				";
		$sql .= "state= '0'				  				  				";
		$sql .= "WHERE subscriber_identity = '" . $subscriberIdentity . "' ";
		$sql .= "AND technology = '" . $technology .					 "' ";

		$res = mysqli_query($this->connection, $sql) or $this->soap_fault_return(-3);
		$result['return'] = 1;

		$sql = "INSERT INTO " . $this->DB . ".log (subscriber_identity,request,response,fecha,ip)
		VALUES ('$subscriberIdentity','Provisioning.suspend','true',NOW(),'" . $_SERVER['REMOTE_ADDR'] . "')";
		$res = mysqli_query($this->connection, $sql) or $this->soap_fault_return(-3);
		return $result;
	}

	public function unsuspend($subscriberIdentity, $technology){
		//set all the parameters to upper case
		$subscriberIdentity = mb_strtoupper($subscriberIdentity);
		$technology = mb_strtoupper($technology);
		//Validate Technology
		if (!$this->validateTechnology($technology)) {
			return new soap_fault('5004', 'Technology', 'The specified technology is currently unsupported');
		}
		//Validate Subscriber Format
		$validateSubscriber = $this->validateSubscriber($subscriberIdentity, $technology);
		if ($validateSubscriber == -1) {
			return new soap_fault('5000', 'Subscriber identifier', 'Invalid subscriber identifier format');
		}
		//Validate stock
		else if ($validateSubscriber == -3) {
			return new soap_fault('5010', 'Subscriber identifier', 'El CPE no se encuentra aprovisionado');
		}
		//Validate subscriber Exists
		if (!$this->subscriberExist($subscriberIdentity, $technology)) {
			return new soap_fault('5010', 'Subscriber identifier', 'El CPE no se encuentra aprovisionado');
		}
		//Validate state
		$state = $this->subscriberState($subscriberIdentity, $technology, false);
		if ($state['state'] == '1') {
			return new soap_fault('5008', 'State', 'The susbscriber is not suspended.');
		}
		//ACTUALIZO EL SUBSCRIBER
		$sql  = "UPDATE " . $this->DB . ".subscriber SET	 				";
		$sql .= "state= '1'				  				  				";
		$sql .= "WHERE subscriber_identity = '" . $subscriberIdentity . "' ";
		$sql .= "AND technology = '" . $technology .					 "' ";

		$res = mysqli_query($this->connection, $sql) or $this->soap_fault_return(-3);
		$sql = "INSERT INTO " . $this->DB . ".log (subscriber_identity,request,response,fecha,ip)
		VALUES ('$subscriberIdentity','Provisioning.unsuspend','true',NOW(),'" . $_SERVER['REMOTE_ADDR'] . "')";
			
		$res = mysqli_query($this->connection, $sql) or $this->soap_fault_return(-3);
		$result['return'] = 1;
		return $result;
	}
}
?>