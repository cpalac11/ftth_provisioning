<?php
/*! \file proxy.class.php
 *  \class Proxy 
 *
 *	Contains all internal provisioning methods
 */


class Proxy extends Model {


        private function macConvert($mac,$separator) {
        	$newmac="";
        	for ($i=0; $i < strlen($mac); $i++ ) {
                	if ($i > 0 and $i+2 < strlen($mac)) {
                        	$i++;
                        	$newmac=$newmac.$separator.substr($mac,$i,2);
                	} else if ($i > 0) { $newmac=$newmac;
                	} else $newmac=substr($mac,$i,2);

        	}
        	return $newmac;

	}

	private function httpGet($url) {
    		$ch = curl_init();  
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    		curl_setopt($ch,CURLOPT_URL,$url);
    		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    		$output=curl_exec($ch);
    		curl_close($ch);
    		return $output;
	}

	public function generic_method($method,$subscriberIdentity) {
		global $LTE_WSDL;
                $client=new nusoap_client($LTE_WSDL,'wsdl');
                $param=array('subscriber_identity' => $subscriberIdentity);
                $response = $client->call($method, $param);
                if ($client->fault) {
                        $params['faultcode']=$response['faultcode'];
                        $params['faultmessage']=$response['faultstring'];
                        $params['return']=-1;
                        return $params;
                } else {
                        $error = $client->getError();
                        if ($error) {
                                $params['error']=$error;
                                $params['return']=-3;
                                return $params;
                        }
                }
                $params['return']=1;
                return $params;
	}


	public function update($subscriberIdentity,$profile) {
                global $LTE_WSDL;
                $client=new nusoap_client($LTE_WSDL,'wsdl');
                $param=array('subscriber_identity' => $subscriberIdentity, 'profile' => $profile);
                $response = $client->call('Provisioning.update', $param);
                if ($client->fault) {
                        $params['faultcode']=$response['faultcode'];
                        $params['faultmessage']=$response['faultstring'];
                        $params['return']=-1;
                        return $params;
                } else {
                        $error = $client->getError();
                        if ($error) {
                                $params['error']=$error;
                                $params['return']=-3;
                                return $params;
                        }
                }
                $params['return']=1;
                return $params;
        }	


	public function reset_connection($subscriberIdentity) {
		global $LTE_WSDL;
                $client=new nusoap_client($LTE_WSDL,'wsdl');
                $param=array('subscriber_identity' => $subscriberIdentity);
                $response = $client->call('Provisioning.reset_connection', $param);
                if ($client->fault) {
                        $params['faultcode']=$response['faultcode'];
                        $params['faultmessage']=$response['faultstring'];
                        $params['return']=-1;
                        return $params;
                } else {
                        $error = $client->getError();
                        if ($error) {
                                $params['error']=$error;
                                $params['return']=-3;
                                return $params;
                        }
                }
                $params['return']=1;
                return $params;
	}


	public function upload_firmware($subscriberIdentity,$firmware_name) {
		global $LTE_WSDL;
                $client=new nusoap_client($LTE_WSDL,'wsdl');
                $param=array('subscriber_identity' => $subscriberIdentity, 'firmware_name' => $firmware_name);
                $response = $client->call('Provisioning.upload_firmware', $param);
                if ($client->fault) {
                        $params['faultcode']=$response['faultcode'];
                        $params['faultmessage']=$response['faultstring'];
                        $params['return']=-1;
                        return $params;
                } else {
                        $error = $client->getError();
                        if ($error) {
                                $params['error']=$error;
                                $params['return']=-3;
                                return $params;
                        }
                }
                $params['return']=1;
                return $params;
	}

	public function can_create($subscriberIdentity,$profile) {
		global $LTE_WSDL,$WIMAX_WSDL,$RSSI,$CINR;
		if(strlen($subscriberIdentity) == 15) {
			$client=new nusoap_client($LTE_WSDL,'wsdl');
			$param=array('subscriber_identity' => $subscriberIdentity, 'profile' => $profile);
			$response = $client->call('Provisioning.can_create', $param);			
			if ($client->fault) {
				$params['faultcode']=$response['faultcode'];
				$params['faultmessage']=$response['faultstring'];
				$params['return']=-1;
				return $params;
			} else {
				$error = $client->getError();
				if ($error) {
					$params['error']=$error;
					$params['return']=-3;
					return $params;
				}
			}
			$params['return']=1;
			return $params;
		} else {
			$client=new nusoap_client($WIMAX_WSDL,'wsdl');
			$mac=$this->macConvert($subscriberIdentity,':');
                        $param=array('macAddr' => $mac);
			$response = $client->call('CPE_getParams', $param);
                        if ($client->fault) {
                                $params['faultcode']=$response['faultcode'];
                                $params['faultmessage']=$response['faultstring'];
                                $params['return']=-1;
                                return $params;
                        } else {
                                $error = $client->getError();
                                if ($error) {
                                        $params['error']=$error;
                                        $params['return']=-3;
                                        return $params;
                                }
                        }
			list($v1,$v2,$v3,$RSSI_WS,$CINR_WS) = split ('\|',$response);
			if ( $RSSI_WS >= $RSSI and $CINR_WS >= $CINR ) {	
                        	$params['return']=1;
			} else {
				if ($RSSI_WS < $RSSI) {
				$params['faultcode']="5011";
                                $params['faultmessage']="El nivel de señal RSRP mínimo (-105dB) no es alcanzado. El actual es -25dB. | Comuníquense con el 0800-NET.";
                                $params['return']=-1;

				}
				if ($CINR_WS < $CINR) {
				$params['faultcode']="5010";
				$params['faultmessage']="El CPE no se encuentra conectado | Verifique la correcta instalación a la red eléctrica.";
				$params['return']=-1;
				}
			}
                        return $params;
		}
        }
	
	/** \fn create */



	public function create($subscriberIdentity,$profile,$force_provisioning, $lat,$lon) {
                global $LTE_WSDL, $WIMAX_WSDL, $WIMAX_GET;
                if(strlen($subscriberIdentity) == 15) {
                            $client=new nusoap_client($LTE_WSDL,'wsdl');
                            $param=array('subscriber_identity' => $subscriberIdentity, 'profile' => $profile,'force_provisioning'=>$force_provisioning,$latitude=>$lat,$longitude=>$lon);
                            $response = $client->call('Provisioning.create', $param);
                            if ($client->fault) {
                                    $params['faultcode']=$response['faultcode'];
                                    $params['faultmessage']=$response['faultstring'];
                                    $params['return']=-1;
                                    return $params;
                            } else {
                                    $error = $client->getError();
                                    if ($error) {
                                        $params['error']=$error;
                                        $params['return']=-3;
                                        return $params;
                                   }
                            } 
                 } else {
			    $mac=$this->macConvert($subscriberIdentity,'-');
                            $URL=$WIMAX_GET."&command=addUser&PrivateUserIdentity=".$mac."&TerminalMacId=".$mac."&TerminalType=10";
			    $resp=$this->httpGet($URL);
			    list ($numero,$mensaje) = split('&', $resp);
		            list($variable,$numero) = split('=',$numero);
                            list($variable,$mensaje) = split('=',$mensaje);
			    $mensaje=rawurldecode($mensaje);
			     if ( $numero != "0" ) {
                                $mensaje=str_replace("+", " ", $mensaje);
				$params['faultcode']=$numero;
                                $params['faultmessage']=$mensaje;
                                $params['return']=-1;
                                return $params;
			     } else {
				$URL2=$WIMAX_GET."&command=abortSession&PrivateUserIdentity=".$mac."&TerminalMacId=".$mac;
				$resp2=$this->httpGet($URL2);
				$params['return']=1;
                                return $params;
                             }

                 }
                 $params['return']=1;
                 return $params;

	}
	
	public function delete($subscriberIdentity) {
		global $LTE_WSDL, $WIMAX_GET;
		if(strlen($subscriberIdentity) == 15) {
                	$client=new nusoap_client($LTE_WSDL,'wsdl');
                	$param=array('subscriber_identity' => $subscriberIdentity);
                	$response = $client->call('Provisioning.delete', $param);
                	if ($client->fault) {
                        	$params['faultcode']=$response['faultcode'];
                        	$params['faultmessage']=$response['faultstring'];
                        	$params['return']=-1;
                        	return $params;
                	} else {
                        	$error = $client->getError();
                        	if ($error) {
                                	$params['error']=$error;
                                	$params['return']=-3;
                                	return $params;
                        	}
                	}
		} else {





                            $mac=$this->macConvert($subscriberIdentity,'-');
                            $URL=$WIMAX_GET."&command=deleteUser&PrivateUserIdentity=".$mac."&TerminalMacId=".$mac."&TerminalType=10";

			
			
		
			    $resp=$this->httpGet($URL);

			$URL2=$WIMAX_GET."&command=abortSession&PrivateUserIdentity=".$mac."&TerminalMacId=".$mac;
				$resp2=$this->httpGet($URL2);
				
                             list ($numero,$mensaje) = split('&', $resp);
                             list($variable,$numero) = split('=',$numero);
                             list($variable,$mensaje) = split('=',$mensaje);

			      

			     $mensaje=rawurldecode($mensaje);
                             if ( $numero != "0" ) {
                                $mensaje=str_replace("+", " ", $mensaje);
                                $params['faultcode']=$numero;
                                $params['faultmessage']=$mensaje;
                                $params['return']=-1;
                                return $params;
                             } else {
                                $params['return']=1;
                                return $params;
                             }

                }
                $params['return']=1;
                return $params;
	}

	public function get_available_firmwares($subscriberIdentity) {
		global $LTE_WSDL;
                $client=new nusoap_client($LTE_WSDL,'wsdl');
                $param=array('subscriber_identity' => $subscriberIdentity);
                $response = $client->call('Troubleshooting.get_available_firmwares', $param);
                if ($client->fault) {
                        $params['faultcode']=$response['faultcode'];
                        $params['faultmessage']=$response['faultstring'];
                        $params['return']=-1;
                        return $params;
                } else {
                        $error = $client->getError();
                        if ($error) {
                                $params['error']=$error;
                                $params['return']=-3;
                                return $params;
                        }
                }
                $params['return']=1;
                $params['firmware_version']=$response['firmware_version'];
                return $params;
	}

	public function get_params($subscriberIdentity,$technology) {
		$technology = mb_strtoupper($technology);

		if ($technology = "LTE/WIMAX") {return $this->gp_ltewimax($subscriberIdentity,$technology);}
		if ($technology = "BTB") {return 1;}
		if ($technology = "FTTH") {return 2;}
	}

	public function gp_ltewimax($subscriberIdentity,$technology) {
		global $LTE_WSDL,$WIMAX_WSDL;
		if(strlen($subscriberIdentity) == 15) {
                	$client=new nusoap_client($LTE_WSDL,'wsdl');
                	$param=array('subscriber_identity' => $subscriberIdentity);
                	$response = $client->call('Troubleshooting.get_params', $param);
                	if ($client->fault) {
                        	$params['faultcode']=$response['faultcode'];
                        	$params['faultmessage']=$response['faultstring'];
                        	$params['return']=-1;
                        	return $params;
                	} else {
                        	$error = $client->getError();
                        	if ($error) {
                                	$params['error']=$error;
                                	$params['return']=-3;
                                	return $params;
                        	}
                	}
                	$params['return']=1;
			$params['ip_address']=$response['ip_address'];
			$params['brand']=$response['brand'];
			$params['model']=$response['model'];
			$params['rsrp']=$response['rsrp'];
			$params['rssi']=$response['rssi'];
			$params['cinr']=$response['cinr'];
			$params['tx_power']=$response['tx_power'];
			$params['ping_rtt']=$response['ping_rtt'];
			$params['firmware_version']=$response['firmware_version'];
			$params['bsid']=$response['bsid'];
                	return $params;
		} else {





			$mac=$this->macConvert($subscriberIdentity,':');
			
			

			$client=new nusoap_client($WIMAX_WSDL,'wsdl');
			
			


                        $param=array('macAddr' => $mac);

			

			$response = $client->call('CPE_getParams', $param);


		

			if ($client->fault) {
                                $params['faultcode']=$response['faultcode'];
                                $params['faultmessage']=$response['faultstring'];
                                $params['return']=-1;
                                return $params;
                        } else {
                                $error = $client->getError();
                                if ($error) {
                                        $params['error']=$error;
                                        $params['return']=-3;
                                        return $params;
                                }
                        }
			list($model,$brand,$v3,$RSSI_WS,$CINR_WS,$v4,$v5,$ip_address) = split ('\|',$response);
                        $params['return']=1;
                        $params['ip_address']=$ip_address;
                        $params['brand']=$brand;
                        $params['model']=str_replace("\"","",$model);
                        $params['rsrp']="";
                        $params['rssi']=$RSSI_WS;
                        $params['cinr']=$CINR_WS;
                        $params['tx_power']="";
                        $params['ping_rtt']="";
                        $params['firmware_version']="";
                        $params['bsid']="";
                        return $params;
		}
		
	}//function

	public function run_speed_test($subscriberIdentity) {
                global $LTE_WSDL;
                $client=new nusoap_client($LTE_WSDL,'wsdl');
                $param=array('subscriber_identity' => $subscriberIdentity);
                $response = $client->call('Troubleshooting.run_speed_test', $param);
                if ($client->fault) {
                        $params['faultcode']=$response['faultcode'];
                        $params['faultmessage']=$response['faultstring'];
                        $params['return']=-1;
                        return $params;
                } else {
                        $error = $client->getError();
                        if ($error) {
                                $params['error']=$error;
                                $params['return']=-3;
                                return $params;
                        }
                }
		$params['download_speed']=$response['download_speed'];
                $params['return']=1;
                $params['upload_speed']=$response['upload_speed'];
                return $params;

        }

	public function read($subscriberIdentity) {
                global $LTE_WSDL;
                $client=new nusoap_client($LTE_WSDL,'wsdl');
                $param=array('subscriber_identity' => $subscriberIdentity);
                $response = $client->call('Provisioning.read', $param);
                if ($client->fault) {
                        $params['faultcode']=$response['faultcode'];
                        $params['faultmessage']=$response['faultstring'];
                        $params['return']=-1;
                        return $params;
                } else {
                        $error = $client->getError();
                        if ($error) {
                                $params['error']=$error;
                                $params['return']=-3;
                                return $params;
                        }
                }
                $params['return']=1;
                $params['ip_address']=$response['ip_address'];
                $params['brand']=$response['brand'];
                $params['model']=$response['model'];
                $params['rsrp']=$response['rsrp'];
                $params['rssi']=$response['rssi'];
                $params['cinr']=$response['cinr'];
                $params['tx_power']=$response['tx_power'];
                $params['ping_rtt']=$response['ping_rtt'];
                $params['firmware_version']=$response['firmware_version'];
                $params['bsid']=$response['bsid'];
                return $params;

        }

}

?>
