<?php
/*! \file model.class.php
 *  \class Model
 * 
 *  Contains the main internal methods. The provisioning and troubleshooting methods then extend it.  
 */

class Model {
	
	var $technologies = array();
	var $profiles = array();

	var $connection;
	var $DB;
	var $DB1;
	var $DB_CONCILIATION;
	var $MYSQL_HOST;
	var $MYSQL_USER;
	var $MYSQL_PASS;
	var $SSH_USER;
	var $SSH_PASS;
	var $ERROR_MYSQL;
	
	/** __consutruct */
	function __construct(){
		
			global $DB, $DB1, $MYSQL_HOST, $MYSQL_USER, $MYSQL_PASS, $technologies, $profiles,$SSH_USER,$SSH_PASS, $DB_CONCILIATION,$SNMPv3_USER,$SNMPv3_PASS,$set2default;
			
			$this->technologies = $technologies;
			$this->profiles = $profiles;
			$this->DB = _MYSQL_DB;
			$this->DB1 = _MYSQL_DB_INGPROV;
			$this->DB_CONCILIATION = _MYSQL_DB_CONCILIATION;
			$this->MYSQL_HOST = _MYSQL_HOST;
			$this->MYSQL_USER = _MYSQL_USER;
			$this->MYSQL_PASS = _MYSQL_PASS;
			$this->SSH_USER	= $SSH_USER;
			$this->SSH_PASS	= $SSH_PASS;
			$this->SNMPv3_USER = $SNMPv3_USER;
			$this->SNMPv3_PASS = $SNMPv3_PASS;
			$this->set2default = $set2default;
			$this->connection= mysqli_connect($this->MYSQL_HOST, $this->MYSQL_USER, $this->MYSQL_PASS) or $this->soap_fault_return(-3);
			mysqli_select_db($this->connection, $this->DB) or $this->soap_fault_return(-3);

			$this->connection1= mysqli_connect($this->MYSQL_HOST, $this->MYSQL_USER, $this->MYSQL_PASS) or $this->soap_fault_return(-3);
			mysqli_select_db($this->connection1, $this->DB1) or $this->soap_fault_return(-3);			
			
			mysqli_query($this->connection, "SET NAMES 'utf8'") or $this->soap_fault_return(-3);
			mysqli_query($this->connection, "set sql_mode = 'ANSI'") or $this->soap_fault_return(-3);

			mysqli_query($this->connection1, "SET NAMES 'utf8'") or $this->soap_fault_return(-3);
			mysqli_query($this->connection1, "set sql_mode = 'ANSI'") or $this->soap_fault_return(-3);

			

	}

	function soap_fault_return($code) { 
		global $ERROR_MYSQL;
		//	if (mysqli_connect_errno()) $ERROR_MYSQL=$code;
		$ERROR_MYSQL=$code;
	}	
	function validateTechnology($technology){
			$technology = mb_strtoupper($technology);
			if(in_array($technology, $this->technologies)){
				return true;
			} else {
				return false;
			}
	}

	function validateProfile($profile){
		$profile = mb_strtoupper($profile);
		if(in_array($profile, $this->profiles)){
				return true;
		} else {
				return false;
		}
    }

	function validateSubscriber($subscriberIdentity, $technology){
	  $subscriberIdentity = mb_strtoupper($subscriberIdentity);
	  $technology = mb_strtoupper($technology);
	  switch ($technology){
	   case 'ADSL':{ if(strlen($subscriberIdentity) == 12) return 1;
	                else return -1;break;}

	   case 'FTTH':{
			return 1;
			if(strlen($subscriberIdentity) == 12) {
				if ($this->subscriberStock($subscriberIdentity,$technology)){
				return 1;break;}
				else{
				return -3;break;}
				}
			else {return -1;}
			break;
			}
	   case 'LTE/WIMAX':{if(strlen($subscriberIdentity) == 12 or (strlen($subscriberIdentity)== 15)) return 1;
                else return -1; break;}
	   case 'BTB': {if(strlen($subscriberIdentity) == 12) return 1;
                else return -1; break;}
          }
    	}
	
	function subscriberExist($subscriberIdentity, $technology){
		$technology = mb_strtoupper($technology);
		$subscriberIdentity = mb_strtoupper($subscriberIdentity);
		$query="select * from ".$this->DB.".subscriber where subscriber_identity='".$subscriberIdentity."' and technology='".$technology."'";
		//print_r($this->DB);die();
		$res=mysqli_query($this->connection, $query) or $this->soap_fault_return(-3);
       	$rows = mysqli_num_rows($res);
		  // print_r($this->MYSQL_HOST);die();
        return $rows;
	}

	function subscriberStock($subscriberIdentity, $technology) {
        $technology = mb_strtoupper($technology);
		$subscriberIdentity = mb_strtoupper($subscriberIdentity);
		$sql  = "SELECT * FROM ".$this->DB.".cpe WHERE ";
        $sql .= "FSAN='$subscriberIdentity' AND ";
        $sql .= "technology='$technology' AND ";
		$sql .= "state ='1'";
		$res=mysqli_query($this->connection, $sql) or $this->soap_fault_return(-3);
		$rows = mysqli_num_rows($res);		
		return $rows;
	}
	
	function subscriberState($subscriberIdentity, $technology, $validate=true){
		$technology = mb_strtoupper($technology);
		if($validate){
			//Validate Technology
			if(!$this->validateTechnology($technology)){ return -1; }
			//Validate Subscriber Format
			if(!$this->validateSubscriber($subscriberIdentity, $technology)){ return -2; }
			//Validate subscriber Exists
			if(!$this->subscriberExist($subscriberIdentity, $technology)){ return -3; }
		}		
		// $query="SELECT profile, state FROM ".$this->DB.".subscriber WHERE subscriber_identity='$subscriberIdentity'";
		$query="select profile,state,technology from ".$this->DB.".subscriber where subscriber_identity='".$subscriberIdentity."' and technology = '".$technology."'";
		$res= mysqli_query($this->connection, $query) or $this->soap_fault_return(-3);
		$result= mysqli_fetch_assoc($res);
		return $result;
	}



	function validateCompatibility($technology,$profile){
		$technology = mb_strtoupper($technology);
		$profile = mb_strtoupper($profile);
		$sql = "SELECT * from ".$this->DB.".compatibility WHERE technology='".$technology."' AND profile='".$profile."'";
		$res= mysqli_query($this->connection, $sql) or $this->soap_fault_return(-3);
		$rows = mysqli_num_rows($res);
		if ($rows > 0){
			return true;
		}else{
			return false;
		}
	}
}
?>
