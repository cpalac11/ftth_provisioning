<?php

/*! \file parser.class.php
    \brief Este archivo es el principal de la clase Parser.

    Details.
*/

class Parser {

        var $connection;
        var $SSH_USER;
        var $SSH_PASS;

	var $fsan;
	var $profile;
	var $slot;
	var $port;
	var $shelf;
	var $ipOlt;
	var $onuID;
	var $trapType=0;
	var $subscriberIdentity;
	var $outFile;
	var $loggingEnabled;

	function __construct(){
		
		global $ipOlt,$fsan,$shelf,$slot,$port,$trapType,$subscriberIdentity,$outFile,$loggingEnabled,$onuID;
		// var $oid;
		// var $value;
		$log=new Log();
		$log->add("---------------------------------\n");
		$counter=0;
		$this->fsan=-1;
		
		
		/**$log->add("COMIENZO DEL TRAP\n");
		while (FALSE !== ($line = fgets(STDIN))) {
   			$log->add($line);
		} 
		$log->add("FIN DEL TRAP\n");**/
		
				
		while (!feof(STDIN)) {
			$input=fgets(STDIN);
			if ( $counter == 1) {
				$value=trim($input);
				$this->ipOlt=substr($value,strpos($value,'[')+1,strpos($value,']')-strpos($value,'[')-1);
			} else {
				list($oid,$value) = split(' ',$input);
				$log->add("VALOR COMPLETO DEL OID: ".$oid."\n");
				
				
				$lastDot=strrpos($oid,'.');
				$lastValue=substr($oid,$lastDot+1);
				
				$oid=substr($oid,0,strrpos($oid,'.'));
				$log->add("OID: ".$oid."\t VALUE: $value\n");
				//if ( substr($oid,0,31) == "SNMPv2-SMI::enterprises.5504.5.14.1.5.1.4." ) $oid="SNMPv2-SMI::enterprises.5504.5.14.1.5.1.4.1073741843";
				
				
				if ($oid=="SNMPv2-SMI::enterprises.5504.3.1.2.4.0") {
					$this->shelf=trim($value);
				} elseif ($oid==".SNMPv2-SMI::enterprises.5504.3.1.2.5.0") {
				 	$this->slot=trim($value);
				} elseif ($oid=="SNMPv2-SMI::enterprises.5504.3.1.2.6.0") {
				 	$this->port=trim($value);
				} elseif (substr($oid,0,42) == "SNMPv2-SMI::enterprises.5504.5.14.1.5.1.4.") {
				 	$log->add("OID COMPLETO:".$oid."\n");
					$log->add("Contador:".$counter."\n");
					$value=str_replace('"','',trim($value));
					$log->add("ENTRO POR EL MASTER CASE: \n");
					$log->add("Valor del FSAN:".$value."\n");
                    $this->fsan=$this->fsanConvert($value);
                    $this->subscriberIdentity=$this->zeroFill($value);
                    $this->trapType=1;
                    $this->onuID=$lastValue;
                    $log->add("onuID:".$this->onuID."\n");
                    $log->add("FSAN:".$this->fsan."\n");
				
				} elseif ($oid=="SNMPv2-SMI::enterprises.5504.3.1.2.6") {
				 	$this->port=trim($value);
				} elseif ($oid=="SNMPv2-SMI::enterprises.5504.3.1.2.5") {
				 	$this->slot=trim($value);
				} elseif ($oid=="SNMPv2-SMI::enterprises.5504.3.1.2.4") {
				 	$this->shelf=trim($value);
				} elseif ($oid=="SNMPv2-SMI::enterprises.5504.3.1.2.4") {
				 	$this->shelf=trim($value);
				} else {
					$log->add("NO ACTION\n");
				}
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				/*if ($oid=="SNMPv2-SMI::enterprises.5504.3.1.2.4") {
					$this->shelf=trim($value);
				} elseif ($oid=="SNMPv2-SMI::enterprises.5504.3.1.2.4") {
				 	$this->shelf=trim($value);
				} elseif ($oid==".SNMPv2-SMI::enterprises.5504.3.1.2.5") {
				 	$this->slot=trim($value);
				} elseif ($oid=="SNMPv2-SMI::enterprises.5504.3.1.2.5") {
					$this->slot=trim($value);
				} elseif ($oid=="SNMPv2-SMI::enterprises.5504.3.1.2.6") {
				 	$this->port=trim($value);
				} elseif ($oid=="SNMPv2-SMI::enterprises.5504.3.1.2.6") {
				 	$this->port=trim($value);
				} elseif (substr($oid,0,31) == "SNMPv2-SMI::enterprises.5504.5.14.1.5.1.4.") {
				 	$log->add("OID COMPLETO:".$oid."\n");
					$log->add("Contador:".$counter."\n");
					$value=str_replace('"','',trim($value));
					$log->add("ENTRO POR EL MASTER CASE: \n");
					$log->add("Valor del FSAN:".$value."\n");
                    $this->fsan=$this->fsanConvert($value);
                    $this->subscriberIdentity=$this->zeroFill($value);
                    $this->trapType=1;
                    $this->onuID=$lastValue;
                    $log->add("onuID:".$this->onuID."\n");
                    $log->add("FSAN:".$this->fsan."\n");
				
				} elseif ($oid=="SNMPv2-SMI::enterprises.5504.3.1.2.6") {
				 	$this->port=trim($value);
				} elseif ($oid=="SNMPv2-SMI::enterprises.5504.3.1.2.5") {
				 	$this->slot=trim($value);
				} elseif ($oid=="SNMPv2-SMI::enterprises.5504.3.1.2.4") {
				 	$this->shelf=trim($value);
				} elseif ($oid=="SNMPv2-SMI::enterprises.5504.3.1.2.4") {
				 	$this->shelf=trim($value);
				} else {
					$log->add("NO ACTION\n");
				}
				
				
				
				
				/*!
		
				
				
				switch ($oid) {
					case "SNMPv2-SMI::enterprises.5504.3.1.2.4":
						$this->shelf=trim($value);
						break;
					case "SNMPv2-SMI::enterprises.5504.3.1.2.4":
                                                $this->shelf=trim($value);
                                                break;
					case "SNMPv2-SMI::enterprises.5504.3.1.2.5":
						$this->slot=trim($value);
                                                break;
					case "SNMPv2-SMI::enterprises.5504.3.1.2.5":
                                                $this->slot=trim($value);
                                                break;
					case "SNMPv2-SMI::enterprises.5504.3.1.2.6":
						$this->port=trim($value);
                                                break;
					case "SNMPv2-SMI::enterprises.5504.3.1.2.6":
						$this->port=trim($value);
                                                break;
					//case "SNMPv2-SMI::enterprises.5504.5.14.1.5.1.4.1073741843":
					case  substr($oid,0,31) == "SNMPv2-SMI::enterprises.5504.5.14.1.5.1.4.": 
						$log->add("OID COMPLETO:".$oid."\n");
						$log->add("Contador:".$counter."\n");
						$value=str_replace('"','',trim($value));
						$log->add("ENTRO POR EL MASTER CASE: \n");
						$log->add("Valor del FSAN:".$value."\n");
                                                // $this->subscriberIdentity=$this->zeroFill($value);
                                                // $this->fsan=$this->fsanConvert($value);
                                                // $this->subscriberIdentity=$this->zeroFill(hexdec(substr($this->fsanConvert($value),2)));
                                                $this->fsan=$this->fsanConvert($value);
                                                $this->subscriberIdentity=$this->zeroFill($value);
                                                $this->trapType=1;
                                                $this->onuID=$lastValue;
                                                $log->add("onuID:".$this->onuID."\n");
                                                $log->add("FSAN:".$this->fsan."\n");
                                                break;
                                                
                                                
					/*!case "SNMPv2-SMI::enterprises.5504.5.14.1.5.1.4.1073741841":
						$value=str_replace('"','',trim($value));
                                                // $this->subscriberIdentity=$this->zeroFill($value);
                                                // $this->fsan=$this->fsanConvert($value);
                                                // $this->subscriberIdentity=$this->zeroFill(hexdec(substr($this->fsanConvert($value),2)));
                                                $this->fsan=$this->fsanConvert($value);
                                                $this->subscriberIdentity=$this->zeroFill($value);
                                                $this->trapType=1;
                                                break;*/
                    /*!case "SNMPv2-SMI::enterprises.5504.5.14.1.5.1.4.1073741827":
						$value=str_replace('"','',trim($value));
						// $this->subscriberIdentity=$this->zeroFill($value);
						// $this->fsan=$this->fsanConvert($value);
						// $this->subscriberIdentity=$this->zeroFill(hexdec(substr($this->fsanConvert($value),2)));
						$this->fsan=$this->fsanConvert($value);
						$this->subscriberIdentity=$this->zeroFill($value);
						$this->trapType=1;
                                                break;
					case "SNMPv2-SMI::enterprises.5504.5.14.1.5.1.4.1073741827":
						$value=str_replace('"','',trim($value));
                                                // $this->subscriberIdentity=$this->zeroFill($value);
                                                // $this->fsan=$this->fsanConvert($value);
                                                // $this->subscriberIdentity=$this->zeroFill(hexdec(substr($this->fsanConvert($value),2)));
						$this->fsan=$this->fsanConvert($value);
						$this->subscriberIdentity=$this->zeroFill($value);
                                                $this->trapType=1;
                                                break;
					case "SNMPv2-SMI::enterprises.5504.3.1.2.6":
						$this->port=trim($value);
                                                break;
					case "SNMPv2-SMI::enterprises.5504.3.1.2.5":
                                                $this->slot=trim($value);
                                                break;
                                        case "SNMPv2-SMI::enterprises.5504.3.1.2.4":
                                                $this->shelf=trim($value);
                                                break;
					case "SNMPv2-SMI::enterprises.5504.3.1.2.4":
                    	$this->shelf=trim($value);
                    break;
                    
                 	 /*!case  substr($oid,0,31) == "SNMPv2-SMI::enterprises.5504.5.14.1.5.1.4.": 
                 		//$log->add("Este es el ONUID: ".$lastValue."\n");
                 		$this->onuID=$lastValue;	
                 		break;
                    
               			default:
               		 	
               		 $log->add("Resultado del substring: ".substr($oid,0,31)."\n");		
               		 	if ( substr($oid,0,31) == "SNMPv2-SMI::enterprises.5504.5.14.1.5.1.4." ) $this->onuID=$lastValue;
               		 	
               		 	$log->add("Este es el ONUID: ".$lastValue."\n");*/
						
				}
	

			//}
			$counter++;
				
		}	
        }
	
	private function fsanConvert($fsan) {
		// $log->add("FSAN Converter:"."0".dechex($fsan)."\n");
		return "0".dechex($fsan);
	}

	private function zeroFill($subscriberIdentity) {
		while (strlen($subscriberIdentity) < 8 ) {
			$subscriberIdentity="0".$subscriberIdentity;
		}
		return $subscriberIdentity;
	}

	
}
?>

