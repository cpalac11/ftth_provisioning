<?php

class interface_integrity{

	public static function check_interface_integrity($subs_id){

		include('mysql_connection.class.php');
	        $provisioning_config = @parse_ini_file('provisioning_config.ini', true);
        	$GLOBALS['provisioning_config'] = $provisioning_config;
		$conn = new db_connection();
                $connection_array = $conn->get_connection_info($subs_id);
			if($connection_array['olt_ip']){
				$slot = $connection_array['olt_slot'];
				$port = $connection_array['olt_port'];
				$onu_id = $connection_array['olt_onuid'];
				$olt_ip = $connection_array['olt_ip'];
				$index_oid = ".1.3.6.1.4.1.5504.3.5.7.1.6.1.$slot.$port.$onu_id.1.25";
				#syslog(LOG_INFO,"update: integrity: index is $index_oid for $subs_id");

				#for ($attempt=0;$attempt<=3;$attempt++){	
				$snmp_index = snmpget($olt_ip,'DTVNET-SNMP',$index_oid);
				$snmp_index = preg_replace('/[^0-9]/', '', $snmp_index);
				syslog(LOG_INFO,"int_check snmp_index :".$snmp_index." for ".$subs_id);
				#}

				#echo $snmp_index;	
				if(!$snmp_index){
					syslog(LOG_INFO,"int_check: integrity: not index founded for $subs_id");
					$connection_array['match']=false;
					return $connection_array;
				}

				#for ($attempt=0;$attempt<=3;$attempt++){	
				$sub_fsan=snmpget($olt_ip,'DTVNET-SNMP',"1.3.6.1.4.1.5504.5.14.1.2.1.17.$snmp_index");
				#syslog(LOG_INFO,"lengh :".strlen($sub_fsan)." for ".$subs_id);
				#	if(strlen($sub_fsan) > 1){
				#		syslog(LOG_INFO,"update: attempt $attempt found  $sub_fsan for $subs_id");
				#		break;
				#	}
				#}

				$sub_fsan=str_replace(' ', '', $sub_fsan);
				$sub_fsan=str_replace('Hex-STRING:', '', $sub_fsan);
				$sub_fsan=str_replace('"', '', $sub_fsan);
				syslog(LOG_INFO,"int_check: hex string to converto is  $sub_fsan for $subs_id");
				$str = '';
    				for($i=0;$i<(strlen($sub_fsan)-2);$i+=2) $str .= chr(hexdec(substr($sub_fsan,$i,2)));
					$fsan = 'ZNTS'.$str;
					syslog(LOG_INFO,"int_check: integrity: fsan to compare is $fsan for $subs_id");

					if($subs_id==$fsan){
						$connection_array['snmp_index']=$snmp_index;
						$connection_array['match']=true;
						return $connection_array;
					}else{
						syslog(LOG_INFO,"int_check: dont match for $subs_id");
						$connection_array['match']=false;
						return $connection_array;
					}
			
			}else{
			syslog(LOG_INFO,"int_check: no interface/ip founded for $subs_id\n");
			return false;
			}

	}

}//class

?>
