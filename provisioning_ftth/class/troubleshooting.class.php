<?php
/*! \file troubleshooting.class.php
    \brief Este archivo es el principal de la clase Troubleshooting.

/* Class troubleshooting
 *
 *	Updates
 * \Authors 
 * 		Fernando Munevar fmunevar@directvla.com.co
 * 		Christian Palacios cpalac11@directvla.com.co
 * 
 * Se modifica clase, solo esta el get_params unicamente con tecnologia FTTH.
 * 
 *


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once("proxy.class.php");
*/
class Troubleshooting extends Model {
	public function get_params($subscriberIdentity, $technology = 'FTTH') {
		include('interface_integrity.class.php');
		include('ip_from_radius.class.php');
		/*
		$result['ip_address'] = '181.118.0.165';
		$result['technology'] = 'FTTH';
		$result['olt_location'] =  'ARM0100-GCRUZ-OLT01';
		$result['oper_status'] = 'down(2)';
		$result['config_state'] = 'noAlarm+inactive+lossOfSignal+lossOfAck+dyingGasp';
		$result['onu_omci_state'] = 'down - ONU is down';
		$result['olt_rx_power'] = '0 dBm';
		$result['ont_rx_power'] = '0 dBm';
		$result['olt_slot'] = '9';
		$result['olt_port'] = '7';
		$result['onu_ID'] = '3';
		$result['distance'] = '0 KM';
		$result['epc_location'] = "";
			*/
		$connection_array = interface_integrity::check_interface_integrity($subscriberIdentity);	
		if($connection_array){	
			if($connection_array['match']==true){
       			syslog(LOG_INFO,"snmp_index : ".$connection_array['snmp_index']."\n");
				$result['technology']='FTTH';
				$result['olt_slot']=$connection_array['olt_slot'];
				$result['olt_port']=$connection_array['olt_port'];
				$result['onu_ID']=$connection_array['olt_onuid'];		

				$snmpv3=new Connection();
				$result['oper_status']=$snmpv3->getSNMPv3($connection_array['olt_ip'],'onuOperationalState', $connection_array['olt_slot'], $connection_array['olt_port'], $connection_array['olt_onuid'], $connection_array['snmp_index']);
        		$result['config_state']=$snmpv3->getSNMPv3($connection_array['olt_ip'],'onuStatusWord', $connection_array['olt_slot'], $connection_array['olt_port'], $connection_array['olt_onuid'], $connection_array['snmp_index']);
        		$result['olt_rx_power']=($snmpv3->getSNMPv3($connection_array['olt_ip'],'rxPowerOlt', $connection_array['olt_slot'], $connection_array['olt_port'], $connection_array['olt_onuid'], $connection_array['snmp_index']))*0.1." dBm";
        		$result['ont_rx_power']=($snmpv3->getSNMPv3($connection_array['olt_ip'],'rxPowerOnt', $connection_array['olt_slot'], $connection_array['olt_port'], $connection_array['olt_onuid'], $connection_array['snmp_index']))*0.1." dBm";
				$result['distance']= substr((($snmpv3->getSNMPv3($connection_array['olt_ip'],'distance', $connection_array['olt_slot'], $connection_array['olt_port'], $connection_array['olt_onuid'], $connection_array['snmp_index']))*0.000001),0,6)." KM";
				$result['olt_location']=$snmpv3->getSNMPv3($connection_array['olt_ip'],'location',$connection_array['olt_slot'], $connection_array['olt_port'], $connection_array['olt_onuid'], $connection_array['snmp_index']);
				$result['onu_omci_state']=$snmpv3->getSNMPv3($connection_array['olt_ip'],'onuOmciState', $connection_array['olt_slot'],$connection_array['olt_port'], $connection_array['olt_onuid'],$connection_array['snmp_index']);
				$result['ip_address'] = rad_db_connection::get_current_ip($subscriberIdentity);
				//print_r($result); die();
				return $result;
			}
			else{
				return new soap_fault('666','','no hay coherencia entre la db y la respuesta de la olt');
			}
		}
		else{
			return new soap_fault('6700','','El cpe no puede ser alcanzado');
		}
	}

	public function get_available_firmwares($subscriberIdentity, $technology) {

	}
	
	public function upload_firmware($subscriberIdentity, $firmware_name, $technology) {
		
	}

	public function run_speed_test($subscriberIdentity, $technology) {

	}

	public function reset_connection($subscriberIdentity, $technology) {
		
	}
}
?>