<?php
/*! \file conciliation.class.php
 *  \class Conciliation 
 *
 *      Contains all internal conciliation methods
 */

/*! OLT Connection class include */
require_once("connection.class.php");
require_once("proxy.class.php");

class Conciliation extends Model{
	public function create($subscriberIdentity,$technology,$lastProfile){
		$technology = mb_strtoupper($technology);
		$lastProfile = mb_strtoupper($lastProfile);
		
		switch ($technology){
			case 'FTTH':
				$lastProvider="ZHONE";break;
			case 'ADSL':
				$lastProvider="ZYXEL";break;
			case 'LTE/WIMAX':
				$lastProvider="HUAWEI";break;
			case 'BTB':
				$lastProvider="SERCOM";break;
			}
	
		$technology = $this->findTechnologyId($technology);
		$lastProfile = $this->findProfileId($lastProfile);
		$active = $this->findIdStatus("ACTIVE");
		$lastProvider =  $this->findProviderId($lastProvider);

		if ($this->subscriberExists($subscriberIdentity,$technology)) {
        	     $sql = "UPDATE ".$this->DB_CONCILIATION.".devices SET ";
	             $sql .= "idStatus='".$active."', ";
		     $sql .= "activationDate=NOW()  " ;
                     $sql .= "WHERE cpeId = '".$subscriberIdentity.                       "' AND ";
                     $sql .= "idTechnology= '".$technology.                               "'";
                     $res= mysqli_query($this->connection, $sql) or $this->soap_fault_return(-3);
		}else{
		//save in the db conciliation in the devices table
		        $sql  = "INSERT INTO ".$this->DB_CONCILIATION.".devices(";
                        $sql .= "cpeId,";
                        $sql .= "idTechnology,";
                        $sql .= "activationDate,";
                        $sql .= "lastProfileId,";
                        $sql .= "lastProviderId,";
                        $sql .= "firstNavigationDate,";
                        $sql .= "lastNavigationDate,";
                        $sql .= "endDate,";
                        $sql .= "idStatus)";

                        $sql .= " VALUES ('".$subscriberIdentity."',";
                        $sql .= "'".$technology."',";
                        $sql .= "NOW(),";
                        $sql .= "'".$lastProfile."',";
                        $sql .= "'".$lastProvider."',";
                        $sql .= "NULL,";
                        $sql .= "NULL,";
                        $sql .= "NULL,";
                        $sql .= "'".$active."');";

			$res= mysqli_query($this->connection, $sql) or $this->soap_fault_return(-3);
		}
		$idDevice=$this->findDeviceId($subscriberIdentity,$technology);
		$this->addToDevicesProfiles("NOW()",$idDevice,$lastProfile,"NULL");
		$this->addToDevicesProviders("NOW()",$idDevice,$lastProvider,"NULL");
		$this->addToDevicesStatus("NOW()",$idDevice,$active,"NULL");
		

	}

	public function delete($subscriberIdentity,$technology) {
		$technology = $this->findTechnologyId($technology);
		$deleted = $this->findIdStatus("DELETED");
		$sql = "UPDATE ".$this->DB_CONCILIATION.".devices SET ";
                $sql .= "idStatus='".$deleted."'  ";
		$sql .= "WHERE cpeId='".$subscriberIdentity."'  ";
		$sql .= "AND idTechnology='".$technology."'  ";
                $res= mysqli_query($this->connection, $sql) or $this->soap_fault_return(-3);
		$idDevice=$this->findDeviceId($subscriberIdentity,$technology);
		$this->addToDevicesStatus("NOW()",$idDevice,$deleted,"NULL");
		}

        public function update($subscriberIdentity,$technology,$profile) {
                $technology = $this->findTechnologyId($technology);
                $profile = $this->findProfileId($profile);
                $sql = "UPDATE ".$this->DB_CONCILIATION.".devices SET                        ";
                $sql .= "lastProfileId= '".$profile.                                 "'";
                $sql .= "WHERE cpeId = '".$subscriberIdentity.                  "' AND ";
                $sql .= "idTechnology= '".$technology.                          "'";
                $res= mysqli_query($this->connection, $sql) or $this->soap_fault_return(-3);
                $idDevice=$this->findDeviceId($subscriberIdentity,$technology);
                $this->addToDevicesProfiles("NOW()",$idDevice,$profile,"NULL");
                }



	private function  finishDeviceProfile($idDevice){
		$sql = "UPDATE ".$this->DB_CONCILIATION.".devicesprofiles SET ";
                $sql .= "endDate=NOW()";
                $sql .= "WHERE idDevice= '".$idDevice."'";
                $sql .= "AND endDate is null";
                $res= mysqli_query($this->connection, $sql) or $this->soap_fault_return(-3);
	}

        private function  finishDeviceProvider($idDevice){
                $sql = "UPDATE ".$this->DB_CONCILIATION.".devicesproviders SET ";
                $sql .= "endDate=NOW()";
                $sql .= "WHERE idDevice= '".$idDevice."'";
                $sql .= "AND endDate is null";
                $res= mysqli_query($this->connection, $sql) or $this->soap_fault_return(-3);
        }

        private function  finishDeviceStatus($idDevice){
                $sql = "UPDATE ".$this->DB_CONCILIATION.".devicesstatus SET ";
                $sql .= "endDate=NOW()";
                $sql .= "WHERE idDevice= '".$idDevice."'  ";
                $sql .= "AND endDate is null";
                $res= mysqli_query($this->connection, $sql) or $this->soap_fault_return(-3);
        }


	private function addToDevicesProfiles($date,$idDevice,$idProfile,$endDate){
		$this->finishDeviceProfile($idDevice);

		$sql  = "INSERT INTO ".$this->DB_CONCILIATION.".devicesprofiles(";
                $sql .= "date,";
                $sql .= "idDevice,";
                $sql .= "idProfile,";
                $sql .= "endDate)";

		if ($date="NOW()"){$sql .= " VALUES (NOW() ,";}
		else {$sql .= " VALUES ('".$date."',";}
                $sql .= "'".$idDevice."',";
                $sql .= "'".$idProfile."',";
                if ($endDate="NULL"){$sql .= "NULL)";}
                else{$sql .= "'".$endDate."');";}

                $res= mysqli_query($this->connection, $sql) or $this->soap_fault_return(-3); 

	}

	private function addToDevicesProviders($date,$idDevice,$idProvider,$endDate){
                $this->finishDeviceProvider($idDevice);

		$sql  = "INSERT INTO ".$this->DB_CONCILIATION.".devicesproviders(";
                $sql .= "date,";
                $sql .= "idDevice,";
                $sql .= "idProvider,";
                $sql .= "endDate)";

                if ($date="NOW()"){$sql .= " VALUES (NOW() ,";}
                else {$sql .= " VALUES ('".$date."',";}
                $sql .= "'".$idDevice."',";
                $sql .= "'".$idProvider."',";
                if ($endDate="NULL"){$sql .= "NULL)";}
                else{$sql .= "'".$endDate."');";}
                $res= mysqli_query($this->connection, $sql) or $this->soap_fault_return(-3); 

	}

	private function addToDevicesStatus($date,$idDevice,$idStatus,$endDate){
                $this->finishDeviceStatus($idDevice);

		$sql  = "INSERT INTO ".$this->DB_CONCILIATION.".devicesstatus(";
                $sql .= "date,";
                $sql .= "idDevice,";
                $sql .= "idStatus,";
                $sql .= "endDate)";

                if ($date="NOW()"){$sql .= " VALUES (NOW() ,";}
                else {$sql .= " VALUES ('".$date."',";}
                $sql .= "'".$idDevice."',";
                $sql .= "'".$idStatus."',";
		if ($endDate="NULL"){$sql .= "NULL)";}
		else{$sql .= "'".$endDate."');";}
                $res= mysqli_query($this->connection, $sql) or $this->soap_fault_return(-3); 

	}


	private function findTechnologyId($technology){
                $technology = mb_strtoupper($technology);
                $sql = "select idTechnology from ".$this->DB_CONCILIATION.".technologies where technology='".$technology."'";
                $res= mysqli_query($this->connection, $sql) or $this->soap_fault_return(-3);
                $row = mysqli_fetch_row($res);
		$id = trim($row[0]);
                return $id;
                }


        private function findProviderId($provider){
                $provider = mb_strtoupper($provider);
                $sql = "select idProvider from ".$this->DB_CONCILIATION.".providers where provider='".$provider."'";
                $res= mysqli_query($this->connection, $sql) or $this->soap_fault_return(-3);
                $row = mysqli_fetch_row($res);
                $id = trim($row[0]);
                return $id;
                }

        private function findDeviceId($subscriberIdentity,$technology){
                $sql = "select idDevice from ".$this->DB_CONCILIATION.".devices where cpeId='".$subscriberIdentity."' AND idTechnology='".$technology."'";
                $res= mysqli_query($this->connection, $sql) or $this->soap_fault_return(-3);
                $row = mysqli_fetch_row($res);
                $id = trim($row[0]);
                return $id;
                }




        private function findProfileId($profile){
                $profile = mb_strtoupper($profile);
                $sql = "SELECT idProfile from ".$this->DB_CONCILIATION.".profiles WHERE  profile='".$profile."'";
                $res= mysqli_query($this->connection, $sql) or $this->soap_fault_return(-3);
		$row = mysqli_fetch_row($res);
                $id = trim($row[0]);
                return $id;

                }




        private function findIdStatus($status){
                $status = mb_strtoupper($status);
                $sql = "SELECT idStatus from ".$this->DB_CONCILIATION.".status WHERE  status='".$status."'";
                $res= mysqli_query($this->connection, $sql) or $this->soap_fault_return(-3);
                $row = mysqli_fetch_row($res);
                $id = trim($row[0]);
                return $id;
                
             }


	private function subscriberExists($subscriberIdentity,$technology){
                $technology = mb_strtoupper($technology);
                $sql = "SELECT * from ".$this->DB_CONCILIATION.".devices WHERE  cpeId='".$subscriberIdentity."' and idTechnology='".$technology."'";
                $res= mysqli_query($this->connection, $sql) or $this->soap_fault_return(-3);
                $rows = mysqli_num_rows($res);
                if ($rows > 0){
                return true;
                }else{
                return false;
                }

	}
}

?>
