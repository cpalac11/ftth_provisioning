<?php
// ini_set('display_errors', '1');
/*! \file index.php
 *  \brief Main file of provisioning system.
 *  \author Lucas Lorenzo
 *  \version 1.0
 *  \date March 2015
 *  \detail Includes Definitions and methods for provisioning and Troubleshooting.
 */

//ERROR HANDLING
//ini_set('error_reporting', E_ALL|E_STRICT);
//ini_set('display_errors', 1);
require("../enviroment.php");
require_once('lib/nusoap.php');
require_once('config.php');
require_once('class/model.class.php');
require_once('class/provisioning.class.php');
require_once('class/troubleshooting.class.php');
try {
    $server = new soap_server();
    $ns="Broadband";
    $server->soap_defencoding = 'UTF-8';
    $server->configurewsdl('Broadband','Broadband'); /** WS name */
    $server->wsdl->schematargetnamespace="$ns"; /** WS namespace */
    /*! Definitions Include */
    require_once('includes/definitions.php');
    /*! Soap Methods Include - Provisioning */
    require_once('includes/provisioningDefine.php');
    /*! Soap Methods Include - Troubleshooting */
    require_once('includes/troubleshootingDefine.php');
    /*! Util Functions Include */
    require_once('includes/utils.php');
    /*! Processing and Response */
    $input = (isset($HTTP_RAW_POST_DATA)) ? $HTTP_RAW_POST_DATA : implode("\r\n", file('php://input'));
    $server->service($input);
    exit;
} catch (Exception $e) {
    header("Content-Type: text/xml");
    header("Status: 200");
    $string = (
		'<?xml version="1.0" encoding="ISO-8859-1" ?>'."\n".
		'<SOAP-ENV:Envelope SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" '.
		'xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" '.
		'xmlns:xsd="http://www.w3.org/2001/XMLSchema" '.
		'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '.
		'xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:tns="'. $namespace .'">'.
			'<SOAP-ENV:Body>'.
				'<SOAP-ENV:Fault>'.
					'<faultcode>'. $e->getCode() .'</faultcode>'.
					'<faultstring>'. $e->getMessage() .'</faultstring>'.
				'</SOAP-ENV:Fault>'.
			'</SOAP-ENV:Body>'.
		'</SOAP-ENV:Envelope>
		');
	$render = new SimpleXMLElement($string);
	echo $render->asXML();
}
?>